/* first things first--we've got to hide results before the document loads
and them show them again during document ready, so we don't get the weird
"jump" */
if (navigator.userAgent.match(/MSIE/)) {
    document.styleSheets[0].addRule("div.pageContent", "display:none");
    document.styleSheets[0].addRule("#quick-access-block-wrap", "display:none");
} else {
    for (var i = 0; i < document.styleSheets.length; i++) {
        if (typeof(document.styleSheets[i].href) === "string" && document.styleSheets[i].href.match(/\/styles.css/)) {
            document.styleSheets[i].insertRule("div.pageContent {display:none;}", 1);
            document.styleSheets[i].insertRule("#quick-access-block-wrap {display:none;}", 1);
        }
    }
}

// submit subject function for III e-resource subject search

function submitSubject(id) {
    window.location = "http://library.colgate.edu:2082/search/d?" + $(id).val();
    return true;
}

// config options for addthis
var addthis_config = {
    pubid: "mmdl",
    "data_track_clickback": true,
    ui_cobrand: "UNT Library Catalog",
    ui_header_color: "#ffffff",
    ui_header_background: "#059033"
}
// config for Google Analytics
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-23458559-2']);
_gaq.push(['_trackPageview']);
(function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();



/*************/
/* superfish */
/*************/

/*
 * Superfish v1.4.8 - jQuery menu widget
 * Copyright (c) 2008 Joel Birch
 *
 * Dual licensed under the MIT and GPL licenses:
 * 	http://www.opensource.org/licenses/mit-license.php
 * 	http://www.gnu.org/licenses/gpl.html
 *
 * CHANGELOG: http://users.tpg.com.au/j_birch/plugins/superfish/changelog.txt
 */

;
(function($) {
    $.fn.superfish = function(op) {
        var sf = $.fn.superfish,
            c = sf.c,
            $arrow = $(['<span class="', c.arrowClass, '"> &#187;</span>'].join('')),
            over = function() {
                var $$ = $(this),
                    menu = getMenu($$);
                clearTimeout(menu.sfTimer);
                $$.showSuperfishUl().siblings().hideSuperfishUl();
            },
            out = function() {
                var $$ = $(this),
                    menu = getMenu($$),
                    o = sf.op;
                clearTimeout(menu.sfTimer);
                menu.sfTimer = setTimeout(function() {
                    o.retainPath = ($.inArray($$[0], o.$path) > -1);
                    $$.hideSuperfishUl();
                    if (o.$path.length && $$.parents(['li.', o.hoverClass].join('')).length < 1) {
                        over.call(o.$path);
                    }
                }, o.delay);
            },
            getMenu = function($menu) {
                var menu = $menu.parents(['ul.', c.menuClass, ':first'].join(''))[0];
                sf.op = sf.o[menu.serial];
                return menu;
            },
            addArrow = function($a) {
                $a.addClass(c.anchorClass).append($arrow.clone());
            };

        return this.each(function() {
            var s = this.serial = sf.o.length;
            var o = $.extend({}, sf.defaults, op);
            o.$path = $('li.' + o.pathClass, this).slice(0, o.pathLevels).each(function() {
                $(this).addClass([o.hoverClass, c.bcClass].join(' '))
                    .filter('li:has(ul)').removeClass(o.pathClass);
            });
            sf.o[s] = sf.op = o;

            $('li:has(ul)', this)[($.fn.hoverIntent && !o.disableHI) ? 'hoverIntent' : 'hover'](over, out).each(function() {
                if (o.autoArrows) addArrow($('>a:first-child', this));
            })
                .not('.' + c.bcClass)
                .hideSuperfishUl();

            var $a = $('a', this);
            $a.each(function(i) {
                var $li = $a.eq(i).parents('li');
                $a.eq(i).focus(function() {
                    over.call($li);
                }).blur(function() {
                    out.call($li);
                });
            });
            o.onInit.call(this);

        }).each(function() {
            var menuClasses = [c.menuClass];
            if (sf.op.dropShadows && !($.browser.msie && $.browser.version < 7)) menuClasses.push(c.shadowClass);
            $(this).addClass(menuClasses.join(' '));
        });
    };

    var sf = $.fn.superfish;
    sf.o = [];
    sf.op = {};
    sf.IE7fix = function() {
        var o = sf.op;
        if ($.browser.msie && $.browser.version > 6 && o.dropShadows && o.animation.opacity != undefined)
            this.toggleClass(sf.c.shadowClass + '-off');
    };
    sf.c = {
        bcClass: 'sf-breadcrumb',
        menuClass: 'sf-js-enabled',
        anchorClass: 'sf-with-ul',
        arrowClass: 'sf-sub-indicator',
        shadowClass: 'sf-shadow'
    };
    sf.defaults = {
        hoverClass: 'sfHover',
        pathClass: 'overideThisToUse',
        pathLevels: 1,
        delay: 800,
        animation: {
            opacity: 'show'
        },
        speed: 'normal',
        autoArrows: true,
        dropShadows: true,
        disableHI: false, // true disables hoverIntent detection
        onInit: function() {}, // callback functions
        onBeforeShow: function() {},
        onShow: function() {},
        onHide: function() {}
    };
    $.fn.extend({
        hideSuperfishUl: function() {
            var o = sf.op,
                not = (o.retainPath === true) ? o.$path : '';
            o.retainPath = false;
            var $ul = $(['li.', o.hoverClass].join(''), this).add(this).not(not).removeClass(o.hoverClass)
                .find('>ul').hide().css('visibility', 'hidden');
            o.onHide.call($ul);
            return this;
        },
        showSuperfishUl: function() {
            var o = sf.op,
                sh = sf.c.shadowClass + '-off',
                $ul = this.addClass(o.hoverClass)
                    .find('>ul:hidden').css('visibility', 'visible');
            sf.IE7fix.call($ul);
            o.onBeforeShow.call($ul);
            $ul.animate(o.animation, o.speed, function() {
                sf.IE7fix.call($ul);
                o.onShow.call($ul);
            });
            return this;
        }
    });

})(jQuery);

// Jquery log

jQuery.fn.log = function(msg) {
    console.log("%s: %o", msg, this);
    return this;
};


/**
 * jQuery Validation Plugin 1.8.1
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2011 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
(function(c) {
    c.extend(c.fn, {
        validate: function(a) {
            if (this.length) {
                var b = c.data(this[0], "validator");
                if (b) return b;
                b = new c.validator(a, this[0]);
                c.data(this[0], "validator", b);
                if (b.settings.onsubmit) {
                    this.find("input, button").filter(".cancel").click(function() {
                        b.cancelSubmit = true
                    });
                    b.settings.submitHandler && this.find("input, button").filter(":submit").click(function() {
                        b.submitButton = this
                    });
                    this.submit(function(d) {
                        function e() {
                            if (b.settings.submitHandler) {
                                if (b.submitButton) var f = c("<input type='hidden'/>").attr("name", b.submitButton.name).val(b.submitButton.value).appendTo(b.currentForm);
                                b.settings.submitHandler.call(b, b.currentForm);
                                b.submitButton && f.remove();
                                return false
                            }
                            return true
                        }
                        b.settings.debug && d.preventDefault();
                        if (b.cancelSubmit) {
                            b.cancelSubmit = false;
                            return e()
                        }
                        if (b.form()) {
                            if (b.pendingRequest) {
                                b.formSubmitted = true;
                                return false
                            }
                            return e()
                        } else {
                            b.focusInvalid();
                            return false
                        }
                    })
                }
                return b
            } else a && a.debug && window.console && console.warn("nothing selected, can't validate, returning nothing")
        },
        valid: function() {
            if (c(this[0]).is("form")) return this.validate().form();
            else {
                var a = true,
                    b = c(this[0].form).validate();
                this.each(function() {
                    a &= b.element(this)
                });
                return a
            }
        },
        removeAttrs: function(a) {
            var b = {}, d = this;
            c.each(a.split(/\s/), function(e, f) {
                b[f] = d.attr(f);
                d.removeAttr(f)
            });
            return b
        },
        rules: function(a, b) {
            var d = this[0];
            if (a) {
                var e = c.data(d.form, "validator").settings,
                    f = e.rules,
                    g = c.validator.staticRules(d);
                switch (a) {
                    case "add":
                        c.extend(g, c.validator.normalizeRule(b));
                        f[d.name] = g;
                        if (b.messages) e.messages[d.name] = c.extend(e.messages[d.name], b.messages);
                        break;
                    case "remove":
                        if (!b) {
                            delete f[d.name];
                            return g
                        }
                        var h = {};
                        c.each(b.split(/\s/), function(j, i) {
                            h[i] = g[i];
                            delete g[i]
                        });
                        return h
                }
            }
            d = c.validator.normalizeRules(c.extend({}, c.validator.metadataRules(d), c.validator.classRules(d), c.validator.attributeRules(d), c.validator.staticRules(d)), d);
            if (d.required) {
                e = d.required;
                delete d.required;
                d = c.extend({
                    required: e
                }, d)
            }
            return d
        }
    });
    c.extend(c.expr[":"], {
        blank: function(a) {
            return !c.trim("" + a.value)
        },
        filled: function(a) {
            return !!c.trim("" + a.value)
        },
        unchecked: function(a) {
            return !a.checked
        }
    });
    c.validator = function(a, b) {
        this.settings = c.extend(true, {}, c.validator.defaults, a);
        this.currentForm = b;
        this.init()
    };
    c.validator.format = function(a, b) {
        if (arguments.length == 1) return function() {
            var d = c.makeArray(arguments);
            d.unshift(a);
            return c.validator.format.apply(this, d)
        };
        if (arguments.length > 2 && b.constructor != Array) b = c.makeArray(arguments).slice(1);
        if (b.constructor != Array) b = [b];
        c.each(b, function(d, e) {
            a = a.replace(RegExp("\\{" + d + "\\}", "g"), e)
        });
        return a
    };
    c.extend(c.validator, {
        defaults: {
            messages: {},
            groups: {},
            rules: {},
            errorClass: "error",
            validClass: "valid",
            errorElement: "label",
            focusInvalid: true,
            errorContainer: c([]),
            errorLabelContainer: c([]),
            onsubmit: true,
            ignore: [],
            ignoreTitle: false,
            onfocusin: function(a) {
                this.lastActive = a;
                if (this.settings.focusCleanup && !this.blockFocusCleanup) {
                    this.settings.unhighlight && this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass);
                    this.addWrapper(this.errorsFor(a)).hide()
                }
            },
            onfocusout: function(a) {
                if (!this.checkable(a) && (a.name in this.submitted || !this.optional(a))) this.element(a)
            },
            onkeyup: function(a) {
                if (a.name in this.submitted || a == this.lastElement) this.element(a)
            },
            onclick: function(a) {
                if (a.name in this.submitted) this.element(a);
                else a.parentNode.name in this.submitted && this.element(a.parentNode)
            },
            highlight: function(a, b, d) {
                a.type === "radio" ? this.findByName(a.name).addClass(b).removeClass(d) : c(a).addClass(b).removeClass(d)
            },
            unhighlight: function(a, b, d) {
                a.type === "radio" ? this.findByName(a.name).removeClass(b).addClass(d) : c(a).removeClass(b).addClass(d)
            }
        },
        setDefaults: function(a) {
            c.extend(c.validator.defaults, a)
        },
        messages: {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Please enter a valid email address.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Please enter a valid number.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: c.validator.format("Please enter no more than {0} characters."),
            minlength: c.validator.format("Please enter at least {0} characters."),
            rangelength: c.validator.format("Please enter a value between {0} and {1} characters long."),
            range: c.validator.format("Please enter a value between {0} and {1}."),
            max: c.validator.format("Please enter a value less than or equal to {0}."),
            min: c.validator.format("Please enter a value greater than or equal to {0}.")
        },
        autoCreateRanges: false,
        prototype: {
            init: function() {
                function a(e) {
                    var f = c.data(this[0].form, "validator");
                    e = "on" + e.type.replace(/^validate/, "");
                    f.settings[e] && f.settings[e].call(f, this[0])
                }
                this.labelContainer = c(this.settings.errorLabelContainer);
                this.errorContext = this.labelContainer.length && this.labelContainer || c(this.currentForm);
                this.containers = c(this.settings.errorContainer).add(this.settings.errorLabelContainer);
                this.submitted = {};
                this.valueCache = {};
                this.pendingRequest = 0;
                this.pending = {};
                this.invalid = {};
                this.reset();
                var b = this.groups = {};
                c.each(this.settings.groups, function(e, f) {
                    c.each(f.split(/\s/), function(g, h) {
                        b[h] = e
                    })
                });
                var d = this.settings.rules;
                c.each(d, function(e, f) {
                    d[e] = c.validator.normalizeRule(f)
                });
                c(this.currentForm).validateDelegate(":text, :password, :file, select, textarea", "focusin focusout keyup", a).validateDelegate(":radio, :checkbox, select, option", "click", a);
                this.settings.invalidHandler && c(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler)
            },
            form: function() {
                this.checkForm();
                c.extend(this.submitted, this.errorMap);
                this.invalid = c.extend({}, this.errorMap);
                this.valid() || c(this.currentForm).triggerHandler("invalid-form", [this]);
                this.showErrors();
                return this.valid()
            },
            checkForm: function() {
                this.prepareForm();
                for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) this.check(b[a]);
                return this.valid()
            },
            element: function(a) {
                this.lastElement = a = this.clean(a);
                this.prepareElement(a);
                this.currentElements = c(a);
                var b = this.check(a);
                if (b) delete this.invalid[a.name];
                else this.invalid[a.name] = true; if (!this.numberOfInvalids()) this.toHide = this.toHide.add(this.containers);
                this.showErrors();
                return b
            },
            showErrors: function(a) {
                if (a) {
                    c.extend(this.errorMap, a);
                    this.errorList = [];
                    for (var b in a) this.errorList.push({
                        message: a[b],
                        element: this.findByName(b)[0]
                    });
                    this.successList = c.grep(this.successList, function(d) {
                        return !(d.name in a)
                    })
                }
                this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
            },
            resetForm: function() {
                c.fn.resetForm && c(this.currentForm).resetForm();
                this.submitted = {};
                this.prepareForm();
                this.hideErrors();
                this.elements().removeClass(this.settings.errorClass)
            },
            numberOfInvalids: function() {
                return this.objectLength(this.invalid)
            },
            objectLength: function(a) {
                var b = 0,
                    d;
                for (d in a) b++;
                return b
            },
            hideErrors: function() {
                this.addWrapper(this.toHide).hide()
            },
            valid: function() {
                return this.size() == 0
            },
            size: function() {
                return this.errorList.length
            },
            focusInvalid: function() {
                if (this.settings.focusInvalid) try {
                    c(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin")
                } catch (a) {}
            },
            findLastActive: function() {
                var a = this.lastActive;
                return a && c.grep(this.errorList, function(b) {
                    return b.element.name == a.name
                }).length == 1 && a
            },
            elements: function() {
                var a = this,
                    b = {};
                return c(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function() {
                    !this.name && a.settings.debug && window.console && console.error("%o has no name assigned", this);
                    if (this.name in b || !a.objectLength(c(this).rules())) return false;
                    return b[this.name] = true
                })
            },
            clean: function(a) {
                return c(a)[0]
            },
            errors: function() {
                return c(this.settings.errorElement + "." + this.settings.errorClass, this.errorContext)
            },
            reset: function() {
                this.successList = [];
                this.errorList = [];
                this.errorMap = {};
                this.toShow = c([]);
                this.toHide = c([]);
                this.currentElements = c([])
            },
            prepareForm: function() {
                this.reset();
                this.toHide = this.errors().add(this.containers)
            },
            prepareElement: function(a) {
                this.reset();
                this.toHide = this.errorsFor(a)
            },
            check: function(a) {
                a = this.clean(a);
                if (this.checkable(a)) a = this.findByName(a.name).not(this.settings.ignore)[0];
                var b = c(a).rules(),
                    d = false,
                    e;
                for (e in b) {
                    var f = {
                        method: e,
                        parameters: b[e]
                    };
                    try {
                        var g = c.validator.methods[e].call(this, a.value.replace(/\r/g, ""), a, f.parameters);
                        if (g == "dependency-mismatch") d = true;
                        else {
                            d = false;
                            if (g == "pending") {
                                this.toHide = this.toHide.not(this.errorsFor(a));
                                return
                            }
                            if (!g) {
                                this.formatAndAdd(a, f);
                                return false
                            }
                        }
                    } catch (h) {
                        this.settings.debug && window.console && console.log("exception occured when checking element " + a.id + ", check the '" + f.method + "' method", h);
                        throw h;
                    }
                }
                if (!d) {
                    this.objectLength(b) && this.successList.push(a);
                    return true
                }
            },
            customMetaMessage: function(a, b) {
                if (c.metadata) {
                    var d = this.settings.meta ? c(a).metadata()[this.settings.meta] : c(a).metadata();
                    return d && d.messages && d.messages[b]
                }
            },
            customMessage: function(a, b) {
                var d = this.settings.messages[a];
                return d && (d.constructor == String ? d : d[b])
            },
            findDefined: function() {
                for (var a = 0; a < arguments.length; a++)
                    if (arguments[a] !== undefined) return arguments[a]
            },
            defaultMessage: function(a, b) {
                return this.findDefined(this.customMessage(a.name, b), this.customMetaMessage(a, b), !this.settings.ignoreTitle && a.title || undefined, c.validator.messages[b], "<strong>Warning: No message defined for " + a.name + "</strong>")
            },
            formatAndAdd: function(a, b) {
                var d = this.defaultMessage(a, b.method),
                    e = /\$?\{(\d+)\}/g;
                if (typeof d == "function") d = d.call(this, b.parameters, a);
                else if (e.test(d)) d = jQuery.format(d.replace(e, "{$1}"), b.parameters);
                this.errorList.push({
                    message: d,
                    element: a
                });
                this.errorMap[a.name] = d;
                this.submitted[a.name] = d
            },
            addWrapper: function(a) {
                if (this.settings.wrapper) a = a.add(a.parent(this.settings.wrapper));
                return a
            },
            defaultShowErrors: function() {
                for (var a = 0; this.errorList[a]; a++) {
                    var b = this.errorList[a];
                    this.settings.highlight && this.settings.highlight.call(this, b.element, this.settings.errorClass, this.settings.validClass);
                    this.showLabel(b.element, b.message)
                }
                if (this.errorList.length) this.toShow = this.toShow.add(this.containers);
                if (this.settings.success)
                    for (a = 0; this.successList[a]; a++) this.showLabel(this.successList[a]);
                if (this.settings.unhighlight) {
                    a = 0;
                    for (b = this.validElements(); b[a]; a++) this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass)
                }
                this.toHide = this.toHide.not(this.toShow);
                this.hideErrors();
                this.addWrapper(this.toShow).show()
            },
            validElements: function() {
                return this.currentElements.not(this.invalidElements())
            },
            invalidElements: function() {
                return c(this.errorList).map(function() {
                    return this.element
                })
            },
            showLabel: function(a, b) {
                var d = this.errorsFor(a);
                if (d.length) {
                    d.removeClass().addClass(this.settings.errorClass);
                    d.attr("generated") && d.html(b)
                } else {
                    d = c("<" + this.settings.errorElement + "/>").attr({
                        "for": this.idOrName(a),
                        generated: true
                    }).addClass(this.settings.errorClass).html(b || "");
                    if (this.settings.wrapper) d = d.hide().show().wrap("<" + this.settings.wrapper + "/>").parent();
                    this.labelContainer.append(d).length || (this.settings.errorPlacement ? this.settings.errorPlacement(d, c(a)) : d.insertAfter(a))
                } if (!b && this.settings.success) {
                    d.text("");
                    typeof this.settings.success == "string" ? d.addClass(this.settings.success) : this.settings.success(d)
                }
                this.toShow = this.toShow.add(d)
            },
            errorsFor: function(a) {
                var b = this.idOrName(a);
                return this.errors().filter(function() {
                    return c(this).attr("for") == b
                })
            },
            idOrName: function(a) {
                return this.groups[a.name] || (this.checkable(a) ? a.name : a.id || a.name)
            },
            checkable: function(a) {
                return /radio|checkbox/i.test(a.type)
            },
            findByName: function(a) {
                var b = this.currentForm;
                return c(document.getElementsByName(a)).map(function(d, e) {
                    return e.form == b && e.name == a && e || null
                })
            },
            getLength: function(a, b) {
                switch (b.nodeName.toLowerCase()) {
                    case "select":
                        return c("option:selected", b).length;
                    case "input":
                        if (this.checkable(b)) return this.findByName(b.name).filter(":checked").length
                }
                return a.length
            },
            depend: function(a, b) {
                return this.dependTypes[typeof a] ? this.dependTypes[typeof a](a, b) : true
            },
            dependTypes: {
                "boolean": function(a) {
                    return a
                },
                string: function(a, b) {
                    return !!c(a, b.form).length
                },
                "function": function(a, b) {
                    return a(b)
                }
            },
            optional: function(a) {
                return !c.validator.methods.required.call(this, c.trim(a.value), a) && "dependency-mismatch"
            },
            startRequest: function(a) {
                if (!this.pending[a.name]) {
                    this.pendingRequest++;
                    this.pending[a.name] = true
                }
            },
            stopRequest: function(a, b) {
                this.pendingRequest--;
                if (this.pendingRequest < 0) this.pendingRequest = 0;
                delete this.pending[a.name];
                if (b && this.pendingRequest == 0 && this.formSubmitted && this.form()) {
                    c(this.currentForm).submit();
                    this.formSubmitted = false
                } else if (!b && this.pendingRequest == 0 && this.formSubmitted) {
                    c(this.currentForm).triggerHandler("invalid-form", [this]);
                    this.formSubmitted = false
                }
            },
            previousValue: function(a) {
                return c.data(a, "previousValue") || c.data(a, "previousValue", {
                    old: null,
                    valid: true,
                    message: this.defaultMessage(a, "remote")
                })
            }
        },
        classRuleSettings: {
            required: {
                required: true
            },
            email: {
                email: true
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            dateISO: {
                dateISO: true
            },
            dateDE: {
                dateDE: true
            },
            number: {
                number: true
            },
            numberDE: {
                numberDE: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            }
        },
        addClassRules: function(a, b) {
            a.constructor == String ? this.classRuleSettings[a] = b : c.extend(this.classRuleSettings, a)
        },
        classRules: function(a) {
            var b = {};
            (a = c(a).attr("class")) && c.each(a.split(" "), function() {
                this in c.validator.classRuleSettings && c.extend(b, c.validator.classRuleSettings[this])
            });
            return b
        },
        attributeRules: function(a) {
            var b = {};
            a = c(a);
            for (var d in c.validator.methods) {
                var e = a.attr(d);
                if (e) b[d] = e
            }
            b.maxlength && /-1|2147483647|524288/.test(b.maxlength) && delete b.maxlength;
            return b
        },
        metadataRules: function(a) {
            if (!c.metadata) return {};
            var b = c.data(a.form, "validator").settings.meta;
            return b ? c(a).metadata()[b] : c(a).metadata()
        },
        staticRules: function(a) {
            var b = {}, d = c.data(a.form, "validator");
            if (d.settings.rules) b = c.validator.normalizeRule(d.settings.rules[a.name]) || {};
            return b
        },
        normalizeRules: function(a, b) {
            c.each(a, function(d, e) {
                if (e === false) delete a[d];
                else if (e.param || e.depends) {
                    var f = true;
                    switch (typeof e.depends) {
                        case "string":
                            f = !! c(e.depends, b.form).length;
                            break;
                        case "function":
                            f = e.depends.call(b, b)
                    }
                    if (f) a[d] = e.param !== undefined ? e.param : true;
                    else delete a[d]
                }
            });
            c.each(a, function(d, e) {
                a[d] = c.isFunction(e) ? e(b) : e
            });
            c.each(["minlength", "maxlength", "min", "max"], function() {
                if (a[this]) a[this] = Number(a[this])
            });
            c.each(["rangelength", "range"], function() {
                if (a[this]) a[this] = [Number(a[this][0]), Number(a[this][1])]
            });
            if (c.validator.autoCreateRanges) {
                if (a.min && a.max) {
                    a.range = [a.min, a.max];
                    delete a.min;
                    delete a.max
                }
                if (a.minlength && a.maxlength) {
                    a.rangelength = [a.minlength, a.maxlength];
                    delete a.minlength;
                    delete a.maxlength
                }
            }
            a.messages && delete a.messages;
            return a
        },
        normalizeRule: function(a) {
            if (typeof a == "string") {
                var b = {};
                c.each(a.split(/\s/), function() {
                    b[this] = true
                });
                a = b
            }
            return a
        },
        addMethod: function(a, b, d) {
            c.validator.methods[a] = b;
            c.validator.messages[a] = d != undefined ? d : c.validator.messages[a];
            b.length < 3 && c.validator.addClassRules(a, c.validator.normalizeRule(a))
        },
        methods: {
            required: function(a, b, d) {
                if (!this.depend(d, b)) return "dependency-mismatch";
                switch (b.nodeName.toLowerCase()) {
                    case "select":
                        return (a = c(b).val()) && a.length > 0;
                    case "input":
                        if (this.checkable(b)) return this.getLength(a, b) > 0;
                    default:
                        return c.trim(a).length > 0
                }
            },
            remote: function(a, b, d) {
                if (this.optional(b)) return "dependency-mismatch";
                var e = this.previousValue(b);
                this.settings.messages[b.name] || (this.settings.messages[b.name] = {});
                e.originalMessage = this.settings.messages[b.name].remote;
                this.settings.messages[b.name].remote = e.message;
                d = typeof d == "string" && {
                    url: d
                } || d;
                if (this.pending[b.name]) return "pending";
                if (e.old === a) return e.valid;
                e.old = a;
                var f = this;
                this.startRequest(b);
                var g = {};
                g[b.name] = a;
                c.ajax(c.extend(true, {
                    url: d,
                    mode: "abort",
                    port: "validate" + b.name,
                    dataType: "json",
                    data: g,
                    success: function(h) {
                        f.settings.messages[b.name].remote = e.originalMessage;
                        var j = h === true;
                        if (j) {
                            var i = f.formSubmitted;
                            f.prepareElement(b);
                            f.formSubmitted = i;
                            f.successList.push(b);
                            f.showErrors()
                        } else {
                            i = {};
                            h = h || f.defaultMessage(b, "remote");
                            i[b.name] = e.message = c.isFunction(h) ? h(a) : h;
                            f.showErrors(i)
                        }
                        e.valid = j;
                        f.stopRequest(b, j)
                    }
                }, d));
                return "pending"
            },
            minlength: function(a, b, d) {
                return this.optional(b) || this.getLength(c.trim(a), b) >= d
            },
            maxlength: function(a, b, d) {
                return this.optional(b) || this.getLength(c.trim(a), b) <= d
            },
            rangelength: function(a, b, d) {
                a = this.getLength(c.trim(a), b);
                return this.optional(b) || a >= d[0] && a <= d[1]
            },
            min: function(a, b, d) {
                return this.optional(b) || a >= d
            },
            max: function(a, b, d) {
                return this.optional(b) || a <= d
            },
            range: function(a, b, d) {
                return this.optional(b) || a >= d[0] && a <= d[1]
            },
            email: function(a, b) {
                return this.optional(b) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(a)
            },
            url: function(a, b) {
                return this.optional(b) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a)
            },
            date: function(a, b) {
                return this.optional(b) || !/Invalid|NaN/.test(new Date(a))
            },
            dateISO: function(a, b) {
                return this.optional(b) || /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(a)
            },
            number: function(a, b) {
                return this.optional(b) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(a)
            },
            digits: function(a, b) {
                return this.optional(b) || /^\d+$/.test(a)
            },
            creditcard: function(a, b) {
                if (this.optional(b)) return "dependency-mismatch";
                if (/[^0-9-]+/.test(a)) return false;
                var d = 0,
                    e = 0,
                    f = false;
                a = a.replace(/\D/g, "");
                for (var g = a.length - 1; g >= 0; g--) {
                    e = a.charAt(g);
                    e = parseInt(e, 10);
                    if (f)
                        if ((e *= 2) > 9) e -= 9;
                    d += e;
                    f = !f
                }
                return d % 10 == 0
            },
            accept: function(a, b, d) {
                d = typeof d == "string" ? d.replace(/,/g, "|") : "png|jpe?g|gif";
                return this.optional(b) || a.match((".(" + d + ")$", "i"))
            },
            equalTo: function(a, b, d) {
                d = c(d).unbind(".validate-equalTo").bind("blur.validate-equalTo", function() {
                    c(b).valid()
                });
                return a == d.val()
            }
        }
    });
    c.format = c.validator.format
})(jQuery);
(function(c) {
    var a = {};
    if (c.ajaxPrefilter) c.ajaxPrefilter(function(d, e, f) {
        e = d.port;
        if (d.mode == "abort") {
            a[e] && a[e].abort();
            a[e] = f
        }
    });
    else {
        var b = c.ajax;
        c.ajax = function(d) {
            var e = ("port" in d ? d : c.ajaxSettings).port;
            if (("mode" in d ? d : c.ajaxSettings).mode == "abort") {
                a[e] && a[e].abort();
                return a[e] = b.apply(this, arguments)
            }
            return b.apply(this, arguments)
        }
    }
})(jQuery);
(function(c) {
    !jQuery.event.special.focusin && !jQuery.event.special.focusout && document.addEventListener && c.each({
        focus: "focusin",
        blur: "focusout"
    }, function(a, b) {
        function d(e) {
            e = c.event.fix(e);
            e.type = b;
            return c.event.handle.call(this, e)
        }
        c.event.special[b] = {
            setup: function() {
                this.addEventListener(a, d, true)
            },
            teardown: function() {
                this.removeEventListener(a, d, true)
            },
            handler: function(e) {
                arguments[0] = c.event.fix(e);
                arguments[0].type = b;
                return c.event.handle.apply(this, arguments)
            }
        }
    });
    c.extend(c.fn, {
        validateDelegate: function(a, b, d) {
            return this.bind(b, function(e) {
                var f = c(e.target);
                if (f.is(a)) return d.apply(f, arguments)
            })
        }
    })
})(jQuery);

// Harvesting page elements. Totally ripped off from innovative user group

var label_ISBN = "ISBN";
var label_title = "Title";
var label_author = "Author/Creator";
var label_OCLC = "OCLC Number";


// preset global variables and make them available to other script

var exist_ISBN = "0"; // exist variables will be used to track whether a particular piece of data has been found and harvested
var bib_ISBN = "0"; //  bib variable will hold the data

var exist_title = "0";
var bib_title = "0";
var bib_title_short = "0";

var exist_author = "0";
var bib_author = "0";

var exist_OCLC = "0";
var bib_OCLC = "0";

var harvest_run = "0";

//harvest data

function milHarvest() {

    harvest_run = "1";
    var container;
    var tr;
    if ($("#untMainContent").length) {
        $container = $("#untMainContent"); // get the container
        tr = $container.find("tr"); // get the rows for every table in the container
    } else {
        tr = $("tr"); // get the rows for every table in the document
    }

    for (var i = 0; i < tr.length; i++) {
        var x = tr[i].getElementsByTagName('TD'); // for each row, get all of the cells

        if (x.length == 2 && $(x[0]).text() == label_ISBN) { // if I have 2 cells one with the ISBN
            bib_ISBN = $(x[1]).text(); // get the ISBN and strip all tags
            bib_ISBN = bib_ISBN.replace(/[\n\t\s\-]/ig, ""); // regex cleanup of the text
            bib_ISBN = bib_ISBN.replace(/\(.*\)/ig, "");
            bib_ISBN = bib_ISBN.replace(/:.*$/ig, "");
            bib_ISBN_long = "ISBN:" + bib_ISBN; // this is helpful for GoogleBooks API
            bib_ISBN_length = bib_ISBN.length
            exist_ISBN = '1'; // flag the variable as populated
        }


        if (x.length == 2 && $(x[0]).text() == label_title) { // if I have 2 cells one with the title
            bib_title = $(x[1]).text(); // get the title and strip all tags 
            bib_title = bib_title.replace(/[\n\t]/ig, "");
            bib_title = bib_title.replace(/\/.*/ig, "");
            bib_title = bib_title.replace(/:.*$/ig, ""); // strip anything following a ':' - usually a subtitle
            bib_title_short = bib_title;
            bib_title = "Title: " + bib_title; // and preface it with "Title: "
            exist_title = '1';
        }

        if (x.length == 2 && $(x[0]).text() == label_author) { // if I have 2 cells one with the author
            bib_author = $(x[1]).text(); // get the author and strip all tags
            bib_author = bib_author.replace(/[\n\t]/ig, "");
            exist_author = '1';
        }

        if (x.length == 2 && $(x[0]).text() == label_OCLC) { // if I have 2 cells one with the OCLC #
            bib_OCLC = $(x[1]).text(); // get the OCLC# and strip all tags
            bib_OCLC = bib_OCLC.replace(/[\n\t]/ig, "");
            bib_OCLC = bib_OCLC.replace(/\(.*\)/ig, "");
            bib_OCLC_long = "OCLC:" + bib_OCLC; // this is helpful for GoogleBooks API
            var OCLCnumTest = /ssj|heb|bks|SSJ|BKS|HEB/; // not all my record nubers are OCLC#s, so I search for 
            var OCLCnumExist = bib_OCLC.search(OCLCnumTest); // the prefixes of the ones that are not, and only set the  
            if (OCLCnumExist == -1) { // exist variable to 1 if the non OCLC prefixes are not present
                exist_OCLC = '1';
            }
        }
    }
}

function milManipulate() {
    if (harvest_run == "1") {

        if (exist_author == '1') {
            $("#worldCatIdentitiesLinks").show().find("a").attr("href", "http://www.worldcat.org/identities/find?fullName=" + bib_author);
        }

        if (exist_OCLC == '1') {
            $("#citationLink").show().find("a").attr("href", "http://worldcat.org/oclc/" + bib_OCLC + "?page=citation").fancybox();
        }

    }
}

// function to generate a qr code, uses api.qrserver.com
var qrCodeGenerator = function() {
    // generates a QR Code for a bibliographic record
    var permalink = $("#permalink").attr("href");
    var url = encodeURI("http://iii.library.unt.edu" + permalink + "?source=qrcode");
    $("#qrcode").attr("src", "//api.qrserver.com/v1/create-qr-code/?data=" + url + "&size=100x100");
}

// compose_GBS -- used on Bib Display screen to forumlate GBS book cover & preview links.

    function compose_GBS(bib_ISBN) {
        $gbsContainer = $("#gbs");
        var api_url = "https://www.googleapis.com/books/v1/volumes?q=isbn:" + bib_ISBN;
        $.ajax(api_url, {
            dataType: "jsonp",
            success: function(data) {
                if (data && data.items && data.items.length) {
                    var gbsimg = '<img class="preview" src="/screens/gbs_preview_button1.gif" alt="Google Preview" />';
                    var item = data.items[0];
                    // Set text and link values depending on viewablity
                    if (item.accessInfo.viewability == "ALL_PAGES") {
                        var urlGBS = item.volumeInfo.previewLink;
                        var linkGBS = gbsimg;
                        var textGBS = '<span class="googleBooksFull googleBooksText">Full View</span>';
                        var classGBS = "GBSfull";
                    }
                    if (item.accessInfo.viewability == "PARTIAL") {
                        var urlGBS = item.volumeInfo.previewLink;
                        var linkGBS = gbsimg;
                        var textGBS = '<span class="googleBooksPartial googleBooksText">Partial View</span>';
                        var classGBS = "GBSpartial";
                    }
                    if (item.accessInfo.viewability == "NO_PAGES" || item.accessInfo.viewability == "UNKNOWN") {
                        var urlGBS = item.volumeInfo.infoLink;
                        var linkGBS = '<div class="googleBooksNoView googleBooksText">More Info from Google Books</div>';
                        var textGBS = "";
                        var classGBS = "GBSnoview";
                    }
                    // Start cover image block.
                    // Get the thumbnail image and make it bigger.
                    if (item.volumeInfo.imageLinks && window.location.protocol == "http:") {
                        var bigthumb = item.volumeInfo.imageLinks.thumbnail;
                        $("#cover").html('<img src="' + bigthumb + '"' + 'alt="Cover image" />');
                    }
                    // End cover image block.
                    // Rewrite the HTML inside the "gbs" <div> element.
                    // Get rid of "imgcode + " if not using cover image.
                    $gbsContainer.append('<a class="' + classGBS + '" href="' + urlGBS + '" target="_blank">' + linkGBS + '</a>' + textGBS + '<div style="clear:both"></div>');

                    // Turn on the display of the GBS link and image
                    $gbsContainer.show();
                }
            }
        });
    }

    // Content Cafe..

    /*contains passwords*/

    function contentcafe(id) {
        switch (this.size) {
            case 'small':
                var size = 'S';
                break;
            case 'medium':
                size = 'M';
                break;
            case 'large':
                size = 'L';
                break;
        }
        var pw = this.config.Contentcafe.pw;
        var url = (this.config.Contentcafe.url) ? this.config.Contentcafe.url : 'http://contentcafe2.btol.com';
        var isbn = this.isn.get13();
        url += "/ContentCafe/Jacket.aspx?UserID=" + id + "}&Password=var " + pw + "}&Return=1" + "" +
            "&Type=var " + size + "}&Value=var " + isbn + "}&erroroverride=1";
        return isbn ? thisvar this.processImageURLForSource(var url, 'contentcafe') : false;
    }

    // Various general utility functions...

    /*getUrlBase*/
var getUrlBase = function() {
    var port = "";
    if (window.location.port && window.location.port != 80) {
        port = ":" + window.location.port;
    }
    return window.location.protocol + "//" + window.location.hostname + port;
}

/*getQueryVariable*/
var getQueryVariable = function(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return false;
}

/* Some session-data-storage functions. Uses a window.name hack to store 
session data. Originally tried using cookies for this but in this case we
need to have states tied to the individual window, in case somebody has
multiple windows/tabs open with catalog records in them. Using cookies caused
collisions.
*/
// Creates the session if it doesn't exist.
Session.prototype.createSession = function() {
    if (!this.is_active) {
        this.win_obj.name = "[" + this.id + "]\"\"";
        this.is_active = true;
    }
}
Session.prototype.deleteSession = function() {
    if (this.is_active) {
        this.win_obj.name = this.win_obj.name.replace(this.id_regex, "");
        this.is_active = false;
    }
}
Session.prototype.getData = function() {
    var data = {};
    var matches = this.win_obj.name.match(this.id_regex);
    if (matches && matches[1]) {
        var keys_vals = matches[1].split("|");
        for (var i = 0; i < keys_vals.length; i++) {
            var pair = keys_vals[i].split("^");
            data[pair[0]] = pair[1];
        }
    }
    return data;
}
Session.prototype.setData = function(data) {
    var pairs = [];
    var matches = this.win_obj.name.match(this.id_regex);
    if (matches) {
        for (var key in data) {
            pairs.push(key + "^" + data[key]);
        }
    }
    this.win_obj.name = this.win_obj.name.replace(this.id_regex, "[" + this.id + "]" + "\"" + pairs.join("|") + "\"");
}
Session.prototype.setVar = function(key, value) {
    this.createSession();
    var data = this.getData();
    data[key] = value;
    this.setData(data);
}
Session.prototype.getVar = function(key) {
    this.createSession();
    var data = this.getData();
    if (typeof(data[key]) !== "undefined") {
        return data[key];
    } else {
        return;
    }
}
Session.prototype.deleteVar = function(key) {
    this.createSession();
    var data = this.getData();
    delete data[key];
    this.setData(data);
}
/* Session object constructor--sets name and window object. Tries to find
content already in window.name for this session.*/

function Session(id, win_obj) {
    this.id = id;
    this.id_regex = new RegExp("\\[" + id + "\\]\"([^\"]*)\"");
    this.win_obj = win_obj;
    this.is_active = false;
    if (id && win_obj) {
        var matches = win_obj.name.match(this.id_regex);
        if (matches) {
            this.is_active = true;
        }
    }
}

// BIB_DISPLAY ****
/* Below is the code to refactor display of bib data in the catalog. It's
divided into three sections: Utility and Processing Functions, Field 
Definitions, and Framework Code.
*/

/*Global Variables*/
global_abbrevs = "[Aa]bdr|[Aa]bgedr|Abb|Abt|[Aa]cc|[Aa]fd|[Aa]fl|[Aa]lt|[Ee]t al|[Aa]pprox|[Aa]pr|[Aa]rg|[Aa]rr|[Aa]tdolg|[Aa]ufl|[Aa]ugm|[Aa]ug|[Aa]um|Ausg|[Aa]vd|Bd|Bdchn|Bde|Bar|[Bb]il|BCD|b&w|[Bb]ogtr|[Bb]oktr|[Bb]k|b|[Bb]ov|[Bb]p|[Bb]pi|[Bb]r|Bro|Bros|Buchdr|Buchh|[Bb]ull|[Cc]ap|c|[Cc]m|[Cc]ent|[Cc]et|[Cc]h|[Cc]a|[Cc]is|[Cc]ol|Cia|Cie|Co|[Cc]f|[Cc]omp|[Cc]orp|[Cc]z|[Dd]ecl|d|Dec|[Dd]ep|Dept|[Dd]ez|[Dd]iam|[Dd]jil|[Dd]oc|[Dd]op|Dr|[Dd]ruk|[Ee]d|[Ee]ds|[Ee]nl|[Ee]q|[Ee]rg|[Ee]rw|[Ee]stab|[Tt]ip|[Ee]tc|[Ee]vf|[Ff]acsim|[Ff]acsims|[Ff]asc|Feb|[Ff]l|[Ff]ol|[Ff]f|[Ff]t|[Ff]r|[Ff]ps|[Ff]lli|Gebr|[Gg]edr|[Gg]eneal|g|[Gg]eo|[Gg]ovt|[Hh]on|Hs|Hss|Hnos|[Hh]r|Hrsg|[Ii]e|Id|[Ii]ll|[Ii]llus|[Ii]llustr|[Ii]m|[Ii]mpr|[Ii]n|[Ii]ps|[Ii]ncl|Inc|[Ii]ntrod|[Ii]zd|[Ii]zm|[Jj]aarg|Jahrg|Jan|[Jj]av|[Jj]il|Joh|[Jj]r|Jul|Jun|Kal|[Kk]iad|[Kk]m|[Kk]n|[Kk]nj|[Kk]ot|[Kk]sieg|l|[Ll]ibr|Lfg|LL|Ltd|[Ll]ivr|Mar|[Mm]ij|[Mm]s|[Mm]ss|[Mm]en|m|Mz|[Mm]ies|[Mm]m|[Mm]in|Mitarb|[Mm]isc|Mme|[Mm]ono|[Mm]r|[Mm]rs|Nachf|[Nn]akl|[Nn]auk|n|Neudr|Nich|[Nn]ew ser|[Nn]o|Non|[Nn]os|[Nn]ouv|Nov|[Nn]umb|[Nn]r|[Nn]uov|[Oo]bd|Oct|[Oo]euv|[Oo]ddz|[Oo]marb|[Oo]pl|[Oo]ppl|[Oo]prac|[Oo]p|[Ii]t|p|[Pp]bk|[Pp]t|[Pp]ts|[Pp]tie|[Pp]ties|[Pp]hoto|[Pp]hotos|[Pp]l|[Pp]opr|[Pp]ort|[Pp]orts|[Pp]osth|[Pp]red|[Pp]ref|[Pp]relim|[Pp]rint|[Pp]riv|[Pp]roj|[Pp]rof|[Pp]ros|[Pp]rzekl|[Pp]rzerob|[Pp]seud|[Pp]ub|[Qq]uad|[Rr]ed|[Rr]ef|[Rr]eimpr|[Rr]ept|[Rr]epr|[Rr]eprod|[Rr]evid|[Rr]ev|[Rr]pm|RA|[Rr]iv|[Rr]oc|[Rr]ocz|r|[Rr]ozsz|Seb|Sebast|[Ss]ec|Sep|Sept|[Ss]er|[Ss]es|[Ss]ig|[Ss]i|[Ss]ic|[Ss]kl|[Gg]l|[Ss]d|[Ss]r|[Ss]tab|[Ss]tereo|[Ss]t|[Ss]tr|[Ss]upt|Docs|[Ss]uppl|[Ss]v|[Ss]z|[Tt]h|[Tt]all|[Gg]raf|[Tt]isk|[Tt]jet|t|[Tt]ow|[Tt]r|[Tt]ypog|[Tt]yp|[Uu]darb|[Uu]dg|[Uu]itg|[Uu]mgearb|[Uu]n|[Uu]nacc|[Uu]ppl|[Uu]tarb|[Uu]tg|[Uu]zup|[Vv]erb|[Vv]erm|v|[Vv]ol|[Vv]ols|[Vv]s|[Vv]uosik|[Vv]yd|Wilh|Wm|[Ww]yd|[Ww]ydawn|[Ww]ydz|[Zz]al|[Zz]esz|[Zz]v";
var global_relator_codes_lookup = {
    "acp": "art copyist",
    "act": "actor",
    "adp": "adapter",
    "aft": "author of afterword, colophon, etc|",
    "anl": "analyst",
    "anm": "animator",
    "ann": "annotator",
    "ant": "bibliographic antecedent",
    "app": "applicant",
    "aqt": "author in quotations or text abstracts",
    "arc": "architect",
    "ard": "artistic director",
    "arr": "arranger",
    "art": "artist",
    "asg": "assignee",
    "asn": "associated name",
    "att": "attributed name",
    "auc": "auctioneer",
    "aud": "author of dialog",
    "aui": "author of introduction",
    "aus": "author of screenplay",
    "aut": "author",
    "bdd": "binding designer",
    "bjd": "bookjacket designer",
    "bkd": "book designer",
    "bkp": "book producer",
    "blw": "blurb writer",
    "bnd": "binder",
    "bpd": "bookplate designer",
    "bsl": "bookseller",
    "ccp": "conceptor",
    "chr": "choreographer",
    "clb": "collaborator",
    "cli": "client",
    "cll": "calligrapher",
    "clr": "colorist",
    "clt": "collotyper",
    "cmm": "commentator",
    "cmp": "composer",
    "cmt": "compositor",
    "cng": "cinematographer",
    "cnd": "conductor",
    "cns": "censor",
    "coe": "contestant -appellee",
    "col": "collector",
    "com": "compiler",
    "con": "conservator",
    "cos": "contestant",
    "cot": "contestant -appellant",
    "cov": "cover designer",
    "cpc": "copyright claimant",
    "cpe": "complainant-appellee",
    "cph": "copyright holder",
    "cpl": "complainant",
    "cpt": "complainant-appellant",
    "cre": "creator",
    "crp": "correspondent",
    "crr": "corrector",
    "csl": "consultant",
    "csp": "consultant to a project",
    "cst": "costume designer",
    "ctb": "contributor",
    "cte": "contestee-appellee",
    "ctg": "cartographer",
    "ctr": "contractor",
    "cts": "contestee",
    "ctt": "contestee-appellant",
    "cur": "curator",
    "cwt": "commentator for written text",
    "dfd": "defendant",
    "dfe": "defendant-appellee",
    "dft": "defendant-appellant",
    "dgg": "degree grantor",
    "dis": "dissertant",
    "dln": "delineator",
    "dnc": "dancer",
    "dnr": "donor",
    "dpb": "distribution place",
    "dpc": "depicted",
    "dpt": "depositor",
    "drm": "draftsman",
    "drt": "director",
    "dsr": "designer",
    "dst": "distributor",
    "dtc": "data contributor",
    "dte": "dedicatee",
    "dtm": "data manager",
    "dto": "dedicator",
    "dub": "dubious author",
    "ed": "editor",
    "edt": "editor",
    "egr": "engraver",
    "elg": "electrician",
    "elt": "electrotyper",
    "eng": "engineer",
    "etr": "etcher",
    "evp": "event place",
    "exp": "expert",
    "fac": "facsimilist",
    "fld": "field director",
    "flm": "film editor",
    "fmo": "former owner",
    "fpy": "first party",
    "fnd": "funder",
    "frg": "forger",
    "gis": "geographic information specialist",
    "-grt": "graphic technician",
    "hnr": "honoree",
    "hst": "host",
    "ill": "illustrator",
    "ilu": "illuminator",
    "ins": "inscriber",
    "inv": "inventor",
    "itr": "instrumentalist",
    "ive": "interviewee",
    "ivr": "interviewer",
    "lbr": "laboratory",
    "lbt": "librettist",
    "ldr": "laboratory director",
    "led": "lead",
    "lee": "libelee-appellee",
    "lel": "libelee",
    "len": "lender",
    "let": "libelee-appellant",
    "lgd": "lighting designer",
    "lie": "libelant-appellee",
    "lil": "libelant",
    "lit": "libelant-appellant",
    "lsa": "landscape architect",
    "lse": "licensee",
    "lso": "licensor",
    "ltg": "lithographer",
    "lyr": "lyricist",
    "mcp": "music copyist",
    "mfp": "manufacture place",
    "mfr": "manufacturer",
    "mdc": "metadata contact",
    "mod": "moderator",
    "mon": "monitor",
    "mrb": "marbler",
    "mrk": "markup editor",
    "msd": "musical director",
    "mte": "metal-engraver",
    "mus": "musician",
    "nrt": "narrator",
    "opn": "opponent",
    "org": "originator",
    "orm": "organizer of meeting",
    "oth": "other",
    "own": "owner",
    "pat": "patron",
    "pbd": "publishing director",
    "pbl": "publisher",
    "pdr": "project director",
    "pfr": "proofreader",
    "pht": "photographer",
    "plt": "platemaker",
    "pma": "permitting agency",
    "pmn": "production manager",
    "pop": "printer of plates",
    "ppm": "papermaker",
    "ppt": "puppeteer",
    "prc": "process contact",
    "prd": "production personnel",
    "prf": "performer",
    "prg": "programmer",
    "prm": "printmaker",
    "pro": "producer",
    "prp": "production place",
    "prt": "printer",
    "prv": "provider",
    "pta": "patent applicant",
    "pte": "plaintiff -appellee",
    "ptf": "plaintiff",
    "pth": "patent holder",
    "ptt": "plaintiff-appellant",
    "pup": "publication place",
    "rbr": "rubricator",
    "rcd": "recordist",
    "rce": "recording engineer",
    "rcp": "recipient",
    "red": "redactor",
    "ren": "renderer",
    "res": "researcher",
    "rev": "reviewer",
    "rps": "repository",
    "rpt": "reporter",
    "rpy": "responsible party",
    "rse": "respondent-appellee",
    "rsg": "restager",
    "rsp": "respondent",
    "rst": "respondent-appellant",
    "rth": "research team head",
    "rtm": "research team member",
    "sad": "scientific advisor",
    "sce": "scenarist",
    "scl": "sculptor",
    "scr": "scribe",
    "sds": "sound designer",
    "sec": "secretary",
    "sgn": "signer",
    "sht": "supporting host",
    "sng": "singer",
    "spk": "speaker",
    "spn": "sponsor",
    "spy": "second party",
    "srv": "surveyor",
    "std": "set designer",
    "stg": "setting",
    "stl": "storyteller",
    "stm": "stage manager",
    "stn": "standards body",
    "str": "stereotyper",
    "tcd": "technical director",
    "tch": "teacher",
    "ths": "thesis advisor",
    "tr": "translator",
    "trc": "transcriber",
    "trl": "translator",
    "tyd": "type designer",
    "tyg": "typographer",
    "uvp": "university place",
    "vdg": "videographer",
    "voc": "vocalist",
    "wam": "writer of accompanying material",
    "wdc": "woodcutter",
    "wde": "wood-engraver",
    "wit": "witness"
};
global_title_parts = [];
global_ajax_field_params = [];

// 1. Utility and Morphing Funtions
/* In this section, are functions used to process field data. This includes 
custom functions called by the framework code ("morph") along with others used
as utility functions. Custom functions each get the following arguments: 1. the
bib_fields data structure; 2. a jQuery object containing the original,
unaltered bib display container div; and 3. a jQuery object containing the
element that gets modified by the function. They should each return the 
modified jQuery object.
*/

// Utility functions go first.
/*reEscape takes a string that's meant to be converted to a RegExp and returns
the string with appropriate characters escaped*/
var reEscape = function(string) {
    string = string.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    return string;
}

/* fieldSelect takes a string--field_name--and finds/returns the containing 
<tr> element from the bib display. Optional element argument lets you specify
a different overall container element in which to conduct the search.
*/
var fieldSelect = function(field_name, element) {
    label_selector = "td.bibInfoLabel";
    el_context = element ? element : $("div.fullPage");
    search_obj = el_context.find(label_selector);
    var regex = new RegExp("^\\s*" + reEscape(field_name));
    var field_tr = search_obj.filter(function() {
        return regex.test($(this).text());
    }).parent().nextUntil("tr:has(" + label_selector + ")").andSelf();
    return field_tr;
}

/* stripBrackets takes a string and strips brackets from it, protecting
brackets surrounding certain phrases (i.e., sic.)*/
var stripBrackets = function(data) {
    if (data) {
        data = data.replace(/\s*\[(electronic resource|slide|filmstrip|motion picture|videorecording|sound recording|microform|realia|et al\.)\]/ig, "");
        data = data.replace(/\[(i\.\s?e\.\s?.*?|sic\.?)\]/, "{$1}");
        data = data.replace(/[\[\]]/g, "");
        data = data.replace(/\{(.*)\}/, "[$1]");
    }
    return data;
}

/* protectPeriods takes a string and replaces periods that don't serve as
structural markers (e.g., those after initials and abbreviations) with a pipe
character so that they are ignored during processing. The second argument is a
boolean value--true means that periods after numbers (Arabic and Roman) should
be protected.*/
var protectPeriods = function(data, has_per_ord_ind) {
    if (data) {
        var num_abbrevs = "b\\.|bib\\.|bulletin\\.|Comm\\.|D\\.|G\\.|j\\.|K\\.|KV\\.|lat\\.|Ms\\.|no\\.|No\\.|nr\\.|Nr\\.|num\\.|Num\\.|oeuv\\.|Op\\.|op\\.|pt\\.|Pt\\.|S\\.|saints\\.|Sarajevo\\.|v\\.|V\\.|ver\\.|vol\\.|Vol\\.|vs\\.|Vs\\.|Wq\\.|z\\.|Band|band|Book|BWV|COMSIG|comunale,|Conference,|Convergence|cxix|D|DASC|Disc|Dogme|Electro|ESSCIRC|fulfilled,|galerija|GALESIA|Group|Heft|HITEN|ICAPP|ICIS|ICONS|ICSLP|IDC|IE|IECON|IEMC|IMAC|IOLTS|IPCC|ISE|ISSLS|ISSSE|ISSTA|ISTAS|ITAB|K.chel|KV|MEMS|MILCOM|MMET|Movement|MSMW|No|Northcon|number|n.mero|N.mero|oeuvre|op|Op|opera|Pagia|part|Part|Preludio|PRS|Radar|Record|Revolution,|RFIT|Season|Seria|Set|Sinfonia|SISPAD|Suppl.ment|SWV|Symphonia|Symphonie|Symphony|tabul.|TENCON|top|tragodiai|TWV|UT|Vol|vol|Volume|volume|Werk|WESCANEX|WoO|WSC";
        var rn_re_string = "(?=[MDCLXVI])M*(C[MD]|D?C{0,3})(X[CL]|L?X{0,3})(I[XV]|V?I{0,3})";
        var ar_re_string = "\\d{1,3}";
        var ne_ar_test_re = new RegExp("(" + num_abbrevs + ")\\s" + ar_re_string + "\\.\\s");
        var ne_rn_test_re = new RegExp("(" + num_abbrevs + ")\\s(" + rn_re_string + ")\\.\\s");
        var e_ar_test_re1 = new RegExp("(^|[\\.\\-&\\/:;=,])\\s" + ar_re_string + "\\.\\s");
        var e_ar_test_re2 = new RegExp("(^|[\\s\\.])([A-Z])\\.\\s" + ar_re_string + "\\.\\s[^\\s\\.,:;\\-]+\\s");
        var e_rn_test_re1 = new RegExp("(^|[\\.\\-&\\/:;=,])\\s(" + rn_re_string + ")\\.\\s");
        var e_rn_test_re2 = new RegExp("(^|[\\s\\.])([A-Z])\\.\\s(" + rn_re_string + ")\\.\\s[^\\s\\.,:;\\-]+\\s");
        var ar_repl_re = new RegExp("((^|[^a-zA-Z0-9'\\-\\/,\\.]+)" + ar_re_string + ")\\.\\s", "g");
        var rn_repl_re = new RegExp("(^|[^a-zA-Z0-9']+)(" + rn_re_string + ")\\.\\s", "g");
        var abbrevs_regex = new RegExp("(^|[^a-zA-Z0-9']+)(" + global_abbrevs + ")\\.", "g");
        var prot_arabic = false;
        var prot_roman = false;
        // if this is non-English material, treat periods after numbers like
        // initials/abbrevs, unless they're years--e.g., 21. Jahrhunderts == 21st
        // Century (includes Roman numerals as well)
        if (has_per_ord_ind) {
            if (!data.match(ne_ar_test_re)) {
                prot_arabic = true;
            }
            if (!data.match(ne_rn_test_re)) {
                prot_roman = true;
            }
            // protect periods after abbreviations
            data = data.replace(abbrevs_regex, "$1$2|");
        } else {
            // protect periods after abbreviations
            data = data.replace(abbrevs_regex, "$1$2|");
            if (data.match(e_ar_test_re1) && !data.match(e_ar_test_re2)) {
                prot_arabic = true;
            }
            if (data.match(e_rn_test_re1) && !data.match(e_rn_test_re2)) {
                prot_roman = true;
            }
        }
        if (prot_arabic) {
            data = data.replace(ar_repl_re, "$1| ");
        }
        if (prot_roman) {
            data = data.replace(rn_repl_re, "$1$2| ");
        }
        // protect periods after initials
        var init_match;
        while (init_match = data.match(/(^|[\-,.\s])([A-Za-z]\.\s?)+/)) {
            data = data.replace(init_match[0], init_match[0].replace(/\./g, "|"));
        }
    }
    return data;
}

/* unprotectPeriods takes a string and replaces | characters placed by
protectPeriods with periods.*/
var unprotectPeriods = function(data) {
    if (data) {
        data = data.replace(/\|/g, ".");
    }
    return data;
}

/* removeInnerWhitespace takes a string and removes whitespace to the left of
punctuation marks.*/
var removeInnerWhitespace = function(data) {
    if (data) {
        data = data.replace(/\s+([,:;\.])/g, "$1");
        data = data.replace(/\s*[.,\/]:/g, ":");
        data = data.replace(/\s*[.,:\/];/g, ";");
    }
    return data;
}

/* chopEnds takes a string and 1. chops space/linebreaks from the front and
back, 2. chops punctuation off the end (but protects periods as needed)*/
var chopEnds = function(data) {
    if (data) {
        data = data.replace(/^\s+/, "");
        data = protectPeriods(data, true);
        data = data.replace(/(\s*[;:,\/\.]?[\s\n]*)+$/, "");
        data = unprotectPeriods(data);
    }
    return data;
}

/* removeEllipses takes a string and strips out ellipses. (...)*/
var removeEllipses = function(data) {
    if (data) {
        data = data.replace(/\s+\.\.\.(\s*(\.))?/g, "$2");
    }
    return data;
}

/* cleanPunct takes a string and attempts to clean up punctuation--removes
periods, semicolons, colons, and commas from the end; removes brackets; removes
extraneous spaces ("Title : subtitle" becomes "Title: subtitle"). It protects
abbreviations/initials from having the period stripped.
*/
var cleanPunct = function(data) {
    // general find and replace
    data = chopEnds(data);
    data = stripBrackets(data);
    data = removeEllipses(data);
    data = removeInnerWhitespace(data);
    return data;
}

/*stripFRBR strips out the parenthetical FRBR entity from a string*/
var stripFRBR = function(data) {
    return data.replace(/\s*\((work|manifestation|expression|item)\)/, "");
}

/*replaceRelatorCodes replaces relator codes with relator terms.*/
var replaceRelatorCodes = function(data) {
    var keys = $.map(global_relator_codes_lookup, function(v, i) {
        return i;
    });
    var relator_regex = new RegExp('[,.]?\\s(' + keys.join('|') + ')[|.]?(,?\\s|$)');
    var term_string = "";
    var relator_match;
    while (relator_match = data.match(relator_regex)) {
        var add_string = global_relator_codes_lookup[relator_match[1]] + (relator_match[2] ? ", " : "");
        data = data.replace(relator_match[0], ", " + add_string);
        term_string += add_string;
    }
    return {
        "data": data,
        "term_string": term_string
    };
}

/* uninvertName takes a string containing an AACR2 name and "uninverts" it if
it can determine that it's in inverted order (Last, First, Dates, etc.).
Returns a string containing the uninverted name. It isn't perfect, but workable
for now.
*/
var uninvertName = function(name, rem_dates) {
    /*console.log("********************************************************");
	console.log(name);*/
    var name_prefix_list = '[Aa]dmiral|[Bb]ishop|[Bb]p\\|?|[Bb]rother|[Cc]ap\'n|[Cc]apt\\|?|[Cc]aptaine?|Dame|Doctor|[Gg]raf|Hon\\|?|Justice|Lady|Madame|Miss|Mister|Mr\\|?|Mrs\\|?|Ms\\|?|Pope|Prof\\|?|Professor|Saint|Sir|Sister|Swami';
    var name_suffix_list = '[Ee]sq|[Jj]r|[Ss]r';
    var name_suffix_abbrevs = 'L\\|?L\\|?\\s?|Ph\\|?\\s?|Phil|Sc|Tech';
    var of_list = 'of|von|van|von der|van der|zu|d\'|da|der|des|de|del|della|de [lL]a|de [lL]as|de [lL]os|of the';
    var partial_noble_title_list = '[Bb]arone?|[Bb]aroness|[Cc]omte|[Cc]omtesse|[Cc]onte|[Cc]ontessa|[Cc]ount|[Cc]ountess|[Ll]ord|[Vv]iscount|[Vv]iscountess|[Vv]icomte|[Vv]icomtesse';
    var adjective_title_list = 'Aquinas|Scholasticus|Scottus|Scotus|Secundas|Smyrnaeus';
    var marc_rel_terms = $.map(global_relator_codes_lookup, function(v, i) {
        return v;
    });
    var paren_title_list = marc_rel_terms.join("|") + '|actress|anglo-norman poet|associate professor of practice|audiovisual translator|baritone|bass|bass player|bassist|biblical prophet|blues musician|calypso singer|cartoonist|catholic priest|children\'s songwriter|christian rapper|clarinetist|computer scientist|cryptographer|detective|disc jockey|dj|double bassist|drummer|economist|filmmaker|graffiti artist|guitarist|harpist|house musician|housing planner|insurance broker|judge|letterer|librarian|mathematician|mechanical engineer|merchant of york|minnesinger|motion picture producer|musical group|notary|novelist|percussionist|person of quality|phd|pianist|producer, screenwriter|professor|professor of logistics|professor of mathematics|rap artist|rap musician|rap vocalist|rapper|reggaetón vocalist|saxophonist|screenwriter|seneca chief|software engineer|soprano|spirit|tamil film director|trombonist|trumpet player|turntablist|ukelele player|wrestler|writer|yi shu |yoga therapist';
    var dates_regexp = new RegExp('(^|,?\\s)([a-z0-9\\-][^,]*\\d[^,]+)(,|$)');
    var name_prefix_regexp = new RegExp(',\\s((' + name_prefix_list + ')\\|?)(,\\s|\\s\\{|$)');
    var name_suffix_regexp = new RegExp('(,\\s(((' + name_suffix_list + ')|(([A-Z]|' + name_suffix_abbrevs + ')\\|?){2,})\\|?))(,\\s|\\s\\{|$)');
    var full_noble_titles_regexp = new RegExp('^[^,]+,\\s[^,]+(,?\\s([^\\s]+\\s(' + of_list + ')))(,\\s|\\s\\{|$)');
    var partial_noble_titles_regexp = new RegExp('(,\\s|^)(' + partial_noble_title_list + ')(,\\s|\\s\\{|$)');
    var adjective_titles_regexp = new RegExp('(,\\s((' + of_list + ')\\s[^,{]+)|,?\\s(' + adjective_title_list + '))(,\\s|\\s\\{|$)');
    var name_proper_regexp = new RegExp('^(([^,{]+)(,\\s)?([^,{]+)?)(,\\s|\\s\\{|$)');
    var name_proper_regexp2 = new RegExp('^([\\s()]*[A-Z][^\\s]*)+[\\s()]*(' + of_list + ')?(\\s\\(([A-Z][^\\s]+\\s?)+\\))?$');
    var other_titles_regexp = new RegExp('\\s\\((' + paren_title_list + ')\\)(\\s|$)', 'i');
    var dregs_regexp = new RegExp('^([^{]+)?((\\s?\\{(.+)\\})\\s?(.*)?)?$');
    var name_prefixes = "",
        name_proper = "",
        adjective_titles = "",
        full_noble_titles = "",
        name_suffixes = "",
        other_titles = "",
        dates = "",
        roles = "";
    var name_prefixes_array = [],
        name_suffixes_array = [];
    // Just a little cleanup on the name string first.
    name = chopEnds(name);
    name = protectPeriods(name);
    name = name.replace(".", ",");
    name = name.replace(/([^,]),([^0-9\s])/g, "$1, $2");
    var rel_code_results = replaceRelatorCodes(name);
    name = rel_code_results.data;
    // Find dates, strip beginning and ending punctuation, and put them in curly braces.
    var dates_match = name.match(dates_regexp);
    if (dates_match) {
        name = name.replace(dates_match[0], " {" + dates_match[2] + "}");
    }
    // Parse out name_suffixes and build an array of suffixes
    var name_suffixes_match;
    while (name_suffixes_match = name.match(name_suffix_regexp)) {
        name = name.replace(name_suffixes_match[1], "");
        if (name_suffixes_match[2]) {
            name_suffixes_array.push(name_suffixes_match[2]);
        }
    }
    // Parse out name_prefixes and build an array of prefixes
    // E.g., *Sir* *Saint* Thomas More
    var name_prefixes_match;
    while (name_prefixes_match = name.match(name_prefix_regexp)) {
        name = name.replace(name_prefixes_match[0], name_prefixes_match[3]);
        if (name_prefixes_match[2]) {
            name_prefixes_array.push(name_prefixes_match[2].charAt(0).toUpperCase() + name_prefixes_match[2].slice(1));
        }
    }
    // Parse out full_noble_titles, based on presence of "of" (or a translation)
    // E.g.: John Smith, *Baron of Walgren* -- or Johann *Ritter von Walgren*
    var full_noble_titles_match = name.match(full_noble_titles_regexp);
    if (full_noble_titles_match) {
        name = name.replace(full_noble_titles_match[1], "");
        var temp_name_match = name.match(/^([^,]+?)(,\s|\s\{|$)/);
        name = temp_name_match[2] == " {" ? name.replace(temp_name_match[0], "{") : name.replace(temp_name_match[0], "");
        full_noble_titles = full_noble_titles_match[2] + " " + temp_name_match[1];
    }
    // These titles may need to be name_prefixes or full_noble_titles, depending on the structure of the name.
    // E.g.: *Baron* John Walgren versus John Walgren, *Baron Walgren*
    var partial_noble_titles_match = name.match(partial_noble_titles_regexp);
    if (partial_noble_titles_match) {
        name = name.replace(partial_noble_titles_match[0], partial_noble_titles_match[3]);
        var temp_name_match = name.match(/^([^,]+),([^(,]+)?[(,]?\s+/);
        if (temp_name_match && temp_name_match[2] && temp_name_match[2].match(/([A-Z]\S+(\s|$)){2,}/)) {
            full_noble_titles = partial_noble_titles_match[2] + " " + temp_name_match[1];
            name = name.replace(/^([^,]+,\s?)/, "");
        } else {
            name_prefixes_array.push(partial_noble_titles_match[2].charAt(0).toUpperCase() + partial_noble_titles_match[2].slice(1));
        }
    }
    // adjective titles
    // E.g.: Eusebius *of Caesarea* -- or Socrates *Scholasticus*
    var adjective_titles_match = name.match(adjective_titles_regexp);
    if (adjective_titles_match) {
        adjective_titles = adjective_titles_match[2] ? adjective_titles_match[2] : adjective_titles_match[4] ? adjective_titles_match[4] : "";
        name = adjective_titles_match[5] ? name.replace(adjective_titles_match[0], adjective_titles_match[5]) : name.replace(adjective_titles_match[0], "");
    }
    // At this point we parse out the name proper versus any additional titles/descriptors (other_titles)
    // and then uninvert the name proper, if appropriate.
    // E.g.: George II, King of England (George II = name proper, Kind of England = other_titles)
    // E.g.: Smith, John (John Smith = name proper)
    var name_proper_match = name.match(name_proper_regexp);
    if (name_proper_match) {
        if (name_proper_match[4]) {
            if (!name_proper_match[4].match(name_proper_regexp2)) {
                name_proper = name_proper_match[2];
                other_titles = name_proper_match[4];
            } else {
                name_proper = name_proper_match[4] + " " + name_proper_match[2];
            }
        } else {
            name_proper = name_proper_match[2];
        }
        name = (name_proper_match[5] && name_proper_match[5] == " {") ? name.replace(name_proper_match[0], "{") : name.replace(name_proper_match[0], "");
    }
    // Parse out the rest of the name string (the "dregs") to get: additional other_titles (anything left before dates),
    // dates (in {}s), and roles (anything after dates)
    dregs_match = name.match(dregs_regexp);
    if (dregs_match) {
        if (dregs_match[3]) {
            other_titles += dregs_match[1] ? (other_titles ? ", " : "") + $.trim(dregs_match[1]) : "";
            dates = dregs_match[4];
            roles = dregs_match[5] ? dregs_match[5] : "";
        } else {
            if (dregs_match[1] && dregs_match[1].match(/^[A-Z]/)) {
                other_titles += (other_titles ? ", " : "") + dregs_match[1];
            } else {
                roles = dregs_match[1];
            }
        }
        var temp_roles_match;
        while (roles && (temp_roles_match = roles.match(/(^|\s)([A-Z])/))) {
            roles = roles.replace(temp_roles_match[0], temp_roles_match[1] + temp_roles_match[2].toLowerCase());
        }
    }
    // Finally...deal with statements in parentheses in name_proper that are other titles
    var other_titles_match = name_proper ? name_proper.match(other_titles_regexp) : "";
    if (other_titles_match) {
        other_titles += "(" + other_titles_match[1] + ")";
        name_proper = other_titles_match[2] ? name_proper.replace(other_titles_match[0], other_titles_match[2]) : name_proper.replace(other_titles_match[0], "");
    }
    // build the final name
    name_prefixes = name_prefixes_array.join(" ");
    name_suffixes = name_suffixes_array.join(", ");
    var name_and_noble_titles = name_proper.charAt(0).toUpperCase() + name_proper.slice(1) + (adjective_titles ? (name_proper ? " " : "") + adjective_titles : "");
    name_and_noble_titles += full_noble_titles ? (name_and_noble_titles ? (full_noble_titles.match(/\s(v[oa]n)\s/) ? " " : ", ") : "") + full_noble_titles : "";
    var final_name = name_prefixes + (name_and_noble_titles ? (name_prefixes ? " " : "") + name_and_noble_titles : "");
    final_name += name_suffixes ? (final_name ? ", " : "") + name_suffixes : "";
    final_name += other_titles ? (final_name ? (!other_titles.match(/^\(/) ? ", " : " ") : "") + other_titles : "";
    final_name += dates && !rem_dates ? (final_name ? ", " : "") + dates : "";
    final_name += roles ? (final_name ? ", " : "") + roles : "";

    // a little bit of cleanup...
    final_name = final_name.replace(/ de [Ll]a /g, " de la ");
    final_name = final_name.replace(/ de [Ee]l /g, " del ");
    final_name = final_name.replace(/ d' ([A-Z])/g, " d'$1");
    final_name = unprotectPeriods(final_name);
    /*console.log("name_prefixes: " + name_prefixes);
	console.log("name_proper: " + name_proper);
	console.log("adjective_titles: " + adjective_titles);
	console.log("full_noble_titles: " + full_noble_titles);
	console.log("name_suffixes: " + name_suffixes);
	console.log("other_titles: " + other_titles);
	console.log("dates: " + dates);
	console.log("roles: " + roles);
	console.log("final_name: " + final_name);*/
    return final_name;
}

/*parseCorpName parses out and then rebuilds Corporate Names. All we can
really do is parse out roles...*/
var parseCorpName = function(name) {
    /*console.log("********************************************************");
	console.log(name);*/
    var final_name = "",
        roles = "";
    var role_regexp = new RegExp(',?\\s(([a-z][^,).|\\s\\]]+,?\\s?)+)$');
    // Just a little cleanup on the name string first.
    name = chopEnds(name);
    name = protectPeriods(name);
    name = name.replace(/([^,]),([^0-9\s])/g, "$1, $2");
    var rel_code_results = replaceRelatorCodes(name);
    name = rel_code_results.data;
    // Parse out roles, attached to the end of the string.
    var role_match = name.match(role_regexp);
    if (role_match) {
        roles = role_match[1];
        name = name.replace(role_match[0], "");
    }
    // Make sure that any periods left within parentheses are protected.
    var paren_match = name.match(/\(.+\)/);
    name = paren_match ? name.replace(paren_match[0], paren_match[0].replace(/\./g, "|")) : name;
    name = name.replace(/\.\s?/g, " &mdash; ");
    final_name = unprotectPeriods(name + (roles ? ", " : "") + roles);
    /*console.log(roles);
	console.log(final_name);*/
    return final_name;
}

/*findPerOrdInd -- or "find period ordinal indicator" -- takes a MARC 008 and a
MARC 041 JQ element and attempts to find a code indicating a language for the
item/record in question that uses a period for an ordinal indicator. Returns a
boolean true/false.*/
var findPerOrdInd = function(marc008_el, marc041_el) {
    var marc008 = marc008_el.text();
    var marc041 = marc041_el.text();
    // check the 008--if it's blank, eng, N/A, zxx, or |||, look in the 041
    var language = marc008.substring(36, 39);
    if (!language || language == "eng" || language == "N/A" || language == "zxx" || language == "|||" || language.match(/^\s+$/)) {
        var more_langs = marc041.replace(/\s/g, "").replace(/(...)/g, "$1|").split("|");
        for (var i in more_langs) {
            if (more_langs[i] && more_langs[i] != "eng" && more_langs[i] != "zxx") {
                language = more_langs[i];
                break;
            }
        }
    }
    if (language != "eng") {
        return true;
    } else {
        return false;
    }
}

/*procTitlePunct takes a title string and boolean for "has period ordinal
indicator," from the findPerOrdInd function. It preprocesses punctuation to
make sure chunkTitleString will work correctly. Normally this is just called
via chunkTitleString, but it can be called by itself. In that case, make sure
to: do something with the SOR (everything after the " / "), convert periods
before the SOR to commas, and convert | back to .*/
var procTitlePunct = function(title, has_per_ord_ind) {
    // convert &amp; to & (the extra ; can interfere with processing)
    title = title.replace(/&amp;/g, "&");
    title = stripBrackets(title);
    title = removeEllipses(title);
    title = protectPeriods(title, has_per_ord_ind);
    title = removeInnerWhitespace(title);
    // where there are ." ," or ;" (or single-qoute equiv.) we flip them around
    title = title.replace(/([\.,;])(['"])/g, "$2$1");
    return title;
}

/* findTitleBy takes a title string. If there's no / to tell us where the title
ends and names begin, we need to figure out where to add it--the regex infers
where based on the presence of the word "by" followed by a capitalized word
anywhere in a clause/phrase after a period, comma, or semicolon*/
var findTitleBy = function(title) {
    if (!title.match(/\s\/\s/)) {
        if (title.match(/[\.;,]([^\.;,]*\s([Bb]y|[Vv]on)\s[A-Z])/)) {
            title = title.replace(/[\.;,]([^\.;,]*\s([Bb]y|[Vv]on)\s[A-Z])/, " /$1");
        }
    }
    return title;
}

/*chunkTitleString takes a string containing a 245 title. Returns an array,
where the first element contains the title portion and the subsequent elements
contain each SOR found in the title string.*/
var chunkTitleString = function(title, language) {
    title = procTitlePunct(title, language);
    title = findTitleBy(title);
    var title_chunks = title.split(/\s\/\s/);
    var final_title_array = [];
    title_chunks[0] = title_chunks[0].replace(/[\.\s]+$/, "");
    title_chunks[0] = title_chunks[0].replace(/\.\s/g, ", ");
    title_chunks[0] = unprotectPeriods(title_chunks[0]);
    final_title_array.push(title_chunks[0]);
    if (title_chunks[1]) {
        for (i = 1; i < title_chunks.length; i++) {
            var sor = "";
            var new_title = "";
            if (title_chunks[i].match(/\.\s/)) {
                sor = title_chunks[i].replace(/^(.*?)\.\s.*$/, "$1");
                new_title = title_chunks[i].replace(/^.*?\.\s(.*)$/, "$1");
            } else {
                sor = title_chunks[i].replace(/\.+\s*$/, "");
            }
            if (sor) {
                sor = unprotectPeriods(sor);
                final_title_array.push(sor);
            }
            if (new_title && new_title != -1) {
                new_title = new_title.replace(/\.\s/g, ", ");
                new_title = new_title.replace(/\.$/, "");
                new_title = unprotectPeriods(new_title);
                final_title_array[0] += "; " + new_title;
            }
        }
    }
    return final_title_array;
}

/*normalizeTitle takes a title string and attempts to normalize it--protects
periods, strips out all other punctuation, replaces beginning a/an/the, and
converts to lower case*/
var normalizeTitle = function(title) {
    title = title.replace(/^\s*(.*?)\s*$/, "$1");
    title = protectPeriods(title, false);
    title = title.replace(/[\n,\.\[\]\(\):;!?\/\\<>]/g, "");
    title = title.replace(/^(a|an|the)\s*/i, "");
    title = title.toLowerCase();
    return title;
}

/*normalizeName takes a name string and attempts to normalize it--protects
periods, removes relator codes and relationship designators, strips out other
punctuation, and converts to lower case*/
var normalizeName = function(name) {
    name = chopEnds(name);
    name = protectPeriods(name, false);
    name = name.replace(/,\s*[A-Za-z()\s]+$/, "");
    name = name.replace(/[\n,\.\[\]\(\):;!?\/\\<>]/g, "");
    name = name.toLowerCase();
    return name;
}

/* rowsToCell takes a field_tr jQuery object that has multiple values given
in multiple rows, and a delimiter string. It collapses the rows to a single
cell of delimited values.
*/
var rowsToCell = function(field_tr, delimiter) {
    var output_string = "";
    field_tr.find("td.bibInfoData").each(function(i) {
        var html = $(this).html();
        html = html.replace(/[\s\n]*$/, "");
        if (i != 0) {
            output_string += delimiter;
            $(this).parent().empty().remove();
        }
        output_string += html;
    });
    field_tr.find("td.bibInfoData").html(output_string);
    return field_tr;
}

/* getText takes a jQuery object and a delimiter character. It returns the text
from the jQuery object. In cases where the object has children <a> tags, it
concatenates the text from each child and places the delimiter character
between concatenations. (This is so that the text can be processed as a single
entity when needed, and the HTML can be rebuilt with putText.)
*/
var getText = function(jq_obj, delimiter) {
    var text = "";
    if (jq_obj.children("a").length) {
        jq_obj.children("a").each(function() {
            text += $(this).text() + delimiter;
        });
        text = text.substr(0, text.length - delimiter.length);
    } else {
        text = jq_obj.text();
    }
    return text;
}

/* putText takes a text string extracted via getText(), the jQuery object from
which it was extracted, and a delimiter character. It breaks the text string
into its constituent pieces based on the delimiter and inserts each piece back
into the jQuery object's HTML. Only works on <a> tags.
*/
var putText = function(text, jq_obj, delimiter) {
    if (jq_obj.children("a").length) {
        var text_chunks = text.split(delimiter);
        jq_obj.children("a").each(function(i) {
            $(this).html(text_chunks[i]);
        });
    } else {
        jq_obj.empty().append(text);
    }
    return jq_obj;
}

/* processText serves as a wrapper for doing text processing in the custom
Morphing functions. It takes three arguments, the last of which is optional.
First is the jQuery object containing the element with text you want to
process. Second is a function containing the code you want to run on the bare
text. Third is some sort of other data that needs to be passed to your text-
processing function (string, object, array, etc.). processText automatically
preserves HTML <a> tags as well as the HTML formatting for query term matches
so that the Morphing functions don't need to worry about it.
*/
var processText = function(element, routines, other_data) {
    var query_tag = {
        "open": "<font color=\"RED\"><strong>",
        "close": "</strong></font>"
    };
    var query_matches = [];
    element.find("font strong").each(function() {
        query_matches.push($(this).text());
        $(this).replaceWith($(this).text());
    });
    text = getText(element, "__");
    text = routines(text, other_data);
    for (var i in query_matches) {
        var query_regex = new RegExp("(^|[^a-zA-Z0-9_<>])(" + reEscape(query_matches[i]) + ")([^a-zA-Z0-9_<>]|$)", "g");
        if (text.match(query_regex)) {
            text = text.replace(query_regex, "$1" + query_tag.open + "$2" + query_tag.close + "$3");
        }
    }
    element = putText(text, element, "__");
    return element;
}

/*addFieldLabel takes a field_tr jQuery object and an option label string. If 
the label is not provided, it reads the field label from the appropriate cell.
Then it rewrites the label into the actual data cell (prepends it) and affixes
a : to the end. Assumes that you're hiding the original field label.*/
var addFieldLabel = function(field_tr, field_label) {
    if (!field_label) {
        field_label = $.trim(field_tr.find("td").first().text());
    }
    field_tr.find("td.bibInfoData").first().prepend("<span class=\"bibHeadingLabel\">" + field_label + ": </span>");
    return field_tr;
}

/*getBibId takes a string with a bib ID embedded in it and returns the bib ID*/
var getBibId = function(string) {
    if (string) {
        var matches = string.match(/(b\d{6,7})(\D|$)/);
        if (matches && matches[1]) {
            return matches[1];
        } else {
            return "";
        }
    } else {
        return "";
    }
}

/*buildRemoteLink takes a jQuery object that is an anchor with a link to the
Remote Storage Request form. It adds the url query parameter to the end of
the request URL and returns the object.*/
var buildRemoteLink = function(anchor, bib_id, title, author, call_num, barcode) {
    var url_base = getUrlBase();
    var url = anchor.attr("href") + encodeURI("?url=" + url_base + "/record=" + bib_id + "&title=" + title + "&author=" + author + "&call_number=" + call_num + "&barcode=" + barcode);
    anchor.attr("href", url);
    return anchor;
}

/*linkify takes a jQuery object that contains content and replaces email addys
and URLs with linked versions. Returns the object.*/
var linkify = function(entities) {
    // define skipIt -- function used to compare found URLs to any pre-existing links.
    // e.g., we may match a URL in the text that's already enclosed in an <a> tag,
    // in which case we don't want to linkify that URL.
    var skipIt = function(obj, test) {
        var val = false;
        $(obj).find("a").each(function() {
            if ($(this).text().match(test)) {
                val = true;
            }
        });
        return val;
    };
    entities.each(function() {
        var pattern = /(\w+\.\w+@\w+\.\w+)|(https?:\/\/[^\s]+)/g; // group 1 = email address, group 2 = URL
        var match;
        while (match = pattern.exec($(this).text())) {
            var m_text = match[1] ? match[1] : match[2] ? match[2] : "";
            var mailto = match[1] ? "mailto:" : "";
            if (m_text && !skipIt($(this), m_text)) {
                // trim trailing punctuation
                m_text = m_text.replace(/(\)[:,;\.])$|(\][:,;\.])$|([:,;.)\]])$/, "");
                $(this).html($(this).html().replace(m_text, "<a class=\"textLink\" href=\"" + mailto + m_text + "\">" + m_text + "</a>"));
            }
        }
    });
    return entities;
}

// Morphing functions go next.
/* morphDefault serves as the default text processor for most fields. Right
now it passes the text through cleanPunct and stripFRBR.*/
var morphDefault = function(fields, original_div, field_tr) {
    field_tr.find("td.bibInfoData").each(function() {
        processText($(this), function(text) {
            return cleanPunct(stripFRBR(text));
        });
    });
    return field_tr;
}

/* morphDefaultRowsToCell calls morphDefault and then rowsToCell.*/
var morphDefaultRowsToCell = function(fields, original_div, field_tr) {
    field_tr = morphDefault(fields, original_div, field_tr);
    field_tr = rowsToCell(field_tr, "| ");
    return field_tr;
}

/* morphTitle cleans up punctuation and parses the 245 statement of 
responsibility (SOR) implicitly, using punctuation (" / " being what sets off 
the SOR). It chunks out the 245 and saves the resulting array to the global
global_title_parts.
*/
var morphTitle = function(fields, original_div, field_tr) {
    var has_per_ord_ind = findPerOrdInd(fieldSelect("008", original_div).find("td.bibInfoData"), fieldSelect("041", original_div).find("td.bibInfoData"));
    field_tr.find("td.bibInfoData").each(function() {
        processText($(this), function(text, object) {
            var title_parts = chunkTitleString(text, has_per_ord_ind);
            global_title_parts = title_parts;
            return title_parts[0];
        }, $(this));
    });
    return field_tr;
}


/* morphSubject creates a secondary link icon to the actual subject search by 
adding a pipe | character. 
*/
var morphSubject = function(fields, original_div, field_tr) {
    field_tr.find("td.bibInfoData").each(function() {
        var subjectNearby = $(this).find("a").addClass("subjectNearby"),
            subjectText = chopEnds(subjectNearby.text().replace(/\-\-/g, "&mdash;"));
        subLink = $("<a class=\"theSubject\" href=\"/search/?searchtype=d&SORT=D&searcharg=" + subjectText + "|\">" + subjectText + "</a>");
        subLink.insertBefore(subjectNearby);
        subjectNearby.empty().text('See also').hide();
        $(this).hover(function() {
            subjectNearby.css('display', 'block');
        }, function() {
            subjectNearby.hide();
        });
    });
    field_tr.find("td.bibInfoLabel").prepend("<a title=\"Help\" class=\"fancybox_subjecthelp\"><span class=\"ss_sprite ss_help\">&nbsp;</span></a>");
    field_tr.find("td.bibInfoLabel a.fancybox_subjecthelp").attr("href", "#subjectDialog"); // adding the href attr this way for IE7
    return field_tr;
}

/* morphPubInfo cleans up the Publication Information field--removes the
ending period, removes spaces before :, and changes the "c" before the
copyright date to a copyright symbol.
*/
var morphPubInfo = function(fields, original_div, field_tr) {
    field_tr.find("td.bibInfoData").each(function() {
        processText($(this), function(text) {
            text = cleanPunct(text);
            text = text.replace(/c(\d\d\d\d)/g, "&copy;$1");
            return text;
        });
    });
    var label = field_tr.find("td.bibInfoLabel").text();
    field_tr = addFieldLabel(field_tr, label);
    return field_tr;
}

/*morphNotesField is the morph function for any/all Notes or notes-related
fields. It replaces any unlinked email addresses and/or URLs with hyperlinked
versions. Also parses RDA 502 (dissertation notes) fields.*/
var morphNotesField = function(fields, original_div, field_tr) {
    field_tr.each(function() {
        content_td = $(this).find("td").last();
        content_td = linkify(content_td);
        // Is this a 502? If so, parse it out.
        var matches = content_td.text().match(/^\s*(([^`~0-9\s!@#$%^&*()=+{}\[\]|\\;:'",<.>\/?]+\.\s*)+)(([^`~0-9\s!@#$%^&*()=+{}\[\]|\\;:'",<.>\/?]+,?\s)+)(\d+)\.\s*$/);
        if (matches) {
            var degree = matches[1].replace(/\s+/g, "").replace(/\./g, ". ").replace(/\s*$/, "");
            var institution = matches[3].replace(/,?\s*$/, "");
            var month = matches[4].replace(/,?\s*$/, "");
            var date = matches[5];
            if (month.match(/^(january|jan|february|feb|march|mar|april|apr|may|june|jun|july|jul|august|aug|september|sep|sept|october|oct|november|nov|december|dec)/i)) {
                institution = institution.replace(month, "").replace(/\s*$/, "");
                date = month + " " + date;
            }
            content_td.html(degree + "&mdash;" + institution + ", " + date + ".");
        }
    });
    return field_tr;
}

/*morphSummaryNotesField is used for notes fields that appear in the Summary
table that AREN'T the summary. Adds a nice little label to the field and then
calls morphNotesField.*/
var morphSummaryNotesField = function(fields, original_div, field_tr) {
    field_tr = addFieldLabel(field_tr);
    field_tr = morphNotesField(fields, original_div, field_tr);
    return field_tr;
}

/*morph34XField -- Note that this function works on ajax MARC fields; that is,
those copied from the MARC display to pull in subfields. Removes |2 and
replaces additional subfield delimiters with semicolons.*/
var morph34XField = function(fields, original_div, field_tr) {
    field_tr.find("td.bibInfoData").each(function() {
        processText($(this), function(text) {
            return text.replace(/\|2.*?(\||$)/, "$1").replace(/^\|[a-z0-9]/, "").replace(/\|[a-z0-9]/g, "; ").replace(/;+/g, ";");
        });
    });
    return field_tr;
}

/*morphBibDetailsTopField serves as the morph function for the fields in the
Bib Details Top table. It calls morphDefault, then addFieldLabel, then 
rowsToCell.*/
var morphBibDetailsTopField = function(fields, original_div, field_tr) {
    field_tr = morphDefault(fields, original_div, field_tr);
    field_tr = addFieldLabel(field_tr);
    field_tr = rowsToCell(field_tr, " | ");
    field_tr.find("td.bibInfoData a").css({
        'padding': '0'
    });
    return field_tr;
}

/* morphAuthor uninverts the author name and cleans the punctuation.
 */
var morphAuthor = function(fields, original_div, field_tr) {
    field_tr.find("td.bibInfoData").each(function() {
        processText($(this), function(text) {
            return stripFRBR(uninvertName(cleanPunct(text)));
        });
    });
    if (fieldSelect("Corporate Author", original_div).length) {
        fieldSelect("Corporate Author", original_div).find("td.bibInfoData").each(function() {
            processText($(this), function(text) {
                text = stripFRBR(parseCorpName(cleanPunct(text)));
                return text;
            });
            var ca_tr = $(document.createElement("tr"));
            if (field_tr.find("td.bibInfoData").length) {
                ca_tr = ca_tr.append("<td></td>");
            } else {
                ca_tr = ca_tr.append("<td class=\"bibInfoLabel\" width=\"20%\" valign=\"top\"></td>");
            }
            ca_tr.append("<td class=\"bibInfoData\">" + $(this).html() + "</td>");
            field_tr = field_tr.after(ca_tr);
        });
    }
    field_tr = rowsToCell(field_tr, "| ");
    return field_tr;
}

/* morphContributors does the following. First grabs and normalizes the Author
and/or Corporate Author names. Then loops through the existing contributors,
uninverting each name and stripping FRBR entities from relationship
designators. Then loops through Corporate Contributors to add each one to
Contributors. Then loops through Included Works to grab more contributor names.
At each step it normalizes and compares Contributor names to Author, Corporate
Author, and other Contributor names to make sure there are no duplicates.
Finally, it calls rowsToCell to collapse the rows into a pipe-delimited listing
in one cell.
*/
var morphContributors = function(fields, original_div, field_tr) {
    var author = "";
    fieldSelect("Author/Creator", original_div).find("td.bibInfoData").each(function() {
        author = normalizeName(uninvertName(cleanPunct($(this).text())));
    });
    var corp_author = "";
    fieldSelect("Corporate Author", original_div).find("td.bibInfoData").each(function() {
        corp_author = normalizeName(cleanPunct($(this).text()));
    });
    var contributors = [];
    field_tr.find("td.bibInfoData").each(function() {
        var anchors = $(this).children("a");
        if ((anchors.length == 1)) {
            processText($(this), function(text) {
                return stripFRBR(uninvertName(cleanPunct(text)));
            });
            if ($.inArray(normalizeName($(this).text()), contributors) == -1 && normalizeName($(this).text()) != author) {
                contributors.push(normalizeName($(this).text()));
            } else {
                $(this).parent().empty().remove();
            }
        } else {
            $(this).parent().empty().remove();
        }
    });
    if (fieldSelect("Corporate Contributors", original_div).length) {
        fieldSelect("Corporate Contributors", original_div).find("td.bibInfoData").each(function() {
            var anchors = $(this).children("a");
            if (anchors.length == 1) {
                processText($(this), function(text) {
                    text = stripFRBR(parseCorpName(cleanPunct(text)));
                    return text;
                });
                if ($.inArray(normalizeName($(this).text()), contributors) == -1 && normalizeName($(this).text()) != corp_author) {
                    var cc_tr = $(document.createElement("tr"));
                    if (contributors.length) {
                        cc_tr = cc_tr.append("<td></td>");
                    } else {
                        cc_tr = cc_tr.append("<td class=\"bibInfoLabel\" width=\"20%\" valign=\"top\"></td>");
                    }
                    cc_tr.append("<td class=\"bibInfoData\">" + $(this).html() + "</td>");
                    contributors.push(normalizeName($(this).text()));
                    field_tr.push(cc_tr[0]);
                }
            }
        });
    }
    fieldSelect("Included Works", original_div).find("td.bibInfoData").each(function() {
        var anchors = $(this).children("a");
        var a1_text = anchors.first().text();
        if ((anchors.length > 1) && a1_text.match(/^(.*?)(\.|\d+-)/)) {
            processText(anchors.first(), function(text) {
                return stripFRBR(uninvertName(cleanPunct(text)));
            });
            a1_text = anchors.first().text();
            if ($.inArray(normalizeName(a1_text), contributors) == -1 && normalizeName(a1_text) != author && normalizeName(a1_text) != corp_author) {
                var new_tr = $(document.createElement("tr")).append("<td class=\"bibInfoLabel\" width=\"20%\" valign=\"top\"></td>")
                    .append($(document.createElement("td")).addClass("bibInfoData").append(anchors.first()));
                contributors.push(normalizeName(a1_text));
                field_tr.push(new_tr[0]);
            }
        }
    });
    if (field_tr.find("td").length == 0) {
        field_tr.empty().remove().hide();
    } else if (field_tr.find("td.bibInfoLabel").length == 0) {
        field_tr.find("td").first().text("Contributors");
        field_tr.find("td").first().addClass("bibInfoLabel");
    }
    field_tr = rowsToCell(field_tr, "| ");
    return field_tr;
}

/* morphIncludedWorks, in conjunction with morphContributors, attempts to deal
with the basic MARC 700 analytic title entry issue. This pattern is assumed:
"Name_string. Title_string". It shortens the name to last name and creates a
title entry like this: Hamlet (Shakespeare). It also tries to dedupe titles 
(since we are pulling data for this field from multiple MARC fields and there
may be duplicate titles).
*/
var morphIncludedWorks = function(fields, original_div, field_tr) {
    var titles = [];
    field_tr.find("td.bibInfoData").each(function() {
        var anchors = $(this).children("a");
        var title = "";
        if (anchors.length > 1) { //this should be a 700 or 710
            anchors.each(function(i) {
                if (i > 0) {
                    title += $(this).text() + " ";
                }
            });
            title = normalizeTitle(title);
            if ($.inArray(title, titles) == -1) {
                titles.push(title);
            }
            processText(anchors.first(), function(a1_text) {
                if ((a1_text.match(/^(.*?)(\.|\d+-)/))) {
                    a1_text = a1_text.replace(/(.*?),(.*)$/, "$1");
                    a1_text = a1_text.replace(/\.$/, "");
                    a1_text = "(" + a1_text + ")";
                }
                return a1_text;
            });
            processText(anchors.last(), function(text) {
                return cleanPunct(text);
            });
            $(this).append(anchors.first());
        } else if (anchors.length == 1) { //this should be a 730, 740, or 774
            title = normalizeTitle(anchors.text());
            title_regex = new RegExp(title.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"));
            for (var i in titles) {
                var comp_title_regex = new RegExp(titles[i].replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&"));
                if (titles[i].match(title_regex) || title.match(comp_title_regex)) {
                    $(this).empty().remove();
                    break;
                }
            }
        }
        $(this).contents().filter(function() {
            return this.nodeType == 3;
        }).text(function(i, text) {
            this.nodeValue = text.match(/\((work|expression|manifestation|item)\)/) ? stripFRBR(text) : "";
        });
    });
    field_tr = rowsToCell(field_tr, "| ");
    return field_tr;
}

/*morphRelatedWorks is similar to morphContributors--it pulls titles out of the
Contributors field and includes them in Related Works. It also checks for and
removes duplicates.*/
var morphRelatedWorks = function(fields, original_div, field_tr) {
    var relatedworks = [];
    var includedworks = [];
    //starting out, the only RW titles in this field are those from the 730/740.
    //Ones from the 700/710 are in Contributors. We might have titles in 730/740
    //that are dupes of ones in the 700/710--by this point, those titles might
    //be in Included Works or still in Contributors. So first we need to dedupe
    //titles in Included Works. If any RW titles *are* duplicated in IW, we
    //need to remove the RW title. At the same time, we need to store RW titles
    //so that later we can dedupe w/Contributors.
    fieldSelect("Included Works", original_div).find("td.bibInfoData").each(function() {
        includedworks.push(normalizeTitle($(this).children("a").first().text()));
    });
    field_tr.find("td.bibInfoData").each(function() {
        var title = normalizeTitle($(this).text());
        if ($.inArray(title, includedworks) == -1) {
            processText($(this), function(text) {
                return cleanPunct(text);
            });
            relatedworks.push({
                "title": title,
                "td": $(this)
            });
        } else {
            $(this).parent().empty().remove();
        }
    });
    //Here's the function that processes the Contributor to format it for RW.
    var getTitle = function(object, type) {
        var anchors = object.children("a");
        if ((anchors.length > 1)) {
            if (type == "contributor") {
                processText(anchors.first(), function(text) {
                    return uninvertName(cleanPunct(text));
                });
            } else {
                processText(anchors.first(), function(text) {
                    return parseCorpName(cleanPunct(text));
                });
            }
            processText(anchors.last(), function(text) {
                return cleanPunct(text);
            });
            a2_text = normalizeTitle(anchors.last().text());
            anchors.first().text("(" + anchors.first().text() + ")");
            // if this title is a dupe, we want to keep this version since it 
            // has the name associated with it.
            for (var i in relatedworks) {
                if (a2_text == relatedworks[i].title) {
                    relatedworks[i].td.parent().empty().remove();
                }
            }
            var new_tr = $("<tr><td class=\"bibInfoLabel\" width=\"20%\" valign=\"top\"></td></tr>").append(object.append(anchors.first()));
            var new_td = new_tr.find("td.bibInfoData");
            new_td.contents().filter(function() {
                return this.nodeType == 3;
            }).text(function(i, text) {
                this.nodeValue = text.match(/\((work|expression|manifestation|item)\)/) ? stripFRBR(text) : "";
            });
            field_tr.push(new_tr[0]);
            relatedworks.push({
                "title": a2_text,
                "td": new_tr.find("td.bibInfoData")
            });
        }
    }
    var getContributorTitle = function() {
        getTitle($(this), "contributor");
    }
    var getCorporateTitle = function() {
        getTitle($(this), "corporate_contributor");
    }
    fieldSelect("Contributors", original_div).find("td.bibInfoData").each(getContributorTitle);
    fieldSelect("Corporate Contributors", original_div).find("td.bibInfoData").each(getCorporateTitle);
    field_tr = rowsToCell(field_tr, "| ");
    return field_tr;
}

/*morphContents collapses long tables-of-contents fields. Use new_size as the
size (in num chars) of the visible portion of text after the field is collapsed,
and use max_size as the maximum size (in num chars) a toc field can be before it
gets collapsed.*/
var morphContents = function(fields, original_div, field_tr) {
    var new_size = 400;
    var max_size = 600;
    var toc_text = "";

    // first, let's shove together multiple TOC fields so we can get a good
    // char count to see if we need to collapse or not
    field_tr.find("td.bibInfoData").each(function() {
        toc_text += $(this).text() + "<br />";
    });

    // we only collapse the field if the toc_text length is > max_size
    if (toc_text.length > max_size) {
        if (field_tr.length > 1) {
            field_tr = $(field_tr[0]);
        }
        // we don't want to break off in the middle of a word, so let's find
        // the next space character and use that as the new break point
        while (new_size < toc_text.length - 1) {
            var c = toc_text.substring(new_size - 1, new_size);
            if (c == " ") {
                break;
            } else {
                new_size += 1;
            }
        }
        // define the new HTML to use:
        var new_html = "<td class=\"bibInfoLabel\" width=\"20%\" valign=\"top\">Contents</td>";
        new_html += "<td class=\"bibInfoData\"><span class=\"contents_top\"><span class=\"toggle_button contents_trigger ss_sprite ss_add\">&nbsp;</span>";
        new_html += toc_text.substring(0, new_size) + "</span>";
        new_html += "<span class=\"contents_bottom\">" + toc_text.substring(new_size) + "</span>";
        new_html += "<div class=\"contents_trigger\"><a href=\"#\">More ...</a></div>";
        new_html += "</td>";
        field_tr.html(new_html);

        // and finally define behavior:
        field_tr.find(".contents_bottom").hide();
        var contents_hidden = true;
        field_tr.find(".contents_trigger").click(function() {
            field_tr.find("span.toggle_button").toggleClass("ss_add");
            field_tr.find("span.toggle_button").toggleClass("ss_delete");
            if (contents_hidden) {
                field_tr.find("span.contents_bottom").show();
                field_tr.find("div.contents_trigger a").text("Less ...");
                contents_hidden = false;
            } else {
                field_tr.find("span.contents_bottom").hide();
                field_tr.find("div.contents_trigger a").text("More ...");
                contents_hidden = true;
            }
            return false;
        });
    }
    return field_tr;
}

/* morphAlert is for resource records' Resource Advisory field.*/
var morphAlert = function(fields, original_div, field_tr) {
    field_tr.find("td.bibInfoData").each(function() {
        var td = $(this);
        td = linkify(td);
        td.html("<div style=\"display:none;\"><div id=\"alert\"><h1 class=\"dialogTitle\">Advisory Alert</h1>" + td.html() + "</div></div>");
    });
    return field_tr;
}

// Below are functions that morph entire tables/spans/divs.
/*morphBibHeadTable processes the .bibHead table. It adds the statement of
responsibility, or creates a pseudo-SOR, if there's not one, using Author,
Corporate Author, Contributors, Corporate Contributors, and Included Works.
*/
var morphBibHeadTable = function(fields, original_div, table) {
    var append_text = "";
    var names = [];
    var name_fields = ["Author/Creator", "Corporate Author", "Contributors", "Included Works", "Corporate Contributors"];
    var and_others = false;
    if (global_title_parts.length > 1) {
        names.push(global_title_parts[1]);
        if (global_title_parts.length > 2) {
            and_others = true;
        }
    } else {
        for (var i in name_fields) {
            var field = name_fields[i];
            fieldSelect(field, original_div).find("td.bibInfoData").each(function() {
                if (names.length <= 2) {
                    var name = $(this).text();
                    var anchors = $(this).children("a");
                    name = name.replace(/\n/g, "");
                    if (field == "Included Works") {
                        if (name.match(/^[^,.]+,[^.]+\./)) {
                            name = name.replace(/^([^\.]+)\.(.*)$/, "$1");
                        } else {
                            name = "";
                        }
                    }
                    if ((field == "Author/Creator") || (field == "Contributors") || (field == "Included Works")) {
                        name = uninvertName(cleanPunct(name), true);
                        name = name.replace();
                    } else {
                        name = parseCorpName(cleanPunct(name));
                    }
                    // Make sure the name isn't a duplicate, and make sure the name string isn't a "related work"
                    if (name && $.inArray(name, names) == -1 && !((field == "Contributors" || field == "Corporate Contributors") && anchors.length > 1)) {
                        names.push(name);
                    }
                } else {
                    and_others = true;
                    return false;
                }
            });
        }
    }
    for (var i in names) {
        if (i > 0) {
            if ((names.length > 2) || and_others) {
                append_text += ", ";
            } else {
                append_text += " ";
            }
            if (i == names.length - 1 && !and_others) {
                append_text += "and ";
            }
        }
        append_text += names[i];
    }
    if (and_others) {
        append_text += ", and others";
    }
    if (append_text.match(/^[a-z]/)) {
        append_text = append_text.replace(/^[a-z]/, append_text.charAt(0).toUpperCase());
    }
    table.append("<tr><td class=\"bibInfoLabelSuppressed\">Responsibility</td><td class=\"bibInfoData\">" + append_text + "</td></tr>");
    return table;
}

/* morphBibMediaDiv replaces the large version of the media type icon (defined
in wwwoptions) with the small version*/
var morphBibMediaDiv = function(fields, original_div, div) {
    var html = div.html();
    html = html.replace(".png", "_small.png");
    div.html(html);
    return div;
}

/*morphBibLibraryHasTable processes the table containing Library Has (Lib Has).
It finds check-in record Lib Has data and prepends it to the table. It adds 
zebra-striping and, when the table has more than 4 rows, it converts it to an
expandable/collapsible widget.*/
var morphBibLibraryHasTable = function(fields, original_div, table) {
    // first we grab check-in record Lib Hases, if they exist, and tack them
    // onto the beginning of the table.
    var new_table = $(document.createElement("table")).addClass("bibLibHas").append("<tbody></tbody>");
    original_div.find("table.bibHoldings tr").each(function() {
        var lib_has_string = "";
        var field = $(this).find("td.bibHoldingsLabel").text();
        if (field && field.match(/^.*?(Library Has).*?$/m)) {
            var lib_has_value = $(this).find("td.bibHoldingsEntry").html();
            var values = [];
            if (lib_has_value) {
                if ($(this).prev().find("td.bibHoldingsLabel").text().match(/^.*?(Location).*?$/m)) {
                    var location_value = $(this).prev().find("td.bibHoldingsEntry").text();
                    location_value = location_value.replace(/^((&nbsp;)|(\s)|(\n))+/mg, "");
                    location_value = location_value.replace(/((&nbsp;)|(\s)|(\n))+$/mg, "");
                    lib_has_string += location_value.toUpperCase() + ": ";
                }
                lib_has_value = lib_has_value.replace(/^((&nbsp;)|(\s)|(\n))+/mg, "");
                lib_has_value = lib_has_value.replace(/((&nbsp;)|(\s)|(\n)|(<br>))+$/mg, "");
                values = lib_has_value.split("<br>");
            }
            // let's check to make sure this isn't an exact duplicate of existing
            // lib has data
            for (var i in values) {
                lib_has_string += values[i];
                var add = true;
                table.find("td.bibInfoData").each(function() {
                    var comp_text = $(this).text();
                    comp_text = comp_text.replace(/^((&nbsp;)|(\s)|(\n))+/mg, "");
                    comp_text = comp_text.replace(/((&nbsp;)|(\s)|(\n))+$/mg, "");
                    if (comp_text == lib_has_string) {
                        add = false;
                    }
                });
                if (add) {
                    new_table.find("tbody").append("<tr><td></td><td class=\"bibInfoData\">" + lib_has_string + "</td></tr>");
                }
                lib_has_string = "";
            }
        }
    });
    if (new_table.find("tr").length) {
        new_table.find("td").first().addClass("bibInfoLabel").attr("width", "20%").attr("valign", "top").text("Library Has:");
        table.find("td.bibInfoLabel").removeClass("bibInfoLabel").text("");
        table.prepend(new_table.find("tr"));
    } else {
        table.find("td.bibInfoLabel").text("Library Has:");
    }
    table.find("tr:odd").addClass("odd");
    // if there are more than 4 rows, let's define toggle functionality
    if (table.find("tr").length > 4) {
        // first modify table structure and add buttons
        var show_tbody = table.find("tbody").first();
        var hide_tbody = $(document.createElement("tbody"));
        show_tbody.addClass("show_rows");
        hide_tbody.addClass("hide_rows").hide();
        table.find("td.bibInfoData").first().prepend("<span class=\"toggle_button trigger ss_sprite ss_add\">&nbsp;</span>");
        table.find("tr").each(function(i) {
            if (i > 2) {
                hide_tbody.append($(this));
            }
        });
        table.append(hide_tbody);
        table.append("<tbody class=\"toggle_row\"><tr><td colspan=\"2\"><div class=\"trigger\"><a href=\"#\">More ...</a></div></td></tr></tbody>");
        /* then define behavior -- not using jQuery toggle() functions because
		they don't seem to work right with <tbody> in this case*/
        var hidden = true;
        table.find(".trigger").click(function() {
            table.find("span.toggle_button").toggleClass("ss_add");
            table.find("span.toggle_button").toggleClass("ss_delete");
            if (hidden) {
                table.find("tbody.hide_rows").removeAttr("style");
                table.find("tbody.toggle_row div a").text("Less ...");
                hidden = false;
            } else {
                table.find("tbody.hide_rows").attr("style", "display:none;");
                table.find("tbody.toggle_row div a").text("More ...");
                hidden = true;
            }
            return false;
        });
    }
    return table;
}

/*morphBibSummaryTable processes the Summary table. First it fixes a formatting
bug that occurs when there are multiple rows. (Rows after the first need to have
their first <td> hidden, since the first row has a hidden <td>.) Then, assuming
an item summary exists, it adds a "Summary" <th> onto the table.*/
var morphBibSummaryTable = function(fields, original_div, table) {
    table.find("tr").each(function(i) {
        $(this).find("td").attr("width", ""); // remove width attribute, for IE8
        var test_td = $(this).find("td").first();
        if (i > 0) {
            if (test_td.text().match(/^\s*$/)) {
                test_td.addClass("hideMe");
            }
        }
    });
    if (table.find("tr").length) {
        table.prepend("<tr><th colspan=\"2\"><div>Summary</div></th></tr>");
    }
    return table;
}

/*morphBibDetailsBottomTable processes the "More Item Details" portion of the bib
display. It adds the class bibLabelRow to all <td>s in the rows that have the bib
field label (so those rows can be styled separately). And it adds the header to
the table if there is any content in the table.
*/
var morphBibDetailsBottomTable = function(fields, original_div, table) {
    table.find("tr").each(function() {
        if ($(this).find("td.bibInfoLabel").length) {
            $(this).find("td").addClass("bibLabelRow");
        }
    });
    if (table.find("td").length) {
        table.prepend("<tr><th colspan=\"2\"><div>More Item Details</div></th></tr>");
    } else {
        table.hide();
    }
    return table;
}

/*morphBibItemsDiv processes the Items/Holdings table. Does 2 things: 1. Hides
the Notes column if none of the Notes cells have text. 2. Collapses the table
if it has more than 6 rows (including the header), and includes a link for
viewing all copies.*/
var morphBibItemsDiv = function(fields, original_div, div) {
    //if (original_div.find("table.bibResourceBrief").length) {
    //  div.hide();
    //} else {  
    var table = div.find("table.bibItems");
    var trs = table.find("tr");
    var port = "";
    var url_base = getUrlBase();

    // remove width attribute from all table cells--for IE8
    trs.find("th, td").attr("width", "");

    // first we collapse the table if there are > 6 rows
    if (trs.length > 6) {
        trs.each(function(i) {
            if (i > 2) {
                $(this).hide();
            }
        });
        div.find("center").hide();
        // got to generate the holdings URL using record # from permanent link
        var permalink;
        if ($("bibDisplayFullExport").length) {
            permalink = original_div.find("span.permalink");
        } else {
            permalink = $("#permalink");
        }
        var matches = permalink.attr("href").match(/=b(\d+)(~S\d+)/);
        var holdings_link = url_base + "/" + "search" + matches[2] + "?/.b" + matches[1] + "/.b" + matches[1] + "/1,1,1,B/holdings~" + matches[1] + "&FF=&1,0,";
        table.append("<tbody class=\"toggle_row\"><tr><td colspan=\"" + table.find("tr.bibItemsHeader th").length + "\"><a href=\"" + holdings_link + "\"><div class=\"ss_sprite ss_page_copy\">&nbsp;</div>View all copies ...</a></td></tr></tbody>");
    }
    // second, we hide the Notes column if all cells are empty and add a recall
    // button if status = DUE or ON HOLDSHELF -- but not for noncirc_locs
    // also, we want to hide the barcode column
    var has_notes = false;
    trs.each(function(i) {
        if ($(this).attr("style") != "display: none;") {
            var bib_link = encodeURI(url_base + $("#permalink").attr("href"));
            var notes = $(this).find("td").eq(3);
            var status = $(this).find("td").eq(2);
            var call = $(this).find("td").eq(1);
            var loc = $(this).find("td").eq(0);
            if (loc.find("a").attr("href") == "#") {
                loc.html("&nbsp;" + loc.find("a").html());
            }
            var remote_anchor = $(this).find("a.fancybox_remote");
            $(this).find("th,td").eq(4).hide();
            if (notes.text()) {
                has_notes = true;
            }
            if (call.length && call.text().match(/^[\s*]$/)) {
                call.text("N/A");
            }
            $(this).find("th,td").eq(3).addClass("item-notes");
        }
    });
    if (!has_notes) {
        trs.find(".item-notes").hide();
    }
    //}
    return div;
}

/*morphBibUrlsDiv processes the Div containing--by default--URLs from 856es.*/
var morphBibUrlsDiv = function(fields, original_div, div) {
    // First deal with the request button.
    var req_button = $("#bibDisplayBody .navigationRow .requestRecords.buttonText").parent();
    if (req_button.length) {
        req_button.addClass("fancybox_request");
        // append the request button to the biblinks table; create the table if it isn't there
        if (div.find("table.bibLinks").length) {
            div.find("table.bibLinks").append("<tr><td></td></tr>").find("td").last().append(req_button);
        } else {
            div.append("<table class=\"bibLinks\"><tbody><tr><td></td></tr><tr><td></td></tr></tbody></table>").find("td").last().append(req_button);
        }
    }
    // Next deal with holdings links Resource Records, for ERM e-journal holdings.
    var links = div.find("a");
    if (original_div.find("table.resourceLinks").length) {
        links = div.find("table.resourceLinks td a");
        links.each(function() {
            $(this).text("Connect to " + $(this).text());
        });
        if (fieldSelect("Alert", original_div).length) {
            div.find("table.resourceLinks tr").append("<td class=\"resourceAlert\"><a class=\"fancybox_eralert\" title=\"There is an Advisory Alert for this resource.\"><span class=\"ss_sprite ss_exclamation\">&nbsp;</span></a></td>");
            div.find("table.resourceLinks tr td.resourceAlert a").attr("href", "#alert"); // adding href attr this way for IE7
        }
    }
    // Last: Hide the links section if it's still empty. Otherwise, add a nice icon and deal w/media booking links.
    if (!links.length || original_div.find("table.bibResourceBrief").length) {
        div.hide();
    } else {
        links.each(function() {
            if ($(this).attr("class") != "fancybox_eralert") {
                var icon = $(this).prepend("<span class=\"ss_sprite\">&nbsp;</span>").find("span.ss_sprite");
                if ($(this).attr("href").match(/^http:\/\/mediabook.library.unt.edu/)) {
                    $(this).addClass("fancybox_mediabook");
                    icon.addClass("ss_date");
                } else if ($(this).hasClass("fancybox_request")) {
                    icon.addClass("ss_truck");
                } else {
                    icon.addClass("ss_computer_go");
                }
            }
        });
    }
    return div;
}

/*morphBibHoldingsTable builds a new table based on the bibHoldings Table (which
comes from the info in the III Checkin record). Since the state of holdings
information in our catalog is complicated, this tries to make some sense out of
it. There are two basic cases that we need to cover: first, if this has ERM
holdings info (e.g., it's an e-journal), then we need to display the holdings
table instead of the URLs and the items table. Second, if it's not ERM holdings
info, then we need to produce a sane display. In the second case, if there's an
items table, it displays only info about the latest items received--otherwise
it also displays call number and location. */
var morphBibHoldingsTable = function(fields, original_div, table) {
    var new_table = $(document.createElement("table"));
    if (table.find("table.bibResourceBrief").length) {
        // need to do 3 basic things. 1st, make the resource advisory alert also fancybox-ized (inline), if
        // it exists. 2nd, make the journal dates (in the first column) link to
        // the resource, grabbing the URL from the hidden last column. 3rd, remove the resource
        // title link and chop the name as needed so that it won't wrap.
        var caption = table.find("tr.bibResourceCaption");
        var external_res_max_length = table.find("td.bibResourceSubEntry").length ? 24 : 28;
        new_table = new_table.addClass("bibResourceBrief").append("<tbody></tbody>");
        table.find("tr").each(function(i) {
            if ($(this).hasClass("bibResourceEntry")) {
                var dates = $(this).find("td.bibResourceEntry:nth-child(1)").addClass("dates");
                var external_res = $(this).find("td.bibResourceEntry:nth-child(2)").addClass("resourceTitle");
                var res_record = $(this).find("td.bibResourceEntry:nth-child(3)").addClass("info");
                var res_alert = $(this).find("td.bibResourceEntry:nth-child(4)").addClass("advisory");
                var res_alert_text = $(this).next().find("td.bibResourceSubEntry").text();
                var dirty_url = $(this).find("td.bibResourceEntry:nth-child(5)").addClass("hideMe");
                var new_url = "";
                //fancybox-ize the resource alert, if it exists.
                if (res_alert_text.match(/[^\s]+/)) {
                    res_alert.html("<div style=\"display:none\"><div id=\"alert" + i + "\"><h1 class=\"dialogTitle\">Advisory Alert</h1>" + res_alert_text + "</div></div><a class=\"fancybox_eralert\" title=\"There is an Advisory Alert for this resource.\"><span class=\"ss_sprite ss_exclamation\">&nbsp;</span></a>");
                    res_alert.find("a.fancybox_eralert").attr("href", "#alert" + i);
                } else {
                    res_alert.text("");
                }
                //grab the appropriate URL and link the Dates string up.
                match = dirty_url.find("a");
                if (match.length) {
                    new_url = match.attr("href");
                } else {
                    new_url = external_res.find("a").attr("href");
                }
                dates.html("<a href=\"" + new_url + "\" target=\"_blank\"><span class=\"ss_sprite ss_computer_go\">&nbsp;</span>" + caption.find("td").text() + " " + dates.text() + "</a>");
                //remove the resource name link and chop the length as needed.
                var external_res_string = external_res.find("a").text();
                if (external_res_string.length >= external_res_max_length) {
                    external_res_string = "<span title=\"" + external_res.find("a").text() + "\">" + external_res_string.substring(0, external_res_max_length - 3) + "..." + "</span>";
                }
                external_res.html(external_res_string);
                res_record.html(res_record.html().replace(/a>\s*(&nbsp;)+\s*/, "a>"));
                caption.hide();
                new_table = new_table.append($(this));
            }
        });
    } else {
        // first let's put the tabular data in a form we can use.
        var h_data = [];
        var h_record = {
            "Call Number": "",
            "Location": "",
            "Latest Received": "",
            "Library Has": ""
        };
        var cn_list = [];
        table.find("tr").each(function(i) {
            var value = $(this).find("td.bibHoldingsEntry").html();
            if (value) {
                value = value.replace(/^((&nbsp;)|(\s)|(\n))+/mg, "");
                value = value.replace(/((&nbsp;)|(\s)|(\n)|(<br>)|\.)+$/mg, "");
                value = value.replace(/\s*\(click to request\)/i, "");
            }
            for (var k in h_record) {
                if ($(this).find("td.bibHoldingsLabel:contains('" + k + "')").length && value) {
                    h_record[k] = value;
                    if (k == "Latest Received" && h_record["Location"]) {
                        h_record["url"] = $(this).find("td.bibHoldingsLabel a").attr("href");
                    } else if (k == "Call Number") {
                        cn_list.push(value);
                    }
                }
            }
            if (i == table.find("tr").length - 1 || table.find("tr:eq(" + (i + 1) + ") td.bibHoldingsLabel:contains('Call Number')").length) {
                if (h_record["Location"]) {
                    h_data.push(h_record);
                }
                h_record = {
                    "Call Number": "",
                    "Location": "",
                    "Latest Received": "",
                    "Library Has": ""
                };
            }
        });
        // now we'll populate the new table.
        var perstr = "";
        if (!original_div.find("table.bibItems").length || cn_list) {
            for (var i = 0; i < h_data.length; i++) {
                perstr += "Volumes " + (h_data[i]["Call Number"] ? "with call number " + h_data[i]["Call Number"] + " " : "");
                perstr += (h_data[i]["Library Has"] ? "(for " + h_data[i]["Library Has"] + ") " : "");
                perstr += "are available" + (h_data[i]["Location"] ? " at " + h_data[i]["Location"] : "") + ".";
                perstr += (h_data[i]["Latest Received"] ? " The last issue received " + (h_data[i]["Location"] ? "here " : "") + "was " + h_data[i]["Latest Received"] + ". <a href=\"" + h_data[i]["url"] + "\">(View latest issues received)</a>" : "");
                if (i < h_data.length - 1 && (h_data[i + 1]["Call Number"] || h_data[i + 1]["Location"])) {
                    perstr += "<br /><br />";
                }
            }
        }
        if (perstr) {
            new_table.addClass("bibHoldings").append("<tbody><tr><td class=\"bibHoldingsEntry\">" + perstr + "</td></tr></tdboy>");
        } else {
            new_table.hide();
        }
    }
    return new_table;
}

// 2. Field Definitions
/* Change the bib_fields array to add/rearrange fields. Fields are displayed 
in the order in which they appear in the array. If a field is not here, it is
not displayed. If a field is here but not in the record, it is ignored. Add a 
value to bib_fields[n].css_class for each field that gets a new CSS class.
Change bib_fields[n].display_fn to remove/display the field name text in the
display. Add a function name to bib_fields[n].process to specify a function
that does some additional processing on a field's contents. (Then create the
function above.)

Note: field_name entries preceded by an * are special entries--*Urls dictates
where the bibUrls table goes. *Items dictates where the bibItems table goes.
*table entries specify the start of new <table> elements, which allows you to
add a CSS class just to that table. All fields after a *table entry up until
the next * entry go inside that table.

*/

var resource_fields = [{
    "field_name": "*table",
    "css_class": "bibHead",
    "process": morphBibHeadTable
}, {
    "field_name": "Resource Name",
    "css_class": "displayTitle",
    "display_fn": 0,
    "process": morphTitle
}, {
    "field_name": "*div.bibMedia",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibMediaDiv
}, {
    "field_name": "Alert",
    "css_class": "displayAlert",
    "display_fn": 0,
    "process": morphAlert
}, {
    "field_name": "*div.bibDisplayUrls",
    "process": morphBibUrlsDiv
}, {
    "field_name": "*table",
    "css_class": "bibSummary",
    "process": morphBibSummaryTable
}, {
    "field_name": "Summary",
    "css_class": "displaySummary",
    "display_fn": 0,
    "process": morphNotesField
}, {
    "field_name": "Notes",
    "css_class": "displayNotes",
    "display_fn": 0,
    "process": morphSummaryNotesField
}, {
    "field_name": "Support Contact",
    "css_class": "displaySupportContact",
    "display_fn": 0,
    "process": morphSummaryNotesField
}, {
    "field_name": "*table",
    "css_class": "bibDetailsBottom",
    "process": morphBibDetailsBottomTable
}, {
    "field_name": "Alternate Titles",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Contributors",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Resource Type",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Resource Format",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}];

var bib_fields = [{
    "field_name": "*table",
    "css_class": "bibHead",
    "process": morphBibHeadTable
}, {
    "field_name": "Title",
    "css_class": "displayTitle",
    "display_fn": 0,
    "process": morphTitle
}, {
    "field_name": "*div.bibMedia",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibMediaDiv
}, {
    "field_name": "*table",
    "css_class": "bibDetailsTop",
    "process": 0
}, {
    "field_name": "Production Info",
    "css_class": "",
    "display_fn": 0,
    "process": morphPubInfo
}, {
    "field_name": "Publication Info",
    "css_class": "",
    "display_fn": 0,
    "process": morphPubInfo
}, {
    "field_name": "Distribution Info",
    "css_class": "",
    "display_fn": 0,
    "process": morphPubInfo
}, {
    "field_name": "Manufacture Info",
    "css_class": "",
    "display_fn": 0,
    "process": morphPubInfo
}, {
    "field_name": "Copyright Notice Date",
    "css_class": "",
    "display_fn": 0,
    "process": morphPubInfo
}, {
    "field_name": "Edition",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Series",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Continues",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Continues In Part",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Continued By",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Continued In Part By",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Supersedes",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Superseded By",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Supersedes In Part",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Superseded In Part By",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Absorbed By",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Absorbed In Part By",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Formed By Union Of",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Absorbed",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Absorbed In Part",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Separated From",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Split Into",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Merged With",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "Changed Back To",
    "css_class": "",
    "display_fn": 0,
    "process": morphBibDetailsTopField
}, {
    "field_name": "*table",
    "css_class": "bibLibHas",
    "process": morphBibLibraryHasTable
}, {
    "field_name": "Library Has",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "*table.bibHoldings",
    "process": morphBibHoldingsTable
}, {
    "field_name": "*div.bibDisplayUrls",
    "process": morphBibUrlsDiv
}, {
    "field_name": "*div.bibDisplayItems",
    "process": morphBibItemsDiv
}, {
    "field_name": "*table.bibOrder",
    "process": 0
}, {
    "field_name": "*table",
    "css_class": "bibSummary",
    "process": morphBibSummaryTable
}, {
    "field_name": "Summary",
    "css_class": "displaySummary",
    "display_fn": 0,
    "process": morphNotesField
}, {
    "field_name": "*table",
    "css_class": "bibDetailsBottom",
    "process": morphBibDetailsBottomTable
}, {
    "field_name": "Uniform Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Translated Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Parallel Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Distinctive Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Cover Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Other Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Added Title Page Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Caption Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Running Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Spine Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Alternate Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Additional Titles",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Former Title",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Author/Creator",
    "css_class": "",
    "display_fn": 1,
    "process": morphAuthor
}, {
    "field_name": "Contributors",
    "css_class": "displayContributors",
    "display_fn": 1,
    "process": morphContributors
}, {
    "field_name": "Included Works",
    "css_class": "",
    "display_fn": 1,
    "process": morphIncludedWorks
}, {
    "field_name": "Contents",
    "css_class": "",
    "display_fn": 1,
    "process": morphContents
}, {
    "field_name": "Frequency",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Dates of Publication",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Main Series",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Has Subseries",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Translation of",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Has Supplement",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Supplement to",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Part of",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Issued With",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Other Editions",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Other Versions",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Other Formats",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Related Works",
    "css_class": "",
    "display_fn": 1,
    "process": morphRelatedWorks
}, {
    "field_name": "Genres",
    "css_class": "subject",
    "display_fn": 1,
    "process": morphSubject
}, {
    "field_name": "Subjects",
    "css_class": "subject",
    "display_fn": 1,
    "process": morphSubject
}, {
    "field_name": "Related Meetings",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefaultRowsToCell
}, {
    "field_name": "Performers",
    "css_class": "",
    "display_fn": 1,
    "process": morphNotesField
}, {
    "field_name": "Production Credits",
    "css_class": "",
    "display_fn": 1,
    "process": morphNotesField
}, {
    "field_name": "Notes",
    "css_class": "",
    "display_fn": 1,
    "process": morphNotesField
}, {
    "field_name": "Physical Description",
    "css_class": "",
    "display_fn": 1,
    "process": morphDefault
}, {
    "field_name": "Scale",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Physical Medium",
    "css_class": "ajax-update 340",
    "display_fn": 1,
    "process": morph34XField
}, {
    "field_name": "Geospatial Data",
    "css_class": "ajax-update 342 343",
    "display_fn": 1,
    "process": morph34XField
}, {
    "field_name": "Audio Details",
    "css_class": "ajax-update 344",
    "display_fn": 1,
    "process": morph34XField
}, {
    "field_name": "Projection Details",
    "css_class": "ajax-update 345",
    "display_fn": 1,
    "process": morph34XField
}, {
    "field_name": "Video Details",
    "css_class": "ajax-update 346",
    "display_fn": 1,
    "process": morph34XField
}, {
    "field_name": "Digital File Details",
    "css_class": "ajax-update 347",
    "display_fn": 1,
    "process": morph34XField
}, {
    "field_name": "Graphic Representation",
    "css_class": "ajax-update 352",
    "display_fn": 1,
    "process": morph34XField
}, {
    "field_name": "Standard Identifier",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "ISBN",
    "css_class": "isbn",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "ISSN",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "International Standard Recording Code",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Universal Product Code",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "International Standard Music Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "International Article Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Serial Item and Contribution Identifier",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Overseas Acquisition Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Fingerprint Identifier",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Standard Technical Report Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Publisher Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "CODEN Designation",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "LC Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "LC Control Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "OCLC Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Government Document Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "GPO Item Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Government Document Classification Number",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}, {
    "field_name": "Url",
    "css_class": "",
    "display_fn": 1,
    "process": 0
}];

// 3. Framework Code
/* refactorBibDisplay--takes a field list data structure (e.g., bib_fields) 
and generates a new display of the bib record based on the field list. Returns
a jquery object that is a new div (with class refactoredBib) to be appended to
the main content div.
This has become a freaking mess. I'll try to clean it up later.
*/
var refactorBibDisplay = function(fields, container_div) {
    var original_div = container_div.clone();
    // New content goes into new_div--we'll attach it to the DOM when we're done
    var new_div = $(document.createElement("div"));
    var new_tables = [$(document.createElement("table"))];
    new_div.addClass("refactoredBib");
    var table_processor = 0;
    for (var i in fields) {
        // With *Urls and *Items, we'll grab the existing div and pass it
        // through the process function, if specified
        if ((fields[i].field_name == "*div.bibDisplayUrls") || (fields[i].field_name == "*div.bibDisplayItems") || (fields[i].field_name == "*table.bibOrder") || (fields[i].field_name == "*table.bibHoldings") || (fields[i].field_name == "*div.bibMedia")) {
            if (table_processor) {
                new_tables[new_tables.length - 1] = table_processor(fields, original_div, new_tables[new_tables.length - 1]);
                table_processor = 0;
            }
            var special_div = container_div.find(fields[i].field_name.substr(1));
            if (special_div.length) {
                if (fields[i].process) {
                    special_div = fields[i].process(fields, original_div, special_div);
                }
                new_tables.push(special_div);
                new_tables.push($(document.createElement('table')));
            }
            // With *table we create a new table, if the last new_table isn't
            // already a new, empty one
        } else if (fields[i].field_name == "*table") {
            if (table_processor) {
                new_tables[new_tables.length - 1] = table_processor(fields, original_div, new_tables[new_tables.length - 1]);
                table_processor = 0;
            }
            if (new_tables[new_tables.length - 1][0].hasChildNodes()) {
                new_tables.push($(document.createElement('table')));
            } else {
                new_tables[new_tables.length - 1].removeClass();
            }
            if (fields[i].process) {
                table_processor = fields[i].process;
            }
            if (fields[i].css_class) {
                new_tables[new_tables.length - 1].addClass(fields[i].css_class);
            }
            // Otherwise this is actually a field and should be handled as such
        } else {
            var field_tr = fieldSelect(fields[i].field_name, container_div).detach();
            if (field_tr.length && fields[i].process) {
                // Fields with css class ajax-update need to have their process function
                // registered with $.ajaxStop so that they fire AFTER the ajax that loads
                // the marc data into each field fires.
                if (fields[i].css_class.match(/ajax-update/)) {
                    global_ajax_field_params.push([fields[i].process, field_tr]);
                    $(document).ajaxStop(function() {
                        var params = global_ajax_field_params.pop();
                        params[1] = params[0](fields, original_div, params[1]);
                    });
                } else {
                    field_tr = fields[i].process(fields, original_div, field_tr);
                }
            }
            field_tr.find("td.bibInfoLabel").each(function(x) {
                if ($(this).text() == "") {
                    $(this).text(fields[i].field_name);
                }
                if ((!fields[i].display_fn)) {
                    $(this).replaceWith("<td class=\"bibInfoLabelSuppressed\">" + $(this).text() + "</td>");
                } else if (x > 0) {
                    $(this).replaceWith("<td></td>");
                }
            });
            if (fields[i].css_class) {
                field_tr.addClass(fields[i].css_class);
            }
            new_tables[new_tables.length - 1].append(field_tr);
        }
    }
    if (table_processor) {
        new_tables[new_tables.length - 1] = table_processor(fields, original_div, new_tables[new_tables.length - 1]);
        table_processor = 0;
    }
    for (var i in new_tables) {
        if (new_tables[i][0].hasChildNodes()) {
            new_div.append(new_tables[i]);
        }
    }
    return new_div;
}

/* refactorBriefCitDetail. Here we're basically just concerned with making sure
the uniform and transcribed titles aren't exact duplicates (removing the
uniform if they are) and cleaning up punctuation and line breaks, etc.
*/
var refactorBriefCitDetail = function(container_div) {
    var new_div = $(document.createElement("div"));
    var unif_span = container_div.find("span.briefcitTitleUniform");
    var indexed_span = container_div.find("span.briefcitIndexedTitle");
    var title_div = container_div.find("div.briefcitTitle245");
    var has_per_ord_ind = findPerOrdInd(container_div.find("span.briefcit008"), container_div.find("span.briefcit041"));
    var unif_text = "";
    var title_text = "";
    var by_pub_regex = /<\/span>\n{0,2}([^>]*?)<br>\n?(.*)$/i;
    var by_pub_string = "";
    new_div.addClass("briefcitDetailMain");
    //process uniform (maybe) title (UT)
    if (indexed_span.length) {
        unif_text = indexed_span.text();
        unif_text = unif_text.replace(/["»]/g, "");
        indexed_span.empty().remove();
    } else {
        unif_text = unif_span.text();
    }
    //normalize the UT, but we don't want to use this for display
    var ut_array = chunkTitleString(unif_text, has_per_ord_ind);
    var norm_unif_text = normalizeTitle(ut_array[0]);
    //process transcribed title (TT) and generate normalized version
    title_text = title_div.text();
    var tt_array = chunkTitleString(title_text, has_per_ord_ind);
    title_text = cleanPunct(tt_array[0]);
    var norm_title_text = reEscape(normalizeTitle(tt_array[0]));
    //if normalized UT and TT match, hide UT
    var title_regex = new RegExp("^" + norm_title_text);
    if (norm_unif_text.match(title_regex) && title_text) {
        unif_span.hide();
    } else {
        //generate display UT
        unif_text = procTitlePunct(unif_text, false);
        unif_text = chopEnds(unif_text);
        unif_text = unprotectPeriods(unif_text);
        unif_text = unif_text.replace(/\s\/\s/g, " ");
        unif_span.children("a").empty().append(unif_text);
    }
    title_div.children("a").empty().append(title_text);
    new_div.append(container_div.find("h2.briefcitTitle"));
    remaining_html = container_div.html();
    var matches = by_pub_regex.exec(remaining_html);
    if (matches && (matches[1] || matches[2])) {
        if (matches[1] && !matches[1].match(/^\s+$/)) {
            by_pub_string += uninvertName(cleanPunct(matches[1])) + "<br />";
        }
        if (matches[2]) {
            var pub = cleanPunct(matches[2]);
            by_pub_string += pub.replace(/c(\d\d\d\d)/, "&copy;$1");
        }
    }
    new_div.append(by_pub_string);
    return new_div;
}

/*modifyResultsThumbs contains the logic for modifying the cover images on all
results (briefcit) rows.*/
var modifyResultsThumbs = function(container_div) {
    var cover_img = $("<img />");
    if (container_div.find("span.mediaThumbnail").text().match("http")) {
        cover_img.attr("src", container_div.find("span.mediaThumbnail").text());
        cover_img.attr("class", "MLCover");
        cover_img.attr("alt", "item cover thumbnail");
    } else {
        var dl_match = false;
        container_div.find("div.briefcitActions > a").each(function() {
            if ($(this).attr("href").match(/digital\.library\.unt\.edu\/ark:/) || $(this).attr("href").match(/texashistory\.unt\.edu\/ark:/)) {
                dl_match = true;
                cover_img.attr("src", $(this).attr("href").replace(/\/$/, "").replace(/^http:/, "") + "/thumbnail/");
                cover_img.attr("class", "DLCover");
                cover_img.attr("alt", "Cover Image");
            }
        });
        if (!dl_match) {
            cover_img.attr("src", "/screens/media_no_cover_available.png");
            cover_img.attr("class", "noCover");
            cover_img.attr("alt", "no item cover image available");
        }
    }
    cover_img.width(56);

    container_div.find("div.briefcitMedia").append($("<span class=\"coverThumb\"></span>").append(cover_img));

    var isbn = container_div.find("span.isbn").text().replace("\n", "").split(" ", 1);
    if (isbn != "") {
        container_div.find("span.coverThumb").addClass("isbn_" + isbn);
        isbns.push(isbn);
    }
    return container_div;
}

/* modifyResultsMediaIcon modifies the media type icon. NOTE: If you're 
wondering why we're using JS to append _small to all of the material type
icons to get the correct filenames rather than just modifying wwwoptions, the
reason is that we use the icons defined in wwwoptions for our mobile display,
so we need them defined there for that.*/
var modifyResultsMediaIcon = function(container_div) {
    var small_icon_img = $("<img />");
    if (container_div.find("span.mediaTypeIcon img").length) {
        small_icon_img.attr("src", container_div.find("span.mediaTypeIcon img").attr("src").replace(/\.png$/, "_small.png"));
        small_icon_img.attr("alt", container_div.find("span.mediaTypeIcon img").attr("alt"));
        container_div.find("div.briefcitMedia span.mediaTypeIcon").hide();
        container_div.find("div.briefcitDetailMain h2").after($("<br />")).after(small_icon_img);
    }
    return container_div;
}

/*hideItemCols takes a container jQuery object and a params array, formatted
like this: [{"col": 1, "empty_check": true, "replace": "N/A"}, {"col": 3, 
"empty_check": true, "repl": ""}, {"col": 4, "empty_check": false}].
Col is the column number (starting at 0) of a column in the items table, and
empty_check is a boolean indicating whether or not we should check to make sure
the column is empty before hiding it. If a value for replace is specified,
then, instead of hiding the cell, the blank cell is filled with the replacement
value.*/
var hideItemCols = function(container_div, params) {
    var trs = container_div.find("table.bibItems tr");
    for (var x in params) {
        if (params[x].empty_check) {
            var empty = true;
            trs.each(function(i) {
                if ($(this).find("td").eq(params[x].col).text().match(/[^\s]/)) {
                    empty = false;
                } else if (params[x].repl && params[x].repl.match(/[^\s]/)) {
                    $(this).find("td").eq(params[x].col).text(params[x].repl);
                    empty = false;
                } else {
                    $(this).find("td").eq(params[x].col).hide();
                }
                if ((i == trs.length - 1) && empty) {
                    trs.first().find("th").eq(params[x].col).hide();
                }
            });
        } else {
            trs.each(function() {
                $(this).find("th").eq(params[x].col).hide();
                $(this).find("td").eq(params[x].col).hide();
            });
        }
    }
    return container_div;
}

/*findName takes a string containing a name NOT in inverted order and an 
array of strings that might contain a version of that name. It attempts to
match the name in the array. Returns true if it does, false if not. The last
argument is optional--it lets you specify an index of the array to ignore,
e.g., if the name you're trying to match originally came from that array 
element.
*/
var findName = function(name, name_array, ignore_index) {
    for (i in name_array) {
        if (i != ignore_index) {
            // first, the simple case--if the name exactly matches
            if (name == name_array[i]) {
                return true;
                // if the name didn't match, we'll extract first name and last name
                // and test for two match cases--first/last, first initial/last
            } else {
                var name_part_matches = name.match(/^([A-Z][^\s^\.]*)([\.\s]*.*?\s?)([A-Z][^\s]*?)$/);
                if (name_part_matches && name_part_matches[1] && name_part_matches[3]) {
                    var first = name_part_matches[1].replace(/\.+$/, "");
                    var last = name_part_matches[3].replace(/\.+$/, "");
                    var regex1 = new RegExp(first + "[\\.\\s]+([^\\s]*\\s+){0,2}" + last);
                    var regex2 = new RegExp(first.substr(0, 1) + "[^\\s]*\\s([^\\s]*\\s+){0,2}" + last);
                    if (name_array[i].match(regex1) || name_array[i].match(regex2)) {
                        return true;
                    }
                } else {
                    var regex1 = new RegExp("(^|\\W*)" + last + "(\\W*|$)");
                    if (name_array[i].match(regex1)) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}
/* processBrowseTitle is called by refactorBrowseListTitle. It does the text
processing on the title string that comes from a call number browse screen. It
takes the title string and returns a modified title string.
*/
var processBrowseTitle = function(title) {
    var output = "";
    // PREPROCESS
    title = chopEnds(title);
    /* If we have the pattern: /; Lastname, Firstname$/ then we need to pull off
	the name and store it for later. The below is a quick-and-dirty heuristic
	test to see if this chunk contains *just* one name (without any other
	text--"by," "edited by," etc.) in inverted form. If so, it's text from the
	MARC 100.*/
    var temp_matches = title.match(/;\s*([^;]+)$/);
    var name_100 = "";
    if (temp_matches && temp_matches[1] && temp_matches[1].match(/^([A-Z]([^\s]*?)(\s|$)([a-z]([^\s]*?)(\s|$)){0,2}){1,3}/)) {
        name_100 = uninvertName(temp_matches[1]);
        title = title.replace(/;\s*([^;]+)$/, "");
    }
    var title_chunks = chunkTitleString(title);
    var name_string = (title_chunks[1] ? title_chunks[1] + (name_100 && !findName(name_100, [title_chunks[1]]) ? "; " + name_100 : "") : (name_100 ? name_100 : ""));
    // POSTPROCESS
    output = "<em>" + title_chunks[0] + "</em>";
    if (name_string) {
        output += " &mdash; " + name_string;
    }
    return output;
}
/* refactorBrowseListTitle does cleanup on titles for call number browse lists.
I've added a wwwoption that generates a combined Title/Author string, and this
pulls the title string from the <td> element where it lives and calls
processBrowseTitle on it. It returns a new <td> element with the modified
title.
*/
var refactorBrowseListTitle = function(orig_td) {
    new_td = $(document.createElement("td")).addClass(orig_td.attr("class"));
    // need to toUpperCase the call number first
    var call_num = orig_td.find("a").eq(1).text().toUpperCase();
    orig_td.find("a").eq(1).text(call_num);
    var html = orig_td.html();
    var title_regex = /(<\/[Aa]>\s+:\s+)(.*)&nbsp;/;
    var matches = title_regex.exec(html);
    if (matches) {
        html = html.replace(title_regex, "$1") + processBrowseTitle(matches[2]);
    }
    new_td.html(html);
    return new_td;
}
// END BIB_DISPLAY *****

/* Definitions for selectBindings class -- stores and organizes select options
into bindings, to help make it easier to generate options lists for customized
drop-down menus (e.g., for advanced search limiters) NOTE: Since I'm using the
var X = function(){} method of declaring functions, the class seems to be
defined in reverse order -- class methods are at the top, and the constructor is
at the bottom.*/

// getCollections -- method of selectBindings
var getCollections = function() {
    return this.collections;
}
// getHtml -- method of selectBindings
var getHtml = function(any) {
    var html = this.html;
    if (any) {
        html = "<option value=\"\">ANY</option>\n" + html;
    }
    return html;
}
// getCollectionBinding -- method of selectBindings
var getCollectionBinding = function(col_name) {
    return this.collections[col_name].binding;
}
// getCollectionHtml -- method of selectBindings
var getCollectionHtml = function(col_name, any) {
    var html = this.collections[col_name].html;
    if (any) {
        html = "<option value=\"\">ANY</option>\n" + html;
    }
    return html;
}
// setHtml -- method of selectBindings
var setHtml = function(html) {
    this.html = html;
}
// setCollectionHtml -- method of selectBindings
var setCollectionHtml = function(col_name, html) {
    this.collections[col_name].html = html;
}
// genHtml -- method of selectBindings
var genHtml = function() {
    var obj_html = "";
    var collections = this.getCollections();
    for (var col_name in collections) {
        var binding = this.getCollectionBinding(col_name);
        var col_html = "";
        var labels = [];
        for (var i in binding) {
            labels.push(binding[i].label);
        }
        labels.sort();
        for (var i in labels) {
            for (var j in binding) {
                if (binding[j].label == labels[i]) {
                    //console.log(branch.label);
                    col_html += "<option value=\"" + binding[j].codes + "\">" + binding[j].label + "</option>\n";
                }
            }
        }
        this.setCollectionHtml(col_name, col_html);
        obj_html += "<optgroup label=\"" + col_name + "\">" + col_html + "</optgroup>\n";
    }
    this.setHtml(obj_html);
}
// selectBindings: constructor
var selectBindings = function(binding_obj) {
    this.getCollections = getCollections;
    this.getHtml = getHtml;
    this.getCollectionBinding = getCollectionBinding;
    this.getCollectionHtml = getCollectionHtml;
    this.setHtml = setHtml;
    this.setCollectionHtml = setCollectionHtml;
    this.genHtml = genHtml;
    this.collections = {};
    this.html = "";
    for (var col_name in binding_obj) {
        this.collections[col_name] = {};
        this.collections[col_name].binding = binding_obj[col_name];
        this.collections[col_name].html = "";
    }
    this.genHtml();
}

// END selectBindings CLASS DEFINITIONS

/* Definitions for custom limiters--Material Types and Locations
 */

// LOCATIONS
var loc = {
    "czm": {
        "label": "Media General Collection",
        "codes": "czm"
    },
    "czmrs": {
        "label": "Media Reserves",
        "codes": "czmrs"
    },
    "czwww": {
        "label": "Media Electronic Resources",
        "codes": "czwww"
    },
    "d": {
        "label": "Dallas General Collection",
        "codes": "d"
    },
    "dcare": {
        "label": "Dallas Career Development",
        "codes": "dcare"
    },
    "dmed": {
        "label": "Dallas Media",
        "codes": "dmed"
    },
    "dref": {
        "label": "Dallas Reference",
        "codes": "dref"
    },
    "gwww": {
        "label": "Government Documents Electronic Resources",
        "codes": "gwww"
    },
    "llan": {
        "label": "General Non-Web-based Electronic Resources",
        "codes": "llan"
    },
    "lwww": {
        "label": "General Web-based Electronic Resources",
        "codes": "lwww"
    },
    "mwww": {
        "label": "Music Electronic Resources",
        "codes": "mwww"
    },
    "r": {
        "label": "Discovery Park General Collection",
        "codes": "r"
    },
    "rzzrs": {
        "label": "Discovery Park Reserves",
        "codes": "rzzrs"
    },
    "s": {
        "label": "Eagle Commons General Collection",
        "codes": "s"
    },
    "sa": {
        "label": "Eagle Commons Art Books",
        "codes": "sa"
    },
    "szzrs": {
        "label": "Eagle Commons Reserves",
        "codes": "szzrs"
    },
    "w": {
        "label": "Willis General Collection",
        "codes": "w"
    },
    "w1arc": {
        "label": "Graphic Novels",
        "codes": "w1arc"
    },
    "w1grs": {
        "label": "1st Floor Service Desk",
        "codes": "w1grs"
    },
    "w1mls": {
        "label": "Best Sellers",
        "codes": "w1mls"
    },
    "w3d": {
        "label": "Government Documents",
        "codes": "w3d"
    },
    "w3dnb": {
        "label": "Government Documents (No Barcode)",
        "codes": "w3dnb"
    },
    "w3drs": {
        "label": "Government Documents Reserves",
        "codes": "w3drs"
    },
    "w4a": {
        "label": "Archives",
        "codes": "w4a"
    },
    "w4m": {
        "label": "Music General Collection",
        "codes": "w4m"
    },
    "w4mau": {
        "label": "Music Audio",
        "codes": "w4mau"
    },
    "w4mrb": {
        "label": "Music Sandborn Collection",
        "codes": "w4mrb"
    },
    "w4mrs": {
        "label": "Music Reserves",
        "codes": "w4mrs"
    },
    "w4r": {
        "label": "Rare Books",
        "codes": "w4r"
    },
    "w4rrf": {
        "label": "Rare Books Reference",
        "codes": "w4rrf"
    },
    "w4rtx": {
        "label": "Texana Collection",
        "codes": "w4rtx"
    },
    "w4rwe": {
        "label": "Weaver Collection",
        "codes": "w4rwe"
    },
    "wlcmc": {
        "label": "Curriculum Materials Collection",
        "codes": "wlcmc"
    },
    "wljuv": {
        "label": "Juvenile Collection",
        "codes": "wljuv"
    },
    "wlmic": {
        "label": "Microforms",
        "codes": "wlmic"
    }
};

// MATERIAL TYPES
var mat = {
    "tree_d": {
        "label": "3D Objects",
        "codes": "r"
    },
    "archival": {
        "label": "Archival Collections",
        "codes": "p"
    },
    "audio_books": {
        "label": "Audio Books",
        "codes": "i"
    },
    "books": {
        "label": "Books",
        "codes": "a"
    },
    "comp_files": {
        "label": "Computer Files",
        "codes": "m"
    },
    "dvds": {
        "label": "DVDs, VHS Tapes, and Films",
        "codes": "g"
    },
    "ebooks": {
        "label": "E-Books",
        "codes": "n"
    },
    "ejournals": {
        "label": "E-Journals",
        "codes": "y"
    },
    "kits": {
        "label": "Educational Kits",
        "codes": "o"
    },
    "journals": {
        "label": "Journals",
        "codes": "q"
    },
    "manuscripts": {
        "label": "Manuscripts",
        "codes": "t"
    },
    "maps": {
        "label": "Maps",
        "codes": "e"
    },
    "maps_unpub": {
        "label": "Maps, Unpublished",
        "codes": "f"
    },
    "recordings": {
        "label": "Music Recordings and CDs",
        "codes": "j"
    },
    "scores": {
        "label": "Music Scores",
        "codes": "c"
    },
    "scores_unpub": {
        "label": "Music Scores, Unpublished",
        "codes": "d"
    },
    "scores_diss": {
        "label": "Music Scores, Dissertations",
        "codes": "s"
    },
    "graphics": {
        "label": "Print Graphics",
        "codes": "k"
    },
    "t_ds": {
        "label": "Theses and Dissertations",
        "codes": "z"
    }
};
// BINDINGS
var col_loc_bindings = new selectBindings({
    "CMC & Kit Room": [loc.wlcmc],
    "Electronic Resources": [loc.czwww, loc.gwww, loc.lwww, loc.llan, loc.mwww],
    "Discovery Park Library": [loc.r, loc.rzzrs, loc.gwww, loc.lwww],
    "Government Documents": [loc.w3d, loc.w3dnb, loc.w3drs, loc.gwww],
    "Juvenile Collections": [loc.w4rrf, loc.w4rwe, loc.wljuv],
    "Media Library": [loc.czm, loc.czmrs, loc.czwww],
    "Music Library": [loc.w4m, loc.w4mau, loc.w4mrb, loc.w4mrs, loc.wlmic, loc.mwww],
    "Archives/Rare Book Colls": [loc.w4a, loc.w4r, loc.w4rrf, loc.w4rtx, loc.w4rwe],
    "Eagle Commons Library": [loc.s, loc.szzrs, loc.gwww, loc.lwww],
    "UNT Dallas": [loc.d, loc.dcare, loc.dmed, loc.dref, loc.czwww, loc.gwww, loc.lwww, loc.mwww],
    "Willis Library": [loc.w, loc.w1grs, loc.w3d, loc.w3dnb, loc.w3drs, loc.w4a, loc.w4m, loc.w4mau, loc.w4mrb, loc.w4mrs, loc.w4r, loc.w4rrf, loc.w4rtx, loc.w4rwe, loc.wlmic, loc.gwww, loc.llan, loc.lwww, loc.mwww]
});
var any_loc_bindings = new selectBindings({
    "Discovery Park Library": [loc.r, loc.rzzrs],
    "Eagle Commons Library": [loc.s, loc.sa, loc.szzrs],
    "Electronic Resources": [loc.czwww, loc.gwww, loc.llan, loc.lwww, loc.mwww],
    "Media Library": [loc.czm, loc.czmrs],
    "Music Library": [loc.w4m, loc.w4mau, loc.w4mrb, loc.w4mrs],
    "UNT Dallas": [loc.d, loc.dcare, loc.dmed, loc.dref],
    "Willis Library": [loc.w, loc.w1arc, loc.w1grs, loc.w1mls, loc.w3d, loc.w3dnb, loc.w3drs, loc.w4a, loc.w4m, loc.w4mau, loc.w4mrb, loc.w4mrs, loc.w4r, loc.w4rrf, loc.w4rtx, loc.w4rwe, loc.wlcmc, loc.wljuv, loc.wlmic]
});
var music_loc_bindings = new selectBindings({
    "Music Library": [loc.w4mau, loc.w4m, loc.w4mrs, loc.w4mrb]
});
var mat_bindings = new selectBindings({
    "Materials": [mat.tree_d, mat.archival, mat.audio_books, mat.books, mat.comp_files, mat.dvds, mat.ebooks, mat.ejournals, mat.kits, mat.journals, mat.manuscripts, mat.maps, mat.maps_unpub, mat.recordings, mat.scores, mat.scores_unpub, mat.scores_diss, mat.graphics, mat.t_ds]
});

/* updateS12Locations updates location list for ~S12 advanced search screen
based on collections*/
var updateS12Locations = function(event, val) {
    var collection = $(event.data.id + " option:selected").text().replace(/^\W+/, "");
    if (collection != "All Collections") {
        $("#b.custom, #W.custom").html(col_loc_bindings.getCollectionHtml(collection, true));
    } else {
        $("#b.custom, #W.custom").html(any_loc_bindings.getHtml(true));
    }
    if (val) {
        for (var i in val) {
            if (val[i] && $("#b.custom, #W.custom").find("option[value='" + val[i] + "']").length) {
                $("#b.custom, #W.custom").find("option[value='" + val[i] + "']").attr("selected", "selected");
            }
        }
    }
}

/* updateS7Locations updates location list for ~S7 advanced search screen*/
var updateS7Locations = function(val) {
    $("#b.custom-music").html(music_loc_bindings.getCollectionHtml("Music Library", true));
    if (val) {
        for (var i in val) {
            if (val[i] && $("#b.custom-music option[value='" + val[i] + "']").length) {
                $("#b.custom-music option[value='" + val[i] + "']").attr("selected", "selected");
            }
        }
    }
}

/* updateMaterials updates materials list in search screens*/
var updateMaterials = function(val) {
    if ($("#m.custom, #M.custom").length) {
        $("#m.custom, #M.custom").html(mat_bindings.getCollectionHtml("Materials", true));
    } else if ($("#m, #M")) {
        $("#m, #M").html(mat_bindings.getCollectionHtml("Materials", true));
    }
    if (val) {
        if (val.length > 1) {
            $("#makeMulti").click();
        }
        for (var i in val) {
            if (val[i] && $("#m.custom, #M.custom").find("option[value='" + val[i] + "']").length) {
                $("#m.custom, #M.custom").find("option[value='" + val[i] + "']").attr("selected", "selected");
                $("#m.custom, #M.custom").find("option[value='']").removeAttr("selected");
            }
        }
    }
}

// Misc utility functions used in $(document).ready() *************************

// refWorksGetURL builds the URL for the refWorks link--returns the URL value
var refWorksGetUrl = function() {
    var return_val = false;
    if ($("#permalink").length) {
        var bib_id = getBibId($("#permalink").attr("href"));
        if (bib_id) {
            return_val = "http://exlibris.colgate.edu/refworks/processor_a.php?bib=" + bib_id;
        }
    }
    return return_val;
}

/* getIlliadUrlQuery_bibdisplay scrapes the pre-modified bib_display screen for
metadata to send to ILLiad via OpenURL for Illiad requests.*/
var getIlliadUrlQuery_bibdisplay = function() {
    var props = {
        "sid": "Catalog Request",
        "rft.genre": "",
        "rft.title": "",
        "rft.au": "",
        "rft.isbn": "",
        "rft.issn": "",
        "rft.date": "",
        "rft.place": "",
        "rft.pub": "",
        "rft.edition": "",
        "notes": ""
    };
    var pub_info = cleanPunct(fieldSelect("Publication Info").find("td.bibInfoData").eq(0).text());
    props["rft.genre"] = ($(".bibDisplayContentMain .bibMedia img").attr("alt") in {
        "JOURNAL": 1,
        "EJOURNAL": 1
    }) ? "article" : "book";
    props["rft.title"] = encodeURIComponent(cleanPunct(fieldSelect("Title").find("td.bibInfoData").eq(0).text()));
    props["rft.au"] = props["rft.genre"] == "book" ?
        fieldSelect("Author/Creator").length ?
        encodeURIComponent(cleanPunct(fieldSelect("Author/Creator").find("td.bibInfoData").eq(0).text())) :
        encodeURIComponent(cleanPunct(fieldSelect("Contributors").find("td.bibInfoData").eq(0).text())) : "";
    props["rft.isbn"] = encodeURIComponent(cleanPunct(fieldSelect("ISBN").find("td.bibInfoData").eq(0).text()));
    props["rft.issn"] = encodeURIComponent(cleanPunct(fieldSelect("ISSN").find("td.bibInfoData").eq(0).text()));
    props["rft.edition"] = encodeURIComponent(cleanPunct(fieldSelect("Edition").find("td.bibInfoData").eq(0).text()));
    props["notes"] = "Catalog record: http://iii.library.unt.edu" + $("#permalink").attr("href");
    if (pub_info) {
        var matches = pub_info.match(/^([^:]+)\s?:\s+(\D*)(,\s*(.*?))?$/);
        props["rft.date"] = (matches && matches[4]) ? encodeURIComponent(matches[4]) : "";
        props["rft.place"] = (matches && matches[1]) ? encodeURIComponent(matches[1]) : "";
        props["rft.pub"] = (matches && matches[2]) ? encodeURIComponent(matches[2]) : "";
    }
    return $.map(props, function(val, key) {
        return val ? key + "=" + val : null;
    }).join("&");
}

/* getIlliadUrlQuery_briefcit scrapes the briefcit screen for metadata to send to
ILLiad via OpenURL for Illiad requests. The only parameter is index, which is
the index for the result number that we want to scrape.*/
var getIlliadUrlQuery_briefcit = function(index) {
    var props = {
        "sid": "Catalog Request",
        "rft.genre": "",
        "rft.title": "",
        "rft.au": "",
        "rft.isbn": "",
        "rft.issn": "",
        "rft.date": "",
        "rft.place": "",
        "rft.pub": "",
        "rft.edition": "",
        "notes": ""
    };
    var context = $(".briefcitCell").eq(index).find(".metadata-for-illiad");
    return $.map(props, function(val, key) {
        if (!val) {
            if (key == "rft.genre") {
                val = (context.find("span.genre").text() in {
                    "JOURNAL": 1,
                    "EJOURNAL": 1
                }) ? "article" : "book";
            } else {
                val = encodeURIComponent(cleanPunct(context.find("span." + key.replace("rft.", "")).text()).replace(/[\n]/g, ""));
            }
        }
        return val ? key + "=" + val : null;
    }).join("&");
}

/*summonQueryTerms takes a string of text (e.g., a Title) and converts it to
a string of terms to use in a Summon search query*/
var summonQueryTerms = function(text) {
    text = text.toLowerCase();
    text = text.replace(/[&:;,.'"\/\\\[\]\(\)]/g, "");
    text = text.replace(/(^|\s)((a|an|the|of|and|or|in|on|for|from|to|not|be|is|am|are)(\s|$))+/gi, " ");
    text = text.replace(/^\s*/, "");
    text = text.replace(/^((.*?\s){5}).*$/, "$1");
    text = text.replace(/\s*$/, "");
    text = encodeURI(text.replace(/\s+/g, "+"));
    return text;
}

$(document).ready(function() {
    // ****SOME SESSION STUFF WE NEED TO DEAL WITH FIRST
    var windowSession = new Session("library.colgate.edu", window.parent.top);
    // We need to deal with the case that somebody on http has clicked the Request button, set session vars to open up
    // the request form, clicked "stop" before the page has refreshed to https, and then refreshed the page or navigated
    // to another page.
    if (typeof(windowSession.getVar("requestURLsource")) !== "undefined" && window.parent.top.location.protocol != "https:") {
        windowSession.deleteVar("requestURLsource");
        windowSession.deleteVar("requestURLtarget");
        windowSession.deleteVar("requestItem");
        windowSession.deleteVar("requestIndex");
        windowSession.deleteVar("illiad_query");
    }

    // Patrons logging-in and logging-out, etc.
    // FIRST, we want to make sure, if someone is logged in, that the login session var is set.
    if ($("#topNavigationLinks li a[href='/logout']").length) { // means we're logged in
        if (typeof(windowSession.getVar("logged_in")) === "undefined" || typeof(windowSession.getVar("account_url")) === "undefined") { // means the session var isn't set
            windowSession.setVar("logged_in", "1");
            windowSession.setVar("account_url", encodeURI($("#topNavigationLinks span.ss_user").parent("a").attr("href")));
        }
    }
    // Behavior on LOGOUT: When someone is logged in and clicks the logout button, we need to try to store their current
    // path. That way we can redirect them to that path if they're logging out from a public (e.g., non-account-related)
    // screen. To do this we have to set a session var. (Note: if users have JavaScript disabled, they'll just get
    // redirected to the catalog homepage as per usual.)
    $("#topNavigationLinks li a[href='/logout']").click(function() {
        windowSession.setVar("logout_path", encodeURI(window.location.pathname + window.location.search));
        return true;
    });
    if (window.location.pathname == "/screens/logout.html") {
        var logout_path = "/";
        if (typeof(windowSession.getVar("logout_path")) !== "undefined") {
            logout_path = windowSession.getVar("logout_path");
            logout_path = (logout_path && !logout_path.match(/^\/patroninfo/)) ? logout_path : "/";
        }
        windowSession.deleteVar("logout_path");
        windowSession.deleteVar("logged_in");
        windowSession.deleteVar("account_url");
        window.location.replace(decodeURI(logout_path));
    }

    $("div.pageContent").show();
    $.fancybox.hideLoading();
    // ******** BEGIN jQuery block for header functionality:
    // First lets bind rename_element to onclick events where we need to rename elements named "search"
    var rename_element = function(element, rename) {
        element.attr("name", rename);
    }
    $("#SEARCH2").parent("form").find("input[type=submit]").click(rename_element($("#SEARCH2"), "SEARCH"));
    $("#search3").parent("form").find("input[type=submit]").click(rename_element($("#search3"), "search"));
    // Tabs
    //$('#untMultiSearch').tabs({selected:1});
    if (location.hash == "#libcat") {
        $('#untMultiSearch').tabs({
            selected: 0
        });
    } else {
        $('#untMultiSearch').tabs({
            selected: 1
        });
    }
    // inline labels
    $('input.waterMark').each(function() {
        if ($(this).val() === '') {
            $(this).val($(this).attr('title'));
        }
        $(this).focus(function() {
            if ($(this).val() === $(this).attr('title')) {
                $(this).val('').addClass('focused');
            }
        });
        $(this).blur(function() {
            if ($(this).val() === '') {
                $(this).val($(this).attr('title')).removeClass('focused');
            }
        });
    });
    // create an non-standard submit for forms
    var $librarySearchSubmits = $("span.librarySearchSubmit");
    $librarySearchSubmits.hover(function() {
        $(this).addClass("activeButton");
    }, function() {
        $(this).removeClass("activeButton");
    });
    $librarySearchSubmits.click(function() {
        $(this).closest("form").submit();
    });
    // iii search input field ordering is exact, so we need to be creative.
    $("#passThisSearchtype").change(function() {
        var iiiTypeVal = $(this).val()
        $("#searchtype").val(iiiTypeVal);
    });
    //basic css show and hide 
    $("#quick-access-block-wrap").addClass("hide");
    $("#find-click").toggleClass("open");
    $('#find-click').click(function() {
        $("#find-click").toggleClass("open");
        //$(".not-front #quick-access-block-wrap").toggleClass("hide");
        $("#quick-access-block-wrap").toggle("blind", "", "fast", "");
    });
    // ******** END jQuery block for header functionality

    // replace a.account-url href with actual account URL, or account login
    $("a.account-url").each(function() {
        if (windowSession.getVar("logged_in") && typeof(windowSession.getVar("account_url") !== "undefined")) {
            $(this).attr("href", decodeURI(windowSession.getVar("account_url")));
        } else {
            $(this).attr("href", "http://www.library.unt.edu/accounts-login");
        }
    });


    // focus the search inputs for templated search screens
    $("#fillThisWithSearch #SEARCH, #accessibleForm #searcharg, #accessibleForm #search, #accessibleForm #SEARCH, #accessibleForm #author").focus();

    /* add class to classless <div> containing the available-items limiter so we
	can style it*/
    $("input[name=\"availlim\"]").parent().addClass("availLimContainer");

    // Do jQuery Validation on submission of search forms
    var $validator = $("#search").validate();
    if ($validator) {
        $("#formSubmit").live('click', function() {
            if ($validator.form() == true) {
                $("#search").submit();
            } else {
                $validator.showErrors();
            }
            return false;
        });
    }

    // Allow #ebooks hash to be used for homepage searches to default to e-books type search.
    var selectEBooks = function() {
        if ($("#m").length) {
            $("#m option").each(function() {
                if ($(this).val() == "n") {
                    $(this).attr("selected", "selected");
                    $("#m").addClass("optionHighlighted");
                    $("#m").click(function() {
                        $(this).removeClass("optionHighlighted");
                    });
                }
            });
        }
        return true;
    }
    if (window.location.hash == "#e-books") {
        selectEBooks();
    }
    $("#eBooks a").click(selectEBooks);

    // experimental ajax-based search switcher for homepage
    userMaterialSetting = "";
    if ($("#m").length) {
        $("#m").change(function() {
            userMaterialSetting = $("#m").val();
        });
    }
    $("#subnavlist a").click(function() {
        userSearchTerm = $("#fillThisWithSearch form input[name='SEARCH']").val(),
        userScopeSetting = $("#fillThisWithSearch form select[name='searchscope']").val(),
        path = $(this).attr("href") + " #search";

        $("#search").fadeOut(2000).remove();
        $("#fillThisWithSearch").load(path, function() {
            var $validator = $("#search").validate();

            $("#fillThisWithSearch form input[name='SEARCH']").val(userSearchTerm).focus();
            $("#fillThisWithSearch form select[name='searchscope']").val(userScopeSetting);
            if ($("#m").length) {
                $("#m").val(userMaterialSetting);
            }
            $("#formSubmit").click(function() {
                if ($validator.form() == true) {
                    $("#search").submit();
                } else {
                    $validator.showErrors();
                }
                return false;
            });


        });
        $(this).parent().addClass("current").siblings().removeClass("current");

        return false;
    });

    // make up for iii's lack style on submit buttons in result pages
    $("div.browseSearchtool input:submit").addClass("button");

    // toggle material types to multi in advanced search forms
    if ($("#advancedSearchScreen #m, #advancedSearchScreen #M").length) {
        var $makeMulti = $('<a />', {
            text: "Need More Than One?",
            "id": "makeMulti",
            "class": "help js-toggle"
        });
        var $makeSingle = $('<a />', {
            text: "Switch Back",
            "id": "makeSingle",
            "class": "help js-toggle"
        }).hide();
        $("#m, #M").after($("<span class=\"mat-widget\"/>").append($makeMulti, $makeSingle));
        $("#makeMulti").live('click', function() {
            $("#m, #M").attr("multiple", "multiple");
            $(this).hide();
            $makeSingle.show();
            return false;
        });
        $("#makeSingle").live('click', function() {
            $("#m, #M").removeAttr("multiple");
            $(this).hide();
            $makeMulti.show();
            return false;
        });
    }

    // modifications for custom branch locations and material types
    // #b.custom and #m.custom are on Advanced Keyword Search screens;
    // #W.custom and #M.custom are on search limiter (searchmod) screens.
    if ($("#b.custom, #W.custom").length || $("#b.custom-music").length || $("#m.custom, #M.custom").length) {
        var b_match = window.location.search.match(/([&;][bW]=[^&;\/]+)+/);
        var m_match = window.location.search.match(/([&;][mM]=[^&;\/]+)+/);
        var b_vals = b_match ? b_match[0].split(/[&;][bW]=/).slice(1) : [];
        var m_vals = m_match ? m_match[0].split(/[&;][mM]=/).slice(1) : [];
        if ($("#b.custom, #W.custom").length) {
            var id = "";
            if ($("div.pageContentInner select[name='searchscope']").length) {
                id = "div.pageContentInner select[name='searchscope']";
                $(id).on("change", "", {
                    "id": id
                }, updateS12Locations);
            } else if ($("#iiiresetscopewidget").length) {
                id = "#iiiresetscopewidget";
            }
            updateS12Locations({
                "data": {
                    "id": id
                }
            }, b_vals);
        }
        if ($("#b.custom-music").length) {
            updateS7Locations(b_vals);
        }
        if ($("#m.custom, #M.custom").length) {
            updateMaterials(m_vals);
        }
    }

    // change index names for RESOURCE SUBJECT or RESOURCE NAME to DATABASE SUBJECT or DATABASE NAME
    if ($("select[name='searchtype'] option[value='y']").length) {
        $("select[name='searchtype'] option[value='y']").text(" DATABASE NAME");
    }
    if ($("select[name='searchtype'] option[value='f']").length) {
        $("select[name='searchtype'] option[value='f']").text(" DATABASE SUBJECT");
    }

    // browse list "RESOURCE" to "DATABASE"
    if ($(".browseHeaderData").text().match(/RESOURCE/)) {
        $(".browseHeaderData").text($(".browseHeaderData").text().replace("RESOURCE", "DATABASE"));
    }
    if ($("p.problem").text().match(/RESOURCE/)) {
        $("p.problem").text($("p.problem").text().replace("RESOURCE", "DATABASE"));
    }

    // Format search results (brief_cit)
    // Note that each row might be a bib record or a resource record, and they
    // each need to be handled differently.
    if ($("table.browseScreen").length) {
        isbns = [];
        $("table.browseScreen td.briefcitCell:odd").addClass("odd");
        $("div.briefcitRow").each(function() {
            // changes we want to make only if this is a bib result row
            if ($(this).find(".briefcitResource").length == 0) {
                // build the Remote Storage form link for applicable items
                var item_table = $(this).find("table.bibItems");
                if (item_table.find("a.fancybox_remote").length) {
                    var link_text = $(this).find("div.refoworks a").attr("href").replace(/\n/g, "");
                    var bib_id = getBibId(link_text);
                    var cit_title = $(this).find("div.briefcitTitle245").text().replace(/\n/g, "");
                    var cit_author = "";
                    var matches = /\<\/h2\>(.+?)\<br\>(.+?)$/i.exec($(this).find("div.briefcitDetailMain").html().replace(/\n/g, ""));
                    if (matches && matches[1]) {
                        cit_author = matches[1].replace(/\n/g, "");
                    }
                    item_table.find("tr").each(function(i) {
                        var remote_anchor = $(this).find("a.fancybox_remote");
                        if (remote_anchor.length) {
                            var cit_call_number = $(this).find("td").eq(1).text().replace(/\s*Nearby Items\s*/, "").replace(/\n/g, "").replace(/^\s*/, "").replace(/\s*$/, "");
                            var cit_barcode = $(this).find("td").eq(4).text().replace(/\n/g, "").replace(/^\s*/, "").replace(/\s*$/, "");
                            buildRemoteLink(remote_anchor, bib_id, cit_title, cit_author, cit_call_number, cit_barcode);
                        }
                    });
                }
                // reformat text in briefcitDetails
                var briefcitDM = $(this).find("div.briefcitDetailMain");
                var new_div = refactorBriefCitDetail(briefcitDM);
                briefcitDM.replaceWith(new_div);
            } else { // changes we want to make only if this is a resource record result
                $(this).find("div.briefcitActions a.moreInfo").attr("href", $(this).find("div.briefcitActions .toResourceRecord a").attr("href"));
                var fields = $(this).find("div.briefcitResourceDescription div");
                fields.each(function() {
                    if (!$(this).find("span.data").text().match(/^\s*$/)) {
                        $(this).show();
                    }
                });
                linkify($(this).find("div.briefcitResourceAdvisory, div.briefcitResourceDescription .data"));
            }
            // format icons--Google Books thumb or Media Library thumb, and (small) media type icon
            if ($(this).find("span.mediaThumbnail").length) {
                modifyResultsThumbs($(this));
            }
            modifyResultsMediaIcon($(this));
            // remove blank Notes columns and Barcodes from the items table
            hideItemCols($(this).find("div.briefcitItems"), [{
                "col": 1,
                "empty_check": true,
                "repl": $(this).find("div.briefcitItems .bibCall").text()
            }, {
                "col": 3,
                "empty_check": true
            }, {
                "col": 4,
                "empty_check": false
            }]);
            $(this).find("table.bibItems th, table.bibItems td").attr("width", "");
        });
        // Make the AJAX call to GBS to get book covers
        if (window.location.protocol == "http:") {
            $.ajax("//books.google.com/books?jscmd=viewapi&bibkeys=" + isbns.join(","), {
                dataType: "jsonp",
                success: function(data) {
                    for (var i in isbns) {
                        if (typeof(data[isbns[i]]) != "undefined") {
                            var gbs_cover_img = $("span.isbn_" + isbns[i] + " img");
                            gbs_cover_img.attr("src", data[isbns[i]].thumbnail_url);
                            gbs_cover_img.attr("alt", "item cover image");
                            gbs_cover_img.attr("class", "gbs_cover");
                        }
                    }
                }
            });
        }
        $(".browseSaveJump").eq(0).addClass("top");
        $(".browseSaveJump").eq(1).addClass("bottom");
        if ($("td.browsePager").length) {
            var new_pager = $("td.browsePager").html().replace(/\s*Result Page.*?(<)/, "$1");
            $("tr.browsePager").remove();
            $(".browseSaveJump.top").prepend($("<div>").append($(".browseSaveJump.top").find("a[onclick='process_save(0);'],#save_page_btn1")));
            $(".browseSaveJump.bottom").prepend($("<div>").append($(".browseSaveJump.bottom").find("a[onclick='process_save(0);'],#save_page_btn2")));
            $(".browseSaveJump").append("<div class=\"browsePagerOuter\"><div class=\"browsePager\">" + new_pager + "</div></div>");
        }
    }
    $("table.browseScreen").show();

    // Do cleanup on titles for call/standard number browse lists
    if ($("table.browseScreen").length && $(".browseSearchtool select[name='searchtype'] option[selected='selected']").text().match(/number$/i)) {
        $("table.browseList td.browseEntryData, td.yourEntryWouldBeHereData").has("a").each(function() {
            var new_td = refactorBrowseListTitle($(this));
            $(this).replaceWith(new_td);
        });
    }

    $(".navigationRow").eq(0).addClass("top");
    $(".navigationRow").eq(1).addClass("bottom");

    // for staff view--a little formatting, fixing, etc.
    // First we need to determine if we're even on a staff screen--could be bib, item, order, or authority record, and each one is different.
    if ($("span.loggedInMessage:contains('staff mode')").length &&
        ($(".navigationRow a .buttonText:contains('Patron View')").length //bib record
            || ($(".pageContent > table.bibScreen").length && !$(".pageContent > table.bibScreen").children().length)) //item or order
        || ($("table tbody th").eq(0).length && $("table tbody th").eq(0).text().match(/^A\d+$/)) //authority
    ) {
        $("div.pageContent > table").addClass("staffMode");
        $("table.staffMode > tbody > tr").eq(1).addClass("staffModeHeader");
        $("table.staffMode > tbody > tr").eq(2).find("td > table").addClass("staffModeFixed");
        $("table.staffMode > tbody > tr").eq(3).find("td > table").addClass("staffModeVariable");
        $("table.staffModeVariable tr:odd").addClass("odd");
        $(".navigationRow a .anotherSearch").parent().remove();
        if ($("#marcData").length) {
            $("#marcData").addClass("buttonText").html($("#marcData").html().replace("Data for Librarians", "View MARC Tags"));
        }
        $("table.bibPager").eq(1).parentsUntil("div").parent().remove();
        $("div.navigationRow.bottom").remove();
        $("div .bibSearchtool").addClass("bibSearch").removeClass("bibSearchtool").parent().attr("id", "bibDisplayContent");
        if ($("table.bibPager").length) {
            $("#bibDisplayContent").append("<div class=\"bibPager\">" + $("#bibDisplayContent table.bibPager td").html().replace(/\s*Record: &nbsp;\s*/, "") + "</div>");
        }
        $("#bibDisplayContent > table").remove();
        if ($(".bibSearch form").children().length == 0) {
            var form = $(".bibSearch form").remove().html($(".bibSearch").html());
            $(".bibSearch").html("").append(form);
        }
        if ($(".navigationRow").length && $(".navigationRow").next("div:not(#bibDisplayContent)").find("a span").length) {
            $(".navigationRow form").append($(".navigationRow").next().find("a"));
            $(".navigationRow").next().remove();
        }
    }

    // Changes for the abbreviated search tool found on search results pages and bib record pages.
    if ($("div.bibSearch .bibSearchtoolMessage, div.browseSearchtool .browseSearchtoolMessage").length) {
        var search_message = $(".bibSearchtoolMessage, .browseSearchtoolMessage");
        var limit_match = $.trim(search_message.text()).match(/Limited to:\s*(.+?)((\s*and\s*\d+ results found\.)|$)/);
        var results_match = search_message.html().match(/(\d+ results found\.)/);
        var sort_match = search_message.html().match(/(Sorted by .+\.)/);
        search_message.empty();
        search_message.append(results_match && results_match[1] ? "<span class=\"results\">" + results_match[1] + "</span>" : "");
        search_message.append(sort_match && sort_match[1] ? "<span class=\"sort\">" + sort_match[1] + "</span>" : "");
        $("input[name='availlim']").before(limit_match && limit_match[1] ? "<span class=\"limit\" title=\"" + limit_match[1].replace(/"/g, "&quot;") + "\">Search limits: " + (limit_match[1].length > 50 ? limit_match[1].substr(0, 50) + " ... <em>more</em>" : limit_match[1]) + "</span>" : "");
        if (!search_message.children().length) {
            search_message.remove();
        }
    }

    // Bib record transformations
    if ($("#bibDisplayBody").length) {
        // Add the ILLiad query to the ILLiad link.
        var illiad_link = $(".illiad-link");
        illiad_link.attr("href", illiad_link.attr("href") + "&" + getIlliadUrlQuery_bibdisplay());
        // harvest and create google books and a few other links.
        qrCodeGenerator();
        milHarvest();
        milManipulate();

        // add GBS preview button and book jacket
        if (typeof bib_ISBN != 'undefined') {
            compose_GBS(bib_ISBN);
        }

        /* ...but, we use the Media book jacket if it's available, or else we
		try to grab the thumbnail if it's a Digital Library item (based on the
		URL from the 856)*/
        var media_div = $("#linkedImage");
        if (media_div.find("table").length) {
            media_div.html(media_div.find("a").html().replace(/_?(thumbnails|thumbnail)/i, ""));
            media_div.find("img").width(128);
            media_div.show();
        } else {
            $("table.bibLinks a").each(function() {
                if ($(this).attr("href").match(/digital\.library\.unt\.edu\/ark:/) || $(this).attr("href").match(/texashistory\.unt\.edu\/ark:/)) {
                    $("#cover img").attr("alt", "Cover Image");
                    $("#cover img").attr("src", $(this).attr("href").replace(/\/$/, "").replace(/^http:/, "") + "/small/");
                    $("#cover img").width(128);
                }
            });
            $("#cover").show();
        }

        /* if there's no record pager at the top, we still want to have the
		space filled*/
        if (!$("div.bibPager a").length) {
            $("div.bibPager").html("&nbsp;");
        }

        /* navigation row tools...want to group "saved items" tools together*/
        $(".navigationRow form .savingRecords").each(function() {
            if ($(this).text() == "Remove Saved Items") {
                $(this).parent().detach();
            } else {
                $(this).parent().detach().prependTo(".navigationRow form");
            }
        });
        $(".navigationRow form .startOver, .navigationRow form .anotherSearch").parent().detach();
        if ($(".bibPager, .recordPager").length) {
            $(".bibPager, .recordPager").html($(".bibPager, .recordPager").html().replace(/\s*Record: &nbsp;\s*/, ""));
        }

        // **** some sidebar work. 
        // Move the call number link to the sidebar
        if ($("tr.bibItemsEntry").find("td:eq(1) a").length) {
            $("tr.bibItemsEntry:eq(0)").find("td:eq(1) a").detach().appendTo("#nearbyItems");
            $("#nearbyItems").show();
        }
        // move the marc link to the sidebar
        $("#marcData").parent().detach().insertBefore("#permalinkContainer").wrap("<div />");

        // show the Distance Ed. Form link if the item is a book/journal that is
        // available for checkout.
        var available = false;
        $("tr.bibItemsEntry td").eq(2).each(function() {
            if ($(this).text().match(/AVAILABLE/i)) {
                available = true;
            }
        });
        if (available && $("div.bibMedia img.thumb-mimeType").attr("alt") == "BOOKS") {
            $("#distanceEdForm").show();
        }

        // Add the URL for the Refworks link, or hide the element altogether if
        // we can't build the URL.
        var rw_url = refWorksGetUrl();
        if (rw_url) {
            $("div.refoworks a.uiRefworksButton").attr("href", refWorksGetUrl());
        } else {
            $("div.refoworks").hide();
        }

        // refactor the Bib or Resource record display.
        if ($("#untMainContent").length) {
            var new_content = refactorBibDisplay(bib_fields, $("#untMainContent"));
            $("#untMainContent").empty().append(new_content);

            if ($("#linkedImage table.bibLinks").length) {
                $("#linkedImage table.bibLinks").detach().appendTo("#untMainContent div.bibDisplayUrls");
            }
        } else if ($("#untMainContentResource").length) {
            /* this is kind of a hack but it's the easiest way to get it done. Need to add
      bibInfoLabel and bibInfoData classes to resourceInfoLabel and resourceInfoData fields
      so our bib refactoring routines will work on resource record fields.*/
            $("td.resourceInfoLabel").each(function() {
                if (!$(this).text().match(/^\s*$/)) {
                    $(this).addClass("bibInfoLabel");
                }
            });
            $("td.resourceInfoData").addClass("bibInfoData");
            var new_content = refactorBibDisplay(resource_fields, $("#untMainContentResource"));
            $("#untMainContentResource").empty().append(new_content);
        }
        // trigger load of marc data for marc-ajax fields. Note: morph transformations
        // for these fields will happen after the below ajax success function fires
        // because they are registered with the .ajaxStop event during refactorBibDisplay.
        if ($(".ajax-update").length) {
            var url = $("#marcData").parent().attr("href");
            $.ajax(url, {
                dataType: "html",
                success: function(data) {
                    var marc_data = $(data).find("pre").html().replace(/\n/g, "{}").replace(/\{\}(\s{7}|$)/g, "");
                    var field_list = {};
                    $(".ajax-update").each(function() {
                        var classes = $(this).attr("class").split(" ");
                        classes.shift();
                        var class_regex = "(" + classes.join("|") + ")";
                        var regex = new RegExp("\\{\\}" + class_regex + "\\s[0-9\\s][0-9\\s]\\s(.*?)(\\s+\\{|\\s*$)");
                        var matches = marc_data.match(regex);
                        if (matches && matches[2]) {
                            var new_content = matches[2];
                            $(this).find("td.bibInfoData").html(new_content);
                            marc_data = marc_data.replace(regex, matches[3]);
                        }
                    });
                }
            });
        }

        // addthis transformations
        var permalink = "http://library.colgate.edu" + $("#permalink").attr("href");
        var prettyTitle = $("tr.displayTitle td.bibInfoData").text();
        $("#addthis").attr({
            "addthis:url": permalink,
            "addthis:title": prettyTitle,
            "addthis:description": "From the Colgate University Libraries Catalog"
        });

        // do a google scholar link with the title as the basis
        $("#scholarLink").show().find("a").attr("href", "http://scholar.google.com/scholar?&q=" + prettyTitle);

        // Populate the summonBetaLink div link href URL
        if ($("#summonBetaLink").length) {
            var summon_query = summonQueryTerms($("tr.displayTitle td.bibInfoData").text());
            $("#summonBetaLink a").attr("href", "http://untexas.summon.serialssolutions.com/search?s.q=" + summon_query + "&s.fvf[]=ContentType%2CJournal+Article%2Cfalse&s.fvf%5B%5D=IsScholarly%2Ctrue&s.fvf%5B%5D=IsFullText%2Ctrue");
        }

        // new isbn to asin review function

        if ($("img.thumb-mimeType[alt='BOOKS']").length) {
            $('tr.isbn td.bibInfoData').each(function() {
                bib_ASIN = $(this).text(); // get the ISBN and strip all tags
                bib_ASIN = bib_ASIN.replace(/[\n\t\s\-]/ig, ""); // regex cleanup of the text
                bib_ASIN = bib_ASIN.replace(/\(.*\)/ig, "");
                bib_ASIN = bib_ASIN.replace(/:.*$/ig, "");

                if (bib_ASIN.length == 10) {
                    $("#amazonLink").show().find("a").attr("href", "http://www.amazon.com/product-reviews/" + bib_ASIN);
                    return false;
                }
            });
        }

        // show an IMDB link for films
        if ($("img.thumb-mimeType[src$='media_video_small.png']").length) {
            $("#imdbLink").show().find("a").attr("href", "http://www.imdb.com/find?q=" + prettyTitle + "&s=all");
        }

        $("#bibDisplayBody").show();
    }


    // JS changes relevant to bib records in Full Export mode for saved records
    if ($("div.bibDisplayFullExport").length) {
        // now refactor the displays of bib records
        $("div.bibDisplayFullExport").each(function() {
            var new_content = refactorBibDisplay(bib_fields, $(this));
            $(this).empty().append(new_content);
        });
    }

    // JS changes for the full item copies table, linked from bib records (NOTE: changes for requesting are further below, in the
    // REQUESTING section.)
    if ($("div.additionalCopies").length) {
        hideItemCols($(this), [{
            "col": 3,
            "empty_check": true
        }, {
            "col": 4,
            "empty_check": false
        }]);
        $("div.additionalCopiesNav").addClass("navigationRow");
        $("div.additionalCopiesNav").eq(1).addClass("bottom");
        $("div.additionalCopiesNav a").wrap("<form></form>");
        $("table.bibItems").wrapAll("<div class=\"bibDisplayItemsMain\"></div>");
    }

    // course reserves zebra striping	
    if ($("#courseMaterials table.reserveBibs").length) {
        $("#courseMaterials table.reserveBibs tr:eq(0)").addClass("first");
        $("#courseMaterials table.reserveBibs tr:eq(1)").addClass("second");
        $("#courseMaterials table.reserveBibs tr:even").addClass("even");
        $("#courseMaterials table.reserveBibs tr:odd").addClass("odd");
    }

    // other changes for course reserves pages
    if ($("div.course #bibDisplayContent table").length) {
        // Make plain-text hyperlinks anywhere in the Course Record clickable
        linkify($("#bibDisplayContent .mainContent td.bibInfoData"));
    }

    // Superfish on horizontal menu at the top of the page	
    $("ul.sf-menu").superfish({
        disableHI: true
    });

    // advanced search 
    if ($("#advancedSearchScreen").length) {
        // hack to make tds act like th
        $("div.formEntryArea tr").find("td:first").addClass("header");

        // some of iii's advanced search code implemented here instead of inline.
        // detects presence of modified search and runs appropriate transform, focus, etc.
        if (location.search.indexOf('SUBKEY') != -1 || location.search.indexOf('SEARCH') != -1 || location.search.indexOf('searcharg=') != -1) {
            modifySearch(document.location.search);
            init_progsearch();
            if (document.search) {
                document.search.searchText1.focus();
            }
        }
        // Experimental custom advanced-search form, with proximity search selector
        if (window.location.pathname.match(/search~S7/)) { // Just for Music right now
            var form = $(".advancedSearch.music .formEntryArea.custom-tool");
            // Define behavior of proximity selector
            form.find("select.proximity").on("change", function() {
                var input = $("input[name='searchText" + $(this).attr("name").substr(-1) + "']");
                var term = input.val().replace(/"/g, "").replace(/~\d+$/, "");
                if ($(this).val()) {
                    term = "\"" + term + "\"";
                    if ($(this).val() != "phrase") {
                        term += $(this).val();
                    }
                }
                input.val(term);
            });
            // Next, add behavior to existing text input fields to enable proximity selector
            var updateProximity = function() {
                var proximity_select = $("select[name='proximity" + $(this).attr("name").substr(-1) + "']");
                if (proximity_select.prop("disabled") && $(this).val().match(/\S\s+\S/)) {
                    proximity_select.prop("disabled", false);
                } else if (!proximity_select.prop("disabled") && !$(this).val().match(/\S\s+\S/)) {
                    proximity_select.val("");
                    proximity_select.prop("disabled", true);
                }
                var matches = [];
                if (!proximity_select.prop("disabled")) {
                    if ($(this).val().match(/^"(.*)"$/)) {
                        proximity_select.val("phrase");
                    } else if (matches = $(this).val().match(/~(\d+)$/)) {
                        if (matches[1] && matches[1] > 10) {
                            proximity_select.val("~11");
                        } else {
                            proximity_select.val(matches[0]);
                        }
                    } else {
                        proximity_select.val("");
                    }
                }
            };
            form.find("input.searchText").on("keyup", updateProximity);
            // Next, parse out existing search string
            var term_string = form.find("input[name='searchText1']").val();
            var terms = term_string.split(/([a-z]?:?\([^\)]+\))/);
            var i = 1;
            for (var j = 0; j < terms.length; j++) {
                var term = terms[j];
                var matches = term.match(/^([a-z]:)?\((.+?)\)/);
                if (matches) {
                    if (matches[1]) {
                        form.find("select[name='fieldLimit" + i + "']").val(matches[1]);
                    }
                    if (matches[2]) {
                        form.find("input[name='searchText" + i + "']").val(matches[2]);
                    }
                }
                if (term.match(/(\sand\s|\sand not\s|\sor\s)/)) {
                    form.find("select[name='boolean" + i + "']").val(encodeURI(term));
                    i++;
                }
            }
            form.find("input.searchText").keyup();
            $("span.buttonSpriteSpan2:contains('Clear Form')").parents("a").removeProp("onclick").on("click", function() {
                iiiDoReset_1();
                form.find("input.searchText").keyup();
            });
        }
    }

    // E-Resources search page
    if ($("#dbm-search-screen").length) {
        var tabToggle = function(show_class, show_id, hide_class, hide_id) {
            $(show_id).show();
            $(hide_id).hide();
            $(show_class).attr("id", "active");
            $(show_class + " a").attr("id", "current");
            $(hide_class).attr("id", "");
            $(hide_class + " a").attr("id", "");
            return true;
        }
        $(".dbTab").click(function() {
            tabToggle(".dbTab", "#dbs", ".ejTab", "#ejs");
        });
        $(".ejTab").click(function() {
            tabToggle(".ejTab", "#ejs", ".dbTab", "#dbs");
        });
        if (window.location.hash == "#databases") {
            tabToggle(".dbTab", "#dbs", ".ejTab", "#ejs");
        } else if (window.location.hash == "#ejournals") {
            tabToggle(".ejTab", "#ejs", ".dbTab", "#dbs");
        }
    }

    // Related searches
    if ($("span.relSearchLabel").length) {
        var relsearch = $("span.relSearchLabel").parent();
        relsearch.find("a").empty().append($("span.relSearchLabel"));
        if ($(".navigationRow.bottom").length) {
            $(".navigationRow.bottom").find("form a").eq(-1).after(relsearch.find("a").clone());
        }
        $(".navigationRow.top").find("form a").eq(-1).after(relsearch.find("a"));
        relsearch.remove();
    }

    // Patron "My"/"Your Account"
    if ($("div.pat").length) {
        $("#patronSearch").html($("#patronSearch").html().replace(/&nbsp;/g, ""));
        if ($("div.pat div.patFuncArea > *").length == 1) {
            $("div.pat div.patFuncArea").append("<table class=\"patFunc\"><tbody><tr class=\"patFuncNoEntries\"><td class=\"patFuncNoEntries\">Currently you have no fines, no items checked out or on hold, and no preferred searches saved.<br></td></tr></tbody></table>");
        }
        // Modifications to patron "action" buttons -- these are all system generated and not controlled by a WWWOPTION.
        // Holds
        $("div.pat .patButHolds a").text($("div.pat .patButHolds a").text().replace(/(\d+)\s.*$/, "View Requests / Holds ($1 Active)"));
        $("div.pat .patButHolds a").wrapInner("<span class=\"buttonText\"></span>");
        $("div.pat .patButHolds a").prepend("<span class=\"ss_sprite ss_truck\">&nbsp;</span>");
        // Fines
        $("div.pat .patButFines a").text($("div.pat .patButFines a").text().replace(/(\$[0-9.]+)\s.*$/, "View Unpaid Fines ($1 Total)"));
        $("div.pat .patButFines a").wrapInner("<span class=\"buttonText\"></span>");
        $("div.pat .patButFines a").prepend("<span class=\"ss_sprite ss_exclamation\">&nbsp;</span>");
        // Checkouts
        $("div.pat .patButChkouts a").text($("div.pat .patButChkouts a").text().replace(/(\d+)\s.*$/, "View Checkouts & Renew ($1 Active)"));
        $("div.pat .patButChkouts a").wrapInner("<span class=\"buttonText\"></span>");
        $("div.pat .patButChkouts a").prepend("<span class=\"ss_sprite ss_page_white_stack\">&nbsp;</span>");
        // Bookings
        $("div.pat .patButBookings a").wrapInner("<span class=\"buttonText\"></span>");
        $("div.pat .patButBookings a").prepend("<span class=\"ss_sprite ss_date\">&nbsp;</span>");
    }

    // ****** REQUESTING
    // First some event functions we're going to use.
    // itemOnChange: function to bind to "change" event for the item selection screen. Checks to see if the selected item's location
    // cell in the item table has a "limit-pickup-loc" class, indicating that the pickup Location screen will be skipped, and
    // marks out "step 3: select pickup location" in the page header.
    var itemOnChange = function() {
        var step3 = $(".requestScreen .pageHeader ol.step-list li").eq(2);
        var request_screen = $(".requestScreen");
        if ($(this).is(':checked')) {
            if ($(this).hasClass("limit-pickup-loc")) {
                request_screen.find(".buttonSpriteSpan2").text("Submit Request");
                step3.addClass("marked-out");
            } else {
                request_screen.find(".buttonSpriteSpan2").text("Next: Select Pickup Location");
                step3.removeClass("marked-out");
            }
        }
    };
    // radioBigClick just makes an entire element set (like a table row or list item) set a radio button contained therein when
    // clicked.
    var radioBigClick = function() {
        $(this).find("input[type='radio']").attr("checked", "checked").change();
    };
    // togglePickupLocInfo -- On the pickup location selection screen, when an option is selected, it toggles a small
    // info pane on the right side of the screen.
    var togglePickupLocInfo = function() {
        var value = $.trim($(this).attr("value"));
        $(".requestScreen div.infoPane > span[class!='" + value + "']").hide();
        $(".requestScreen div.infoPane > span[class='" + value + "']").show();
    }
    // Next...if we're on a bib screen where the item table has a "view all copies" button, we want to store the
    // request URL in a session var if that link is clicked.
    $("div.bibDisplayItems table.bibItems tbody.toggle_row a").on("click", function() {
        var req_button = $(".requestRecords").parent();
        if (req_button.length) {
            windowSession.setVar("requestURLtarget", encodeURI(req_button.attr("href")));
            windowSession.setVar("bib_title", $.trim($(".refactoredBib table.bibHead tr.displayTitle td.bibInfoData").text()));
        }
    });

    // Now, the request button behavior. Request buttons appear on briefcit (search results) and on bib screens. We
    // want them to behave basically the same way regardless.
    var req_button = $(".requestRecords").parent();
    // for the "view all copies" screen, we want to add request button behavior for any click-to-request links.
    if ($("div.additionalCopies").length) {
        req_button = $("<a href=\"" + decodeURI(windowSession.getVar("requestURLtarget")) + "\"><span class=\"requestRecords\">Request</span></a>");
        $("div.additionalCopies").append(req_button.hide());
    }
    if (req_button.length) {
        if (!req_button.hasClass("fancybox_request")) {
            req_button.addClass("fancybox_request");
        }
        // We want to do a couple of things when someone clicks the request button. First, since the request page uses https
        // and it opens in an iframe, we want to redirect the main page to https first before we open up the request form--if
        // it isn't already https. We do this by setting a session variable, then redirecting to https, then checking for the
        // var when the page reloads, and finally opening up the request form if it's set.
        req_button.click(function() {
            var windowSession = new Session("iii.library.unt.edu", window);
            if (window.location.protocol == "http:") {
                $.fancybox.showLoading();
                $.fancybox.helpers.overlay.open();
                var this_rb_el = this;
                setTimeout(function() {
                    var port = "";
                    if (window.location.port == "2082") {
                        port = ":444";
                    }
                    windowSession.setVar("requestURLsource", encodeURI(window.location.href));
                    windowSession.setVar("requestURLtarget", encodeURI($(this_rb_el).attr("href")));
                    // if this is briefcit, we want to capture which search result we've clicked.
                    if ($("table.browseScreen").length) {
                        windowSession.setVar("requestIndex", req_button.index(this_rb_el));
                    }
                    window.location.replace("https://" + window.location.hostname + port + window.location.pathname + window.location.search);
                }, 500);
                return false;
            }
            // We want to store a session variable so that we have the full bib title available on every screen when someone
            // requests something. BUT where we get the title depends on whether or not we're on briefcit or bib_display.
            var bib_title = "";
            if ($(".refactoredBib table.bibHead tr.displayTitle td.bibInfoData").length) {
                bib_title = $.trim($(".refactoredBib table.bibHead tr.displayTitle td.bibInfoData").text());
            } else if ($(this).parentsUntil("div.briefcitRow").find("div.briefcitTitle245 a").length) {
                bib_title = $.trim($(this).parentsUntil("div.briefcitRow").find("div.briefcitTitle245 a").text());
            }
            if (bib_title) {
                windowSession.setVar("bib_title", bib_title);
            }
            // Same with an ILLiad query string.
            if ($("#bibDisplayBody").length) {
                windowSession.setVar("illiad_query", $(".illiad-link").attr("href").replace(/^https:.*?&(sid=.+)$/, "$1"));
            } else if ($("table.browseScreen").length) {
                windowSession.setVar("illiad_query", getIlliadUrlQuery_briefcit(req_button.index(this)));
            }
        });
    }
    // Location names for Remote Storage and Discovery Park items (w/class click-to-request) should
    // take you to the request form, unless the item is Lib Use Only. In that case we'll open up a fancybox
    // window that tells the patron to contact a librarian. Otherwise, we'll store the index of the item 
    // that was clicked (so we can select that one by default on the item screen) and open up the request form.
    // Finally, we'll make sure that the link text actually has (click to request) appended to it.
    var contact_info = {
        "case-geyer": {
            "desk": "Case-Geyer Library Circulation Desk",
            "contact": "(315) 228-7300 or <a href=\"mailto:fkayiwa@colgate.edu\">DPL_Reference@colgate.edu</a>"
        },
        "cooley": {
            "desk": "Cooley Science Library",
            "contact": "(315) 228-7312"
        },
    };
    // The below snippet adds the request link to individual items, in cases where we have "click to request"
    // on the item.
    $("table.bibItems").each(function(i) {
        var req_el = $("table.bibItems").eq(i).parent().parent().parent().find(".requestRecords").parent();
        $(this).find("tr.bibItemsEntry").each(function() {
            if ($(this).find("td:eq(2)").text().match(/lib use only/i)) {
                $(this).find("a.click-to-request").on("click", function() {
                    var key = "willis";
                    for (var i in contact_info) {
                        if ($(this).hasClass(i)) {
                            key = i;
                        }
                    }
                    $.fancybox({
                        "content": "<div class=\"pageContentInner requestScreen\"><p>Sorry, items marked as <em>LIB USE ONLY</em> may not be requested via the online catalog. Please contact the " + contact_info[key]["desk"] + " at " + contact_info[key]["contact"] + " for assistance.</p></div>",
                        "maxWidth": 500
                    });
                });
            } else if (req_el.attr("href") == "undefined") {
                $(this).find("a.click-to-request").on("click", function() {
                    var back_url = $(".additionalCopiesNav a").attr("href");
                    $.fancybox({
                        "content": "<div class=\"pageContentInner requestScreen\"><p><span style=\"color:red;\">Uh oh!</span> We've encountered an unexpected problem. Please go back to the <a target=\"_parent\" href=\"" + back_url + "\">item page</a> and place your request from there, using the &quot;Request this item for pickup&quot; link. If this problem persists, please let us know by submitting a <a target=\"_blank\" href=\"http://www.library.unt.edu/forms/report-problem\">problem report<a>.</p></div>",
                        "maxWidth": 500
                    });
                });
            } else if (req_el.length) {
                $(this).find("a.click-to-request").on("click", function() {
                    windowSession.setVar("requestItem", $(this).parentsUntil("table.bibItems").find("tr.bibItemsEntry").index($(this).parent().parent()));
                    req_el.click();
                });
            }
        });
        $(this).find("a.click-to-request").each(function() {
            var link_text = $.trim($(this).text());
            if (!link_text.match(/\(?click to request\)?$/i)) {
                $(this).text($(this).text() + " (click to request)");
            }
        });
    });

    // Finally, the request screens.
    // Requesting is usually a 4-step process, and we have 4 separate HTML screens--one for each step:
    // pverify3_web = Account verification and a few optional parameters; item_select = Select an item to request;
    // pickuploc_select = Select a pickup location; request_result = View results of the request, e.g. request summary.
    // The templates for these screens unfortunately aren't consistent, so we have some work to do. The
    // pickuploc_select screen doesn't even include the bib title like the other three do, so we need to use
    // a session var to set and read it...
    var request_screen = $("div.requestScreen");
    var default_nna = 45;
    if (request_screen.length) {
        // fix an odd fancybox glitch first...
        var fb_overlay = $(window.parent.document).find("div.fancybox-overlay");
        if (fb_overlay.length && !fb_overlay.find("div.fancybox-wrap").length) {
            $(window.parent.document).find("body").addClass("fancybox-lock");
            fb_overlay.append($(window.parent.document).find("div.fancybox-wrap"));
            fb_overlay.addClass("fancybox-overlay-fixed");
        }
        windowSession = new Session("iii.library.unt.edu", window.parent);
        var title = (typeof(windowSession.getVar("bib_title")) !== "undefined") ? windowSession.getVar("bib_title") : "Error";
        // First, general template changes
        if (typeof(windowSession.getVar("logged_in")) !== "undefined" && windowSession.getVar("logged_in")) {
            request_screen.find("ol.step-list li").eq(0).text("Enter Optional Information");
        }
        $("body").addClass("request");
        request_screen.find(".formButtonArea a").attr("href", "javascript:;");
        title_array = chunkTitleString(title);
        title = title_array[0].length > 100 ? title_array[0].substring(0, 100) + "..." : title_array[0];
        request_screen.find(".pageHeader h1").html("Request Item for Pickup: <span class=\"title\" title=\"" + title_array[0] + "\">" + title + "</span>");
        request_screen.find("form input[type='submit']").remove();
        // ** pverify3_web.html
        if (request_screen.hasClass("login")) {
            $(".requestScreen #optional .nna fieldset legend, .requestScreen #optional .nna fieldset label").hide();
            request_screen.find("form span.buttonSpriteSpan2").text("Next: Select Item");
            if (windowSession.getVar("is_faculty") == "1") {
                $("#is_faculty").attr("checked", "checked");
            }
            if (typeof(windowSession.getVar("illiad_query")) !== "undefined") {
                $(".illiad-link").attr("href", $(".illiad-link").attr("href") + "&" + windowSession.getVar("illiad_query"));
            }
            request_screen.find("form div.formButtonArea a").on("click", function() {
                $("form[name='patform']").submit()
            });
            // When someone submits, we need to capture the "I am a UNT faculty member" value.
            request_screen.find("form").on("submit", function() {
                if ($("#is_faculty").length) {
                    if ($("#is_faculty:checked").length) {
                        windowSession.setVar("is_faculty", "1");
                    } else {
                        windowSession.setVar("is_faculty", "0");
                    }
                }
            });
            $("#is_faculty").parent().find("span").on("click", function() {
                $("#is_faculty").click();
            });
            // jQuery-UI Datepicker functionality:
            var setIIIDate = function(date) { // sets the date for the III-generated date select options
                var date_array = date.split('/');
                $("#needby_Month").val(date_array[0]);
                $("#needby_Day").val(date_array[1]);
                $("#needby_Year").val(date_array[2]);
            };
            request_screen.find("#nna-datepicker").datepicker({
                showOn: "both",
                buttonImage: "screens/calendar.png",
                buttonImageOnly: true,
                minDate: +1,
                maxDate: "+1Y",
                defaultDate: +default_nna,
                changeMonth: true,
                changeYear: true,
                onClose: function(dateText) {
                    setIIIDate(dateText);
                }
            });
            request_screen.find("#nna-datepicker").datepicker("setDate", +default_nna);
            setIIIDate(request_screen.find("#nna-datepicker").val());
            $("#requestInfo").accordion({
                collapsible: true,
                active: false,
                heightStyle: "content"
            });
        }
        // ** item_select.html
        if (request_screen.hasClass("itemSelect")) {
            var form_container = request_screen.find("div.itemForm");
            var form = form_container.find("form");
            form.find("table th").each(function() {
                $(this).text($(this).find("a").length ? $(this).find("a").text() :
                    $(this).text().match(/\s*Mark\s*/) ? "" : $(this).text());
            });
            form.find("table tr").each(function(i) {
                $(this).on("click", radioBigClick);
                $(this).html($(this).html().replace(/&nbsp;/g, ""));
                var mark = $(this).find("th,td").eq(0).attr("width", "7%").addClass("mark"),
                    location = $(this).find("th,td").eq(1).attr("width", "48%"),
                    call_num = $(this).find("th,td").eq(2).attr("width", "25%"),
                    status = $(this).find("th,td").eq(3).attr("width", "20%"),
                    note = $(this).find("th,td").eq(4).hide(),
                    barcode = $(this).find("th,td").eq(5).hide();
                var notice_text = "";
                if (location.find("a").length) {
                    if (location.find("a").hasClass("limit-pickup-loc")) {
                        mark.find("input").addClass("limit-pickup-loc");
                        notice_text = "Note: You must pick up this item at " + (location.find("a").hasClass("music") ? "the Music Library Service Desk <a class=\"ignore\" href=\"http://www.library.unt.edu/service-desks/music/music-library-service-desk\" target=\"_blank\"><span class=\"ss_sprite ss_information\"></span></a>" : (location.find("a").hasClass("media") ? "the Media Library Service Desk <a class=\"ignore\" href=\"http://www.library.unt.edu/service-desks/media-library/media-library-service-desk\" target=\"_blank\"><span class=\"ss_sprite ss_information\"></span></a>" : "")) + ".";
                    }
                    location.html(location.find("a").html());
                }
                if (status.find("font").length) {
                    notice_text = status.find("font").text();
                    status.find("font").remove();
                }
                location.html(notice_text ? location.html() + " <span class=\"error\">" + notice_text + "</span>" : location.html());
            });
            var new_form = $("<form name=\"patform\" method=\"post\"></form>")
                .append(form_container.find("div.formLabel"))
                .append(form.find("table"))
                .append(form.find("input[type='hidden']"))
                .append(form_container.find("div.formButtonArea"));
            form_container.empty().append(new_form);
            if (typeof(windowSession.getVar("requestItem")) !== undefined && $("table.bibItems tr.bibItemsEntry").eq(windowSession.getVar("requestItem")).find("td.mark input[name='radio']").length) {
                $("table.bibItems tr.bibItemsEntry").eq(windowSession.getVar("requestItem")).find("td.mark input[name='radio']").click();
            }
            new_form.find("input[name='radio']").on("change", itemOnChange).change();
        }
        // ** pickuploc_select.html
        if (request_screen.hasClass("pickupSelect")) {
            request_screen.find("form .patFuncPickupLabel").hide();
            var loc_list = request_screen.find("ul.pickupLoc-list");
            request_screen.find("#locx00 option").each(function() {
                var value = $(this).attr("value");
                if (value != "--") {
                    loc_list.append("<li><input type=\"radio\" name=\"locx00\" value=\"" + value + "\" /><span class=\"display\">" + $(this).text() + "</span></li>");
                }
            });
            loc_list.find("li").on("click", radioBigClick);
            loc_list.find("input[type='radio']").on("change", togglePickupLocInfo);
            loc_list.find("li input[type='radio']").eq(0).attr("checked", "checked").change();
            request_screen.find("#locx00").remove();
            request_screen.find("form br").remove();
            // We want to limit the following pickup location options based on ptype:
            // Graduate Reserve Carrels -- only for ptypes 1, 2, 11, 13, and 31. Have to be logged in to see it.
            if (!$("#ptype").length || ($("#ptype").length && $.inArray($("#ptype").text(), ["1", "2", "11", "13", "31", "35"]) === -1)) {
                request_screen.find("li input[value='wgrc ']").parent().hide();
            }
            // Faculty Delivery -- for ptypes 1, 31, and 35. Don't have to be logged in, but have to select the "I am faculty" option on the first page.
            if ((!$("#ptype").length && (typeof(windowSession.getVar("is_faculty")) === "undefined" || windowSession.getVar("is_faculty") == "0")) || ($("#ptype").length && $.inArray($("#ptype").text(), ["1", "31", "35"]) === -1)) {
                request_screen.find("li input[value='mail ']").parent().hide();
            }
        }
        // ** request_result.html
        if (request_screen.hasClass("requestResult")) {
            if (request_screen.find(".pickupLoc").text().match("Faculty - Deliver to Dept Office")) {
                request_screen.find(".for-delivery").show();
                request_screen.find(".for-pickup").hide();
            }
            if (request_screen.find(".original-error font").length) {
                var error_message = request_screen.find(".original-error font").text();
                var etext_mapping = {
                    "Request denied - already on hold for or checked out to you.": "1",
                    "There is a problem with your library record.  Please see a librarian.": "2"
                };
                if (etext_mapping[error_message]) {
                    request_screen.find("span." + etext_mapping[error_message]).show();
                } else {
                    request_screen.find("span.else p.error-message").text(error_message).show();
                }
            }
        }
        // and finally some handy keyboard interaction for people
        setTimeout(function() {
            if (request_screen.find("form input").eq(0).attr("id") != "nna-datepicker") {
                request_screen.find("form input").eq(0).focus();
            }
        }, 10);
        request_screen.find("form").on("keydown", function(event) {
            if (event.which == 13) {
                $(this).submit();
            }
        });
    }
    // Add fancybox to fancybox links
    $("div.thisSearchesWhat a").fancybox({
        "maxWidth": 500,
        "title": ""
    });
    $("a.fancybox_location").fancybox({
        "title": ""
    });
    $("a.fancybox_remote, a.fancybox_recall").fancybox({
        "height": 500,
        "maxWidth": 1000,
        "title": ""
    });
    $("a.fancybox_mediabook").fancybox({
        "height": 500,
        "width": 900,
        "title": "",
        "type": "iframe"
    });
    $("a.fancybox_istatus").fancybox({
        "height": 400,
        "width": 600,
        "autoSize": false,
        "title": ""
    });
    $("a.fancybox_subjecthelp").fancybox({
        "height": 250,
        "width": 600,
        "autoSize": false,
        "title": ""
    });
    $("a.fancybox_eralert").fancybox({
        "minWidth": 200,
        "minHeight": 0,
        "title": ""
    });
    $("a.fancybox_request").fancybox({
        "height": $("#topNavigationLinks li a[href='/logout']").length ? 600 : 750,
        "width": 800,
        "autoSize": false,
        "title": "",
        "type": "iframe",
        "scrolling": "no",
        "afterClose": function() {
            var add_copies_screen = $("div.additionalCopies").length;
            windowSession.deleteVar("requestItem");
            windowSession.deleteVar("is_faculty");
            windowSession.deleteVar("illiad_query");
            if (!add_copies_screen) {
                windowSession.deleteVar("bib_title");
            }
            if (typeof(windowSession.getVar("requestURLsource")) !== "undefined") {
                var requestURL = windowSession.getVar("requestURLsource");
                windowSession.deleteVar("requestURLsource");
                if (!add_copies_screen) {
                    windowSession.deleteVar("requestURLtarget");
                }
                windowSession.deleteVar("requestIndex");
                window.location.replace(decodeURI(requestURL));
            }
        }
    });
    if (typeof(windowSession.getVar("requestURLsource")) !== "undefined" && $(".fancybox_request").length) {
        if (typeof(windowSession.getVar("requestIndex")) !== "undefined") {
            $(".fancybox_request").eq(windowSession.getVar("requestIndex")).click();
        } else {
            $(".fancybox_request").click();
        }
    }

    // JS changes for login error messages:
    var error = "";
    if ($("div.pverify div.formLogin form").length) { // pverify login form
        error = $("div.pverify div.formLogin form span.errormessage");
    } else if ($("body > p > font").length) { // error screen for course reserves
        error = $("body > p > font");
    }
    if (error.length && error.text().match(/unable to identify you through your campus identification/i)) {
        error.html("<p>Unable to identify you through campus identification. Contact the <a href=\"http://www.unt.edu/helpdesk/\">UIT helpdesk</a> to resolve issues with your EUID. If you still cannot log in, or for any other problems, <a href=\"http://exlibris.colgate.edu/services/reference.html\">contact a librarian.</a></p>");
    }

    $("a[target='_blank']").not(".uiRefworksButton, .ignore").append(' <span class="ss_sprite ss_external-link-grey">&nbsp;</span>');

    // stupid hack to put the "locate in results" button at the top of the results
    var locate_el = $("td.browseSaveJump form").not("#export_form").addClass("locateJump");
    $("td.browseSaveJump").eq(0).prepend(locate_el.clone());
    $("td.browseSaveJump").eq(1).prepend(locate_el);

    // Search History tool appears twice on some screens--in that case we want to hide the second appearance
    if ($(".navigationRow select").length > 1) {
        $(".navigationRow select").eq(1).hide();
    }


    /***********************/
    /*      Analytics      */
    /***********************/

    // sets up custom jQuery selectors for types of links--e.g., makes 
    // "a:external" select external links, "a:internal" select internal links,
    // "a:mailto" select email links, "a:newsearch" select links pointing
    // to a search screen
    $.extend($.expr[':'], {
        external: function(obj) {
            if (obj.href) {
                return !obj.href.match(/^mailto:/) && (obj.hostname != document.location.hostname);
            }
        },
        internal: function(obj) {
            if (obj.href) {
                return !obj.href.match(/^mailto:/) && (obj.hostname == document.location.hostname);
            }
        },
        mailto: function(obj) {
            if (obj.href) {
                return obj.href.match(/^mailto:/);
            }
        },
        newsearch: function(obj) {
            if (obj.href) {
                return obj.href.match(/\/(search)?(~S\d+)?(\/)?\w?(#.+)?$/) && ($(obj).attr("id") != "formSubmit" || $(obj).attr("class") != "submitLinkButton");
            }
        }
    });

    // helps easily identify what template an event fires from--used as categories
    var template = "Other Template";

    if ($("#help-screen").length) {
        template = "Help Screen";
    } else if ($("#bibDisplayBody").length) {
        if ($("#untMainContentResource").length) {
            template = "Resource Record";
        } else {
            template = "Bib Record";
        }
    } else if ($("div.pageContent div.course").length) {
        template = "Course Record";
    } else if ($("table.browseScreen").length) {
        if ($("table.browseScreen table.browseList").length) {
            template = "Phrase Index Browse Screen";
        } else {
            template = "Results Browse Screen";
        }
    } else if ($("#search-screen").length) {
        template = "Search Screen: Main";
    } else if ($("#dbm-search-screen").length) {
        template = "Search Screen: Databases and e-Journals";
    } else if ($("#reserves-screen").length) {
        template = "Search Screen: Course Reserves";
    }

    /*     Header      */

    // Clicked a UNT global navigation link
    $("#unt-globallinks a").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "GLOBAL - Clicked UNT navigation link", label]);
    });
    // Performed a Library or UNT website search 
    $("#edit-submit--2").click(function() {
        var searched_for = $("#edit-search-block-form--2").val();
        if ($("#edit-as-sitesearch").val() == "www.library.unt.edu") {
            _gaq.push(['_trackEvent', template, "GLOBAL - Submitted Library website search", searched_for]);
        } else {
            _gaq.push(['_trackEvent', template, "GLOBAL - Submitted UNT website search", searched_for]);
        }
    });
    // Clicked on the main logo to go to library homepage
    $("#name-and-slogan a").click(function() {
        _gaq.push(['_trackEvent', template, "GLOBAL - Clicked Library logo"]);
    });
    // Clicked on the main logo to go to the UNT homepage
    $("#unt-branding a").click(function() {
        _gaq.push(['_trackEvent', template, "GLOBAL - Clicked UNT logo"]);
    });
    // Clicked on a library global navigation link
    $("#main-menu-left a, #main-menu-right a").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "GLOBAL - Clicked library main navigation link", label]);
    });
    // Clicked to open/close the "Find" tab
    $("#find-click").click(function() {
        if ($(this).hasClass("open")) {
            _gaq.push(['_trackEvent', template, "GLOBAL - Find - Opened Find tab"]);
        } else {
            _gaq.push(['_trackEvent', template, "GLOBAL - Find - Closed Find Tab"]);
        }
    });
    // Clicked a link in one of the Find boxes
    $("#untMultiSearch div a").click(function() {
        var label = $(this).text();
        var tab = $("#untMultiSearch li.ui-tabs-selected a").text();
        if ($(this).parents("#alphaDatabaseList").length) {
            _gaq.push(['_trackEvent', template, "GLOBAL - Find - " + tab + " - Clicked DB A-Z link", label]);
        } else if ($(this).parents("#alphaEjournalList").length) {
            _gaq.push(['_trackEvent', template, "GLOBAL - Find - " + tab + " - Clicked EJ A-Z link", label]);
        } else {
            _gaq.push(['_trackEvent', template, "GLOBAL - Find - " + tab + " - Clicked link", label]);
        }
    });
    // Submitted a search from one of the Find boxes
    $("#untMultiSearch form").submit(function() {
        var label = "";
        var action = "Submitted search";
        var tab = $("#untMultiSearch li.ui-tabs-selected a").text();
        var searchbox = $(this).find("input.untTextInput");
        var options = $(this).find("select option:selected, input:checkbox:checked");
        // Depending on the tab, this is either a search or browse. If it's got a search
        // box, it's a search. Otherwise, it's a browse.
        if (searchbox.length) {
            if (options.length) {
                label += "Options: [";
                options.each(function() {
                    if ($(this).text()) {
                        action += $.trim($(this).text());
                    } else {
                        action += $.trim($(this).parent().text());
                    }
                    action += ", ";
                });
                action = action.replace(/, $/, "]; ");
            }
            label += "Query: " + searchbox.val();
        } else {
            label = options.text();
            action = "Submitted browse";
        }
        _gaq.push(['_trackEvent', template, "GLOBAL - Find - " + tab + " - " + action, label]);
    });

    // Gotta capture the subject guides subject browse, which: 1. doesn't actually submit the form,
    // so there's no submission event, and 2. is called from another server by a <script> insertion
    // in the html and may not even exist at document load...so gotta use event delegation and the 
    // .on() method (see http://api.jquery.com/on/ for more info)
    $("#libGuidesSubjectBrowser").on("click", "input", function() {
        var tab = $("#untMultiSearch li.ui-tabs-selected a").text();
        var label = $(this).parent().find("select option:selected").text();
        _gaq.push(['_trackEvent', template, "GLOBAL - Find - " + tab + " - Submitted browse", label]);
    });

    // Clicked a Quick Link
    $("#quick-access-block-right a").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "GLOBAL - Clicked Library Quick Links link", label]);
    });
    // Clicked on the "Library Catalog" sub-logo to go to catalog homepage
    $("h2.catalogSubHead a").click(function() {
        _gaq.push(['_trackEvent', template, "GLOBAL - Clicked Library Catalog Home subheading"]);
    });
    // Clicked a catalog top nav link
    $("#topNavigationLinks a").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "GLOBAL - Clicked catalog top navigation link", label]);
    });

    /*     Footer      */

    // Clicked a footer link
    $("#footer-wrap a").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "GLOBAL - Clicked footer link", label]);
    });

    /*     General Types of Events     */

    // Navigated to another type of catalog search
    $("div.pageContent a:newsearch").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "Changed to new search screen", label]);
    });

    // Clicked a sidebar link
    $("#mainSidebarLinks a").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "Clicked Main Sidebar link", label]);
    });

    // Modified form widgets before conducting a search	
    $(".pageContent select").not("[name='HISTORY']").change(function() {
        // "select" form widgets, except the search history select
        var label = $(this).find("option:selected").text();
        var name = $(this).attr("name");
        _gaq.push(['_trackEvent', template, "Modified search form widget - Changed " + name, label]);
    });
    $(".pageContent input:checkbox").click(function() {
        // "checkbox" form widgets
        var label = $(this).find(":checked").length ? "checked" : "not checked";
        var name = $(this).attr("name");
        _gaq.push(['_trackEvent', template, "Modified search form widget - Changed " + name, label]);
    });
    $("#makeMulti").on('click', function() {
        _gaq.push(['_trackEvent', template, "Modified search form widget - Expanded material type"]);
    });

    // Opened Music performance search
    $("#doPerformanceSearch, #doPerformanceSearch2").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "Opened Music performance search", label]);
    });

    // Submitted a search
    $("#search-screen .pageContent form, #reserves-screen .pageContent form, div.browseSearchtool form, div.bibSearch form, #dbm-search-screen form[name='search'], #dbm-search-screen form[name='SS_EJPSearchForm']").submit(function() {
        var type = "";
        var label = "";
        if ($("#subnavlist").length) {
            type = $("#subnavlist li.current").text();
        } else if ($("#navlist").length) {
            type = $("#active").text();
            if (type == "Title Search") {
                type = "Course Reserves Title";
            } else if (type == "Databases") {
                type = "Databases by Name";
            } else if (type == "e-Journals") {
                type = "e-Journals by Title";
            }
        } else if ($(this).find("#searchtype").length) {
            type = $(this).find("#searchtype").val();
            if (type == "X") {
                type = "Keyword";
            } else if (type == "a") {
                type = "Author";
            } else if (type == "t") {
                type = "Title";
            } else if (type == "v") {
                type = "Journal Title";
            } else if (type == "d") {
                type = "Subject";
            } else {
                type = "Number Search";
            }
        } else {
            type = $("h1.pageHeader").text();
        }
        $(this).find("select, input").not("[type='hidden']").each(function() {
            label += $(this).attr("name") + ": " + $(this).val() + " | ";
        });
        label = label.replace(/\s\|\s$/, "");
        _gaq.push(['_trackEvent', template, "Submitted '" + type + "' search", label]);
    });

    // Submitted a browse
    $("#dbm-search-screen form[name='subjectSearch'], #dbm-search-screen form[name='SS_subjectSearch']").submit(function() {
        var type = "";
        var label = "";
        if ($(this).attr("name") == "subjectSearch") {
            type = "Database Subject";
        } else {
            type = "e-Journal Subject";
        }
        $(this).find("select, input").not("[type='hidden']").each(function() {
            label += $(this).attr("name") + ": " + $(this).val() + " | ";
        });
        label = label.replace(/\s\|\s$/, "");
        _gaq.push(['_trackEvent', template, "Submitted '" + type + "' browse", label]);
    });
    $("#dbm-search-screen .aToZ a").click(function() {
        var type = "";
        var label = $(this).text();
        if ($(this).parents("#dbs").length) {
            type = "Databases A-Z";
        } else {
            type = "e-Journals A-Z";
        }
        _gaq.push(['_trackEvent', template, "Submitted '" + type + "' browse", label]);
    });

    // Clicked help link
    $("div.formHelper a, a.help, p.help a, a.fancybox_subjecthelp, #reservesSidebar a, div.formRight dl a, div.formRight li a, #launchMediaHelp").click(function() {
        var label = $(this).text();
        if ($(this).hasClass("fancybox_subjecthelp")) {
            label = "Subject help";
        }
        _gaq.push(['_trackEvent', template, "Clicked help link", label]);
    });

    // Clicked item Location link
    $("a.fancybox_location").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "Clicked item Location link", label]);
    });

    // Used Search History
    $("select[name='HISTORY']").click(function() {
        var label = $(this).val();
        _gaq.push(['_trackEvent', template, "Used Search History", label]);
    });

    // Clicked navigation row link
    $(".navigationRow a").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "Clicked results navigation row link", label]);
    });

    // Clicked some sort of external cite/share link--e.g., add to refworks, etc.
    $("a.uiRefWorksButton, #citationLink a, #marcData a, #permalink").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "Clicked external cite/share tool", label]);
    });
    $("#at20mc a").click(function() {
        var label = "AddThis - " + $(this).text();
        _gaq.push(['_trackEvent', template, "Clicked external cite/share tool", label]);
    });

    // Clicked 856 or other external, get-the-item link
    $(".briefcitActions > a:external, .bibDisplayUrls a, table.bibResourceBrief td.dates a, h2.briefcitTitle a:external").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "Clicked 856 or other external, get-the-item link", label]);
    });


    /* Search Results */

    // clicked title
    $("h2.briefcitTitle a:internal, .briefCitActions a[title='About this Database']").click(function() {
        var label = $(this).text();
        _gaq.push(['_trackEvent', template, "Navigated to record display screen", label]);
    });


    // Used the Save Marked Records Button/Feature
    $("span.savingRecords").click(function() {
        var analytics_savingRecordsText = $(this).text();
        _gaq.push(['_trackEvent', "Search Results - Saving Records", "Clicked: " + analytics_savingRecordsText]);
    });


    /* course reserves screens */
    if ($("#reserves-screen").length) {

        // browse list
        $("#browseAlphabetical a").click(function() {
            _gaq.push(['_trackEvent', "Reserves", "Used Browse Links"]);
        });

        // utility links
        $("#reservesSidebar a, a.utility").click(function() {
            var analytics_utlityLinkText = $(this).text();
            _gaq.push(['_trackEvent', "Reserves", "Utility Links", analytics_utlityLinkText]);
        });

    }

    /* browse screens */

    /* shared in browse screens and briefcitations */

    if ($("table.browseScreen").length) {

        // search modifier link/buttons (modify, change to keyword, etc.)
        $("span.searchBarModifier").click(function() {
            var analytics_searchBarModifierText = $(this).text();
            _gaq.push(['_trackEvent', "Search Results - Tools", "Modifing Search", analytics_searchBarModifierText]);
        });

        // use the history function
        $("select[name='HISTORY']").change(function() {
            _gaq.push(['_trackEvent', "Search Results - Tools", "'Search History' Select Feature Used"]);
        });


        // Changed scope select on results screens
        $("div.browseSearchtool").find("select[name='searchscope']").change(function() {
            var analytics_selectedScope = $(this).find("option:selected").text();
            _gaq.push(['_trackEvent', "Search Results - Tools", "Changed Scope", analytics_selectedScope]);
        }).end()
            .find("select[name='searchtype']").change(function() {
                // Changed search type on results screen
                var analytics_selectedNumber = $(this).find("option:selected").text();
                _gaq.push(['_trackEvent', "Search Results - Tools", "Changed Search Type", analytics_selectedNumber]);
            }).end()

        .find("input[name='availlim']").change(function() {
            // Limited results to available items only
            _gaq.push(['_trackEvent', "Search Results - Tools", "Clicked Option to Limit to Available Items Only"]);
        }).end()
            .find("#spellcheck a").click(function() {
                // Used the Spell check tool
                _gaq.push(['_trackEvent', "Search Results - Tools", "Spellcheck Used"]);
            }).end()
            .find("div.browseSearchtoolMessage a").click(function() {
                // Changed sorting on search results listings
                var analytics_sortingText = $(this).text();
                _gaq.push(['_trackEvent', "Search Results - Tools", "Changed Sorting", analytics_sortingText]);
            });
    }

    /* Bib Records */

    if ($("#bibDisplayBody").length) {

        var $bibRecordScreen = $("#bibDisplayBody");

        $("span.searchBarModifier, span.browsingModifier span.savingRecords").click(function() {
            var analytics_bibSearchBarModifierText = $(this).text();
            _gaq.push(['_trackEvent', "Bib Records - Tools", "Modifing Search", analytics_bibSearchBarModifierText]);
        });

        $bibRecordScreen.find("select[name='HISTORY']").change(function() {
            _gaq.push(['_trackEvent', "Bib Records", "'Search History' Select Feature Used"]);
        });

        // Changed scope select on bib records screens
        $bibRecordScreen.find("div.bibSearchtool").find("select[name='searchscope']").change(function() {
            var analytics_bibSelectedScope = $(this).find("option:selected").text();
            _gaq.push(['_trackEvent', "Bib Records", "Changed Scope", analytics_bibSelectedScope]);
        });

        // Changed search type on bib records screen
        $bibRecordScreen.find("div.bibSearchtool").find("select[name='searchtype']").change(function() {
            var analytics_bibSelectedNumber = $(this).find("option:selected").text();
            _gaq.push(['_trackEvent', "Bib Records", "Changed Search Type", analytics_bibSelectedNumber]);
        });

        // Used the Spell check tool
        $("#spellcheck a").click(function() {
            _gaq.push(['_trackEvent', "Bib Records", "Spellcheck Used"]);
        });

        // Changed sorting on bib records
        $bibRecordScreen.find(".bibSearchtoolMessage a").click(function() {
            var analytics_bibSortingText = $(this).text();
            _gaq.push(['_trackEvent', "Bib Records", "Changed Sorting", analytics_bibSortingText]);
        });


        // Limited results to available items only from bib records screen
        $bibRecordScreen.find("div.bibSearchtool").find("input[name='availlim']").change(function() {
            _gaq.push(['_trackEvent', "Bib Records", "Clicked Option to Limit to Available Items Only"]);
        });

        // Used the Bib Record Pager
        $bibRecordScreen.find("div.bibPager a").click(function() {
            _gaq.push(['_trackEvent', "Bib Records", "Pager Used"]);
        });


        // Clicked "search" style link in bib-record data
        $bibRecordScreen.find("td.bibInfoData a").live('click', function() {
            var analytics_bibInfoDataText = $(this).text();
            _gaq.push(['_trackEvent', "Bib Records", "Clicked Link to Search Associated Authors/Titles/Subjects", analytics_bibInfoDataText]);
        });

        // bib item table links (Location, call number or status)
        $("div.bibDisplayItemsMain a").live('click', function() {
            var analytics_bibDisplayItemsMainText = $(this).text();
            _gaq.push(['_trackEvent', "Bib Records", "Clicked Help Link", analytics_bibDisplayItemsMainText]);
        });

        // Bib Links (Links above Location/Call Number pulled from 856, 980, and 982 fields
        $bibRecordScreen.find("table.bibLinks a").live('click', function() {
            var analytics_bibLinks = $(this).text();
            _gaq.push(['_trackEvent', "Bib Records", 'Clicked BibLink', analytics_bibLinks]);
        });

        // sidebar link list
        $("#untMoreInfo a, #whats-this").live('click', function() {
            var analytics_untMoreInfoLinkText = $(this).text();
            _gaq.push(['_trackEvent', "Bib Records", 'Sidebar', "Link: " + analytics_untMoreInfoLinkText]);
        });

        $("#gbs").find("img.preview")
            .click(function() {
                _gaq.push(['_trackEvent', "Bib Records", 'Sidebar', "Google Preview Button"]);
            }).end()
            .find("div.googleBooksText").click(function() {
                _gaq.push(['_trackEvent', "Bib Records", 'Sidebar', "Clicked: More Info from Google Books"]);
            });
    }
    // Last: Clean up the session var space, if it's empty.
    if ($.isEmptyObject(windowSession.getData())) {
        windowSession.deleteSession();
    }
});
